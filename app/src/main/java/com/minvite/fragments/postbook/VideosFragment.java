package com.minvite.fragments.postbook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.minvite.R;
import com.minvite.activities.postbook.PostDetailActivity;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.listeners.OnRecyclerItemClickListener;
import com.minvite.models.BaseAdapterModel;
import com.minvite.models.invitation.EventData;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.postbook.ImageModel;
import com.minvite.async_tasks.VideoThumbnailTask;
import com.minvite.util.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 5/12/16.
 */
public class VideosFragment extends BaseFragment implements OnRecyclerItemClickListener{
    private List<EventPost> eventPosts = new ArrayList<>();
    private EventData eventData;
    private BaseRecycleAdapter baseRecycleAdapter;

    public static VideosFragment getInstance(List<EventPost> videoList, EventData eventData) {
        VideosFragment videosFragment = new VideosFragment();
        videosFragment.eventPosts = videoList;
        videosFragment.eventData = eventData;
        return videosFragment;
    }

    @Override
    public void initViews() {
        List<BaseAdapterModel> imageModels = getVideoModelList(eventPosts);
        RecyclerView recyclerView = (RecyclerView) findView(R.id.grid_videos);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new SpacesItemDecoration(4));
        baseRecycleAdapter = new BaseRecycleAdapter(getActivity(), imageModels);
        recyclerView.setAdapter(baseRecycleAdapter);
        baseRecycleAdapter.setOnItemClickListener(this);
        setVideoThumnile();
    }

    private List<BaseAdapterModel> getVideoModelList(List<EventPost> imageEvents) {
        List<BaseAdapterModel> adapterModels = new ArrayList<>();
        if (imageEvents != null) {
            for (EventPost eventPost : imageEvents) {
                ImageModel imageModel = new ImageModel() {
                    @Override
                    public int getViewType() {
                        return AppConstants.VIEW_TYPE.GRIDE_VIDEO;
                    }
                };
                imageModel.setEventPost(eventPost);
                adapterModels.add(imageModel);
            }
        }
        return adapterModels;
    }

    private void setVideoThumnile() {
        for(final EventPost eventPost: eventPosts){
            String videoUrl = eventPost.getVideoUrl();
            if(!TextUtils.isEmpty(videoUrl)){
                VideoThumbnailTask.getThumbnile(videoUrl, new VideoThumbnailTask.OnLoadThumbnile() {
                    @Override
                    public void onLoadThumbnile(String path) {
                        eventPost.setVideoThumbnile(path);
                        baseRecycleAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_videos;
    }

    @Override
    public void onRecyclerItemClick(View view, int position, Object o, int actionType) {
        ImageModel imageModel= (ImageModel) o;
        Intent intent = new Intent(getActivity(), PostDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, imageModel.getEventPost());
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODES.REFRESH_DATA);
    }

}
