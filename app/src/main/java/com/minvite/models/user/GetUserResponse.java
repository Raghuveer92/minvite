package com.minvite.models.user;

import com.minvite.models.BaseApi;

import java.util.List;

/**
 * Created by raghu on 29/11/16.
 */
public class GetUserResponse extends BaseApi {
    List<UserProfile> data;
    public List<UserProfile> getData() {
        return data;
    }
}
