package com.minvite.holders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.like.*;
import com.like.OnLikeListener;
import com.minvite.R;
import com.minvite.activities.postbook.PostDetailActivity;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.async_tasks.CommentPostTask;
import com.minvite.async_tasks.LikePostTask;
import com.minvite.models.BaseAdapterModel;
import com.minvite.models.user.UserProfile;
import com.minvite.models.postbook.Comment;
import com.minvite.models.postbook.EventPost;
import com.google.gson.Gson;
import com.minvite.models.postbook.ImageModel;
import com.squareup.picasso.Picasso;
import com.todddavies.utils.CommanMethodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 25/08/16.
 */
public class PostHolder extends BaseHolder {
    private TextView numLikes, numComments, numShares, title, timeTv, postText, tvUserName;
    private EditText etComment;
    private View view;
    private ImageView ivUser, ivComment,ivThumbnil;
    private RecyclerView recyclerView;
    LinearLayout layComment;
    com.like.LikeButton likeButton;
    FrameLayout videoFrameLayout;
    private EventPost eventPost;
    private BaseRecycleAdapter adapter;
    private boolean isLikedFromButton;

    public PostHolder(View itemView, BaseRecycleAdapter adapter) {
        super(itemView);
        this.view = itemView;
        this.adapter = adapter;

        recyclerView = (RecyclerView) findView(R.id.recyclerView);
        videoFrameLayout = (FrameLayout) view.findViewById(R.id.videoview);
        ivThumbnil= (ImageView) findView(R.id.ivThumbnil);
        tvUserName = (TextView) itemView.findViewById(R.id.tv_userName);
        likeButton = (com.like.LikeButton) itemView.findViewById(R.id.btn_like);
        numLikes = findTv(R.id.tv_noOfLike);
        numComments = findTv(R.id.tv_no_comments);
        title = findTv(R.id.tv_userName);
        timeTv = findTv(R.id.tv_time);
        postText = findTv(R.id.tv_post_text);
        etComment = (EditText) findView(R.id.et_post_comment);
        ivUser = (ImageView) itemView.findViewById(R.id.iv_user);
        ivComment = (ImageView) findView(R.id.sent_comment);
        layComment = (LinearLayout) itemView.findViewById(R.id.lay_comment_container);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        eventPost = (EventPost) obj;
        postText.setText(eventPost.getStatus());
        UserProfile user = eventPost.getUser();
        setUserData(user);
        setImageOrVideo();


        final List<UserProfile> likes = eventPost.getLikes();
        setLikes(likes);

        final List<Comment> comments = eventPost.getComments();
        setComments(comments);
        String time = DateUtils.getRelativeTimeSpanString(eventPost.getCreated(), System.currentTimeMillis(), 0L).toString();
        timeTv.setText(time);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetailActivity();
            }
        });
    }

    private void changeLike() {
        LikePostTask.like(eventPost);
        setLikes(eventPost.getLikes());
    }


    private void setImageOrVideo() {
        String videoUrl = eventPost.getVideoUrl();
        List<String> imgList = eventPost.getImages();
        String imageUrl = eventPost.getImageUrl();

        if (!TextUtils.isEmpty(videoUrl)) {
            setVideo();
        } else if (null != imgList && imgList.size() > 0) {
            setImageList(imgList);
        } else if (!TextUtils.isEmpty(imageUrl)) {
            setImage(imageUrl);
        } else {
            recyclerView.setVisibility(View.GONE);
            videoFrameLayout.setVisibility(View.GONE);
        }
    }

    private void setComments(final List<Comment> comments) {
        layComment.removeAllViews();
        if (comments != null) {
            int size = comments.size();
            numComments.setText(""+size);
            if (size > 0) {
                layComment.removeAllViews();
                Collections.sort(comments);
                setComment(comments.get(0));
            }
        }
        ivComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String commentText = etComment.getText().toString().trim();
                if (!TextUtils.isEmpty(commentText)) {
                    comments.add(CommentPostTask.sendComment(commentText,eventPost));
                    adapter.notifyDataSetChanged();
                    Util.hideKeyboard(context,etComment);
                    etComment.setText("");
                }
            }
        });
    }

    private void setLikes(final List<UserProfile> likes) {
        isLikedFromButton=false;
        likeButton.setLiked(false);
        if (likes != null) {
            numLikes.setText(String.valueOf(likes.size()));
            UserProfile instance = UserProfile.getInstance();
            for (UserProfile userProfile : likes) {
                if (instance.getObjectId().equals(userProfile.getObjectId())) {
                    likeButton.setLiked(true);
                    break;
                }
                likeButton.setLiked(false);
            }
            isLikedFromButton=true;
            likeButton.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    if(isLikedFromButton){
                        changeLike();
                    }
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    if(isLikedFromButton){
                        changeLike();
                    }
                }
            });
        }
    }

    private void showDetailActivity() {
        Intent intent = new Intent(context, PostDetailActivity.class);
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventPost);
        intent.putExtras(bundle);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, AppConstants.REQUEST_CODES.REFRESH_DATA);
    }

    private void setImage(String imageUrl) {
        videoFrameLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        List<BaseAdapterModel> adapterModels = new ArrayList<>();
        ImageModel imageModel = new ImageModel();
        imageModel.setImageUrl(imageUrl);
        adapterModels.add(imageModel);
        setAdapter(adapterModels);
    }

    private void setImageList(List<String> imgList) {
        List<BaseAdapterModel> adapterModels = new ArrayList<>();
        videoFrameLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        for (String imgUrl : imgList) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImageUrl(imgUrl);
            adapterModels.add(imageModel);
        }
        setAdapter(adapterModels);
    }

    private void setVideo() {
        videoFrameLayout.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        videoFrameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetailActivity();
            }
        });
        String videoThumbnile = eventPost.getVideoThumbnile();
        if(!TextUtils.isEmpty(videoThumbnile)){
            Bitmap bitmapFromPath = CommanMethodes.getBitmapFromPath(context, videoThumbnile);
            if(bitmapFromPath!=null) {
                ivThumbnil.setImageBitmap(bitmapFromPath);
            }
        }
    }

    private void setUserData(UserProfile user) {
        if (user != null) {
            tvUserName.setText(user.getName());
            String imageUrl = user.getImageUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(context).load(imageUrl).into(ivUser);
            }
        }
    }

    private void setAdapter(List<BaseAdapterModel> adapterModels) {
        BaseRecycleAdapter baseRecycleAdapter = new BaseRecycleAdapter(context, adapterModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(baseRecycleAdapter);
    }

    private void setComment(Comment comment) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_comment, null);
        setText(comment.getComment(), R.id.tv_comment_text, view);
        String time = DateUtils.getRelativeTimeSpanString(comment.getCreated(), System.currentTimeMillis(), 0L).toString();
        setText(time, R.id.tv_time_stamp, view);

        UserProfile user = comment.getUser();
        if (user != null) {
            setText(user.getName(), R.id.tv_comment_user, view);
            loadIamge(user.getImageUrl(), R.id.iv_comment_row, view);
        }
        layComment.addView(view);
    }

    private void loadIamge(String imageUrl, int iv_photo, View row) {
        ImageView imageView = (ImageView) row.findViewById(iv_photo);
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).into(imageView);
        }
    }

    public void setText(String text, int textViewId, View v) {
        TextView view = (TextView) v.findViewById(textViewId);
        view.setText(text);
    }


}
