package com.minvite.fragments.postbook;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.activities.ChatActivity;
import com.minvite.activities.postbook.OnFragmentChangeListener;
import com.minvite.models.user.UserProfile;
import com.minvite.models.guest.GuestData;
import com.minvite.models.guest.GuestListResponse;
import com.minvite.models.invitation.EventData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 16/9/16.
 */
public class GuestListFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private List<GuestData> guestList = new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private OnFragmentChangeListener fragmentChangeListener;
    private EventData eventData;
    private List<GuestData> guestDataList;


    public static GuestListFragment getInstance(OnFragmentChangeListener fragmentChangeListener, EventData eventData) {
        GuestListFragment guestListFragment = new GuestListFragment();
        guestListFragment.fragmentChangeListener = fragmentChangeListener;
        guestListFragment.eventData = eventData;
        return guestListFragment;
    }

    @Override
    public void initViews() {
        initToolBar(getString(R.string.guest_list), false);
        setHasOptionsMenu(true);
        ListView listView = (ListView) findView(R.id.lv_guest_list);
        listView.setOnItemClickListener(this);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_guest_list, guestList, this);
        listView.setAdapter(customListAdapter);
        if(guestDataList==null){
            getData(0);
        }else {
            guestList.clear();
            setList(removeRepeated(guestDataList));
        }
    }

    private void getData(long offSetPages) {
        UserProfile instance = UserProfile.getInstance();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
//        httpParamObject.addParameter("where", "eventId='" + eventData.getObjectId() + "' and userId='" + instance.getObjectId() + "'");
        httpParamObject.addParameter("where", "eventId='" + eventData.getObjectId()+"'");
        httpParamObject.addParameter("pageSize", AppConstants.PAGE_SIZE);
        httpParamObject.addParameter("offset", String.valueOf(offSetPages));
        httpParamObject.setClassType(GuestListResponse.class);
        executeTask(AppConstants.TASK_CODES.GUEST_LIST, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GUEST_LIST:
                GuestListResponse guestListResponse = (GuestListResponse) response;
                if (guestListResponse != null) {
                    guestDataList = guestListResponse.getData();
                    if (guestDataList != null) {
                        setList(removeRepeated(guestDataList));
                    }
//                    if(guestListResponse.isNextPage()){
//                        getData(guestListResponse.getOffset());
//                    }
                }
                break;
        }
    }

    private List<GuestData> removeRepeated(List<GuestData> guestDataList) {
        List<GuestData> newDataList = new ArrayList<>();
        HashMap<String, GuestData> hashMap = new HashMap();
        for (GuestData guestData : guestDataList) {
            hashMap.put(guestData.getGuestMobile(), guestData);
        }
        UserProfile instance = UserProfile.getInstance();
        if(instance!=null){
            String mobile = instance.getMobile();
            hashMap.remove(mobile);
        }
        Set<String> stringSet = hashMap.keySet();
        Iterator<String> iterator = stringSet.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            newDataList.add(hashMap.get(key));
        }
        return newDataList;
    }

    private void setList(List<GuestData> guestDataList) {
        guestList.addAll(guestDataList);
        Collections.sort(guestList);
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_guest_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        GuestData guestData = guestList.get(position);
        String guestName = guestData.getGuestName();
        holder.tvName.setText(guestName);
        if (guestName.length() > 0) {
            String s = String.valueOf(guestName.charAt(0));
            if (!TextUtils.isEmpty(s)) {
                holder.tvPic.setText(s);
            }
        }
//        setStatus(guestData, holder);
        return convertView;
    }

    private void setStatus(GuestData guestData, Holder holder) {
        String status = guestData.getStatus();
        holder.tvStatus.setVisibility(View.GONE);
        holder.ivStatus.setVisibility(View.VISIBLE);

        switch (status) {
            case AppConstants.INVITATION_STATUS.ACCEPTED:
                holder.ivStatus.setImageResource(R.drawable.dot_green);
                break;
            case AppConstants.INVITATION_STATUS.REJECTED:
                holder.ivStatus.setImageResource(R.drawable.dot_red);
                break;
            case AppConstants.INVITATION_STATUS.MAY_BE:
                holder.ivStatus.setImageResource(R.drawable.dot_orange);
                break;
            default:
                holder.ivStatus.setVisibility(View.GONE);
                holder.tvStatus.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        GuestData guestData = guestList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, guestData);
        startNextActivity(bundle, ChatActivity.class);
    }

    class Holder {
        TextView tvName, tvStatus, tvPic;
        ImageView ivStatus;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_name_invitation);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvPic = (TextView) view.findViewById(R.id.tv_pic);
            ivStatus = (ImageView) view.findViewById(R.id.iv_status);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.our_story, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {

        }
        return false;
    }

}
