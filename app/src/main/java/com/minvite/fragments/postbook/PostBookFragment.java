package com.minvite.fragments.postbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.minvite.R;
import com.minvite.activities.updates.UpdatesActivity;
import com.minvite.activities.event.SubEventsActivity;
import com.minvite.fragments.postFragments.AllPostFragment;
import com.minvite.fragments.postFragments.ImagesFragment;
import com.minvite.models.user.UserProfile;
import com.minvite.models.invitation.EventData;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.postbook.EventPostResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

public class PostBookFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface, AllPostFragment.OnRefreshList {

    LinkedHashMap<String, Fragment> stringFragmentHashMap = new LinkedHashMap<>();
    List<String> tab = new ArrayList<>();
    private EventPostResponse eventPostResponse;

    public static PostBookFragment getInstance(EventData eventData) {
        PostBookFragment postBookFragment = new PostBookFragment();
        postBookFragment.eventData = eventData;
        return postBookFragment;
    }

    private EventData eventData;

    @Override
    public void initViews() {
        initToolBar("");
        if (eventData != null) {
            setHeaderData(eventData);
            if(eventPostResponse==null){
                getEventData(eventData.getObjectId());
            }else {
                setPostData();
            }
        }
        setOnClickListener(R.id.tv_profile_event, R.id.tv_updates);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.TYPE, AppConstants.TYPE.LIST);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventData);
        switch (v.getId()) {
            case R.id.tv_profile_event:
                startNextActivity(bundle, SubEventsActivity.class);
                break;
            case R.id.tv_updates:
                startNextActivity(bundle, UpdatesActivity.class);
                break;
        }
    }

    private void getEventData(String eventId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_EVENT_POST);
        httpParamObject.addParameter("where", "eventId='" + eventId + "'");
        httpParamObject.setClassType(EventPostResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_EVENT_POST, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_EVENT_POST:
                eventPostResponse = (EventPostResponse) response;
                if (eventPostResponse != null) {
                    setPostData();
                }
                break;
        }
    }

    private void setPostData() {
        stringFragmentHashMap.clear();
        List<EventPost> data = eventPostResponse.getData();
        List<EventPost> allEvents = new ArrayList<>();
        allEvents.addAll(data);

        List<EventPost> imageEvents = new ArrayList<>();
        List<EventPost> videoEvents = new ArrayList<>();

        for (EventPost eventPost : data) {
            String imageUrl = eventPost.getImageUrl();
            String videoUrl = eventPost.getVideoUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                imageEvents.add(eventPost);
            }
            if (!TextUtils.isEmpty(videoUrl)) {
                videoEvents.add(eventPost);
            }
        }
        stringFragmentHashMap.put("ALL (" + allEvents.size() + ")", AllPostFragment.getInstance(allEvents, this, eventData));
        ArrayList<String> imagesList = getImageUrls(imageEvents);
        List<String> videoList = getVideoUrls(videoEvents);
        stringFragmentHashMap.put("PHOTOS (" + imagesList.size() + ")", ImagesFragment.getInstance(imageEvents, eventData));
        stringFragmentHashMap.put("VIDEOS (" + videoList.size() + ")", VideosFragment.getInstance(videoEvents, eventData));
        initTab(stringFragmentHashMap);
        initPager();
    }

    private List<String> getVideoUrls(List<EventPost> imageEvents) {
        List<String> videoUrls = new ArrayList<>();
        if (imageEvents != null) {
            for (EventPost eventPost : imageEvents) {
                String videoUrl = eventPost.getVideoUrl();
                if (!TextUtils.isEmpty(videoUrl)) {
                    videoUrls.add(videoUrl);
                }
            }
        }
        return videoUrls;
    }

    private ArrayList<String> getImageUrls(List<EventPost> imageEvents) {
        ArrayList<String> imageUrls = new ArrayList<>();
        if (imageEvents != null) {
            for (EventPost eventPost : imageEvents) {
                List<String> images = eventPost.getImages();
                if (images != null) {
                    for (String img : images) {
                        imageUrls.add(img);
                    }
                }
            }
        }
        return imageUrls;
    }

    private void setHeaderData(EventData invitaion) {
        UserProfile user = invitaion.getUser();
        if (user != null) {
            String imageUrl = user.getImageUrl();
            String coverImagUrl = user.getCoverImageUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                ImageView imageView = (ImageView) findView(R.id.iv_profile);
                Picasso.with(getActivity()).load(imageUrl).into(imageView);
            }
            if (!TextUtils.isEmpty(coverImagUrl)) {
                ImageView imageView = (ImageView) findView(R.id.iv_cover_image);
                Picasso.with(getActivity()).load(coverImagUrl).into(imageView);
            }
            setText(user.getName(), R.id.tv_person_name);
            String location = user.getLocation();
            if(TextUtils.isEmpty(location)){
                hideVisibility(R.id.tv_place_name);
            }else {
                setText(location, R.id.tv_place_name);
            }
        }
    }

    private void initPager() {
        CustomPagerAdapter pagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), tab, this);
        TabLayout profile_tabLayout = (TabLayout) findView(R.id.tab_profile);
        ViewPager profile_viewPager = (ViewPager) findView(R.id.vp_profile);
        LinearLayout profile_below_Layoutv = (LinearLayout) findView(R.id.layout_profile_below);
        profile_viewPager.setAdapter(pagerAdapter);
        profile_tabLayout.setupWithViewPager(profile_viewPager);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_post_book;
    }


    @Override
    protected int getHomeIcon() {
        return R.drawable.menu_horizontal_line;
    }


    private void initTab(HashMap<String, Fragment> stringFragmentHashMap) {
        tab.clear();
        Set<String> stringSet = stringFragmentHashMap.keySet();
        Iterator<String> iterator = stringSet.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            tab.add(next);
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return stringFragmentHashMap.get(tab.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.REQUEST_CODES.REFRESH_DATA) {
                Bundle bundle = data.getExtras();
                if(bundle!=null){
                    EventPost eventPost= (EventPost) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                    if(eventPost!=null){
                        setNewEvent(eventPost);
                    }
                }
            }
        }
    }

    private void setNewEvent(EventPost eventPost) {
        if(eventPostResponse!=null){
            List<EventPost> data = eventPostResponse.getData();
            if(data!=null){
                data.add(0,eventPost);
            }
        }
        setPostData();
    }

    @Override
    public void reFreshList() {
        getEventData(eventData.getObjectId());
    }
}
