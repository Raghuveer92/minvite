package com.minvite.async_tasks;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Random;

import simplifii.framework.utility.Preferences;

public class VideoThumbnailTask extends AsyncTask<String, Object, String> {
    private OnLoadThumbnile onLoadThumbnile;

    public static void getThumbnile(String videoUrl,OnLoadThumbnile onLoadThumbnile){
        VideoThumbnailTask videoThumbnailTask =new VideoThumbnailTask();
        videoThumbnailTask.onLoadThumbnile=onLoadThumbnile;
        videoThumbnailTask.execute(videoUrl);
    }

    @Override
    protected String doInBackground(String... objectURL) {
        try {
            String videoUrl = objectURL[0];
            String videoSavedPath = Preferences.getData(videoUrl, "");
            if(!TextUtils.isEmpty(videoSavedPath)){
                try {
                    File file=new File(videoSavedPath);
                    if(file.exists()){
                        return videoSavedPath;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            Bitmap bitmap = retriveVideoFrameFromVideo(videoUrl);
            String path = saveImage(bitmap);
            Preferences.saveData(videoUrl,path);
            return path;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String path) {
            if(!TextUtils.isEmpty(path)){
                onLoadThumbnile.onLoadThumbnile(path);
            }
    }

    private String saveImage(Bitmap finalBitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "video_" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    public Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());
        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
    public interface OnLoadThumbnile{
        void onLoadThumbnile(String path);
    }
}