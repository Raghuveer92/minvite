package com.minvite.fragments.invitation;
import com.minvite.activities.invitation.RegisterEventActivity;
import com.minvite.models.invitation.EventData;
import com.minvite.services.VideoUploadTask;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.minvite.R;
import com.minvite.activities.invitation.EventIDetailActivity;
import com.minvite.util.CommanMethodes;
import com.minvite.util.EventCat;
import com.minvite.util.EventType;
import com.minvite.util.FragmentType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by robin.bansal on 10/7/16.
 */
public class EventDetailsFragment extends BaseFragment {

    EventType eventType;
    private TextView tvTimeSelect;
    private TextView tvDateSelect;
    private EventCat eventCat;
    private Bitmap bitmap;
    RegisterEventActivity registerEventActivity;
    public static EventDetailsFragment getInstance(EventType eventType, EventCat event, RegisterEventActivity registerEventActivity) {
        EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
        eventDetailsFragment.eventType = eventType;
        eventDetailsFragment.eventCat = event;
        eventDetailsFragment.registerEventActivity = registerEventActivity;
        return eventDetailsFragment;
    }

    Calendar calendar = Calendar.getInstance();
    int mYear = calendar.get(Calendar.YEAR);
    int mMonth = calendar.get(Calendar.MONTH);
    int mDay = calendar.get(Calendar.DAY_OF_MONTH);


    @Override
    public String getActionTitle() {
        return FragmentType.EVENT_DETAILS.getTitle();
    }

    @Override
    public void initViews() {
        initialiseValidation();
        tvTimeSelect = (TextView) findView(R.id.tv_time_select);
        tvDateSelect = (TextView) findView(R.id.tv_date_select);
        setOnClickListener(R.id.btn_select_files, R.id.btn_upload_video, R.id.tv_time_select, R.id.tv_date_select, R.id.btn_done);
        switch (eventType) {
            case TEXT:
                setEmptyValidation(R.id.et_name, "Name");
                setEmptyValidation(R.id.et_location, "Location");
                setEmptyValidation(R.id.et_description, "Description");
                hideVisibility(R.id.LL_event_photo, R.id.LL_event_video);
                break;
            case TEXT_IMAGE:
                setEmptyValidation(R.id.et_name, "Name");
                setEmptyValidation(R.id.et_location, "Location");
                setEmptyValidation(R.id.et_description, "Description");
                hideVisibility(R.id.LL_event_video);
                break;
            case IMAGE:
                setEmptyValidation(R.id.et_name, "Name");
                setEmptyValidation(R.id.et_location, "Location");
                hideVisibility(R.id.LL_event_description, R.id.LL_event_video);
                break;
            case VIDEO:
                hideVisibility(R.id.LL_event_description, R.id.LL_event_photo);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_files:
                imageupload();
                break;
            case R.id.btn_upload_video:
//                selectVideoFromGallery();
                  video_upload();
                break;
            case R.id.tv_time_select:
                TimeSelect(tvTimeSelect);
                break;
            case R.id.tv_date_select:
                DateSelect(tvDateSelect);
                break;
            case R.id.btn_done:
                uploadData();
                break;

        }
    }

    private void uploadData() {
        if (!isValidate()) {
            return;
        }
        String event_date = tvDateSelect.getText().toString();
        String time = tvTimeSelect.getText().toString();
        if (TextUtils.isEmpty(event_date)) {
            showToast("Please select eventCat date");
            return;
        }
        if (TextUtils.isEmpty(time)) {
            showToast("Please select eventCat time");
            return;
        }
        if(eventType==EventType.IMAGE||eventType==EventType.TEXT_IMAGE){
            if(bitmap==null){
                showToast("Please select image..!");
                return;
            }else {
                showProgressBar();
//                final AsyncTask<Bitmap, Void, String> imageUploadTask = ImageUploadTask.uploadImage(new ImageUploadTask.UploadListener() {
//                    @Override
//                    public void onUpload(String imageUrl) {
//                        hideProgressBar();
//                        uploadData(imageUrl);
//                    }
//
//                    @Override
//                    public void onUploadFailed() {
//                        showToast("Fail to upload image");
//                        hideProgressBar();
//                    }
//                });
//                imageUploadTask.execute(bitmap);
            }
        }
        if(eventType==EventType.TEXT){
            uploadData("");
        }
    }

    private void uploadData(String imageUrl) {
        String name = getEditText(R.id.et_name);
        String description = getEditText(R.id.et_description);
        String location = getEditText(R.id.et_location);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("time", tvTimeSelect.getText());
            jsonObject.put("event_date", tvDateSelect.getText());
            jsonObject.put("location", location);
            jsonObject.put("description", description);
            jsonObject.put("event_type", CommanMethodes.getEventTypeJSON(eventType));
            jsonObject.put("user", CommanMethodes.getUsersJSON());
            jsonObject.put("event_cat", CommanMethodes.getEventCatJSON(eventCat));
            jsonObject.put("imageUrl", imageUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.EVENT_REG);
        object.setPostMethod();
        object.setJson(jsonObject.toString());
        object.setClassType(EventData.class);
        executeTask(AppConstants.TASK_CODES.EVENT_REG, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == AppConstants.TASK_CODES.EVENT_REG) {
            EventData eventData = (EventData) response;
            if (eventData.getObjectId() != null) {
                showToast("Successfully uploaded...!");
                final Intent intent = new Intent(getActivity(), EventIDetailActivity.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventData);
                intent.putExtras(bundle);
                startActivityForResult(intent, AppConstants.REQUEST_CODES.ADD_INVITAION);
            } else {
                showToast(eventData.getMessage());
            }
        }
    }

    private void DateSelect(final TextView textView) {
        final DatePickerDialog dpd = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                String dateofmonth = String.valueOf(dayOfMonth);
                String month = String.valueOf(monthOfYear);
                String yearSelected = String.valueOf(year);
                if (dayOfMonth < 10) {
                    dateofmonth = "0" + dateofmonth;
                }

                month = ConvertMonthName(month);

                String DateSelected = dateofmonth + " | " + month + " | " + yearSelected;
                textView.setText(DateSelected);
            }
        }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpd.show();
    }


    private String ConvertMonthName(String month) {
        switch (month) {
            case "0":
                month = "January";
                break;
            case "1":
                month = "February";
                break;
            case "2":
                month = "March";
                break;
            case "3":
                month = "April";
                break;
            case "4":
                month = "May";
                break;
            case "5":
                month = "June";
                break;
            case "6":
                month = "July";
                break;
            case "7":
                month = "August";
                break;
            case "8":
                month = "September";
                break;
            case "9":
                month = "October";
                break;
            case "10":
                month = "November";
                break;
            case "11":
                month = "December";
                break;
        }
        return month;
    }


    private void TimeSelect(final TextView textView) {
        TimePickerDialog tpd = new TimePickerDialog(activity,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String hours = String.valueOf(hourOfDay);
                        String minutes = String.valueOf(minute);

                        if (hourOfDay < 10) {
                            hours = "0" + hourOfDay;
                        }
                        if (minute < 10) {
                            minutes = "0" + minute;
                        }
                        String formattedTime = formatTime(hours + ":" + minutes);
                        textView.setText(formattedTime);
                    }
                }, 8, 0, true);
        tpd.show();
    }

    private String formatTime(String time) {

        int hour = Integer.parseInt(time.substring(0, time.indexOf(':')));
        int minute = Integer.parseInt(time.substring(time.indexOf(':') + 1));
        if (hour == 0) {
            return "12" + ":" + formatedMinute(minute) + "AM";
        }
        if (hour < 12) {
            return time + " AM";
        }
        if (hour == 12) {
            return time + " PM";
        } else {

            return formatedMinute(hour - 12) + ":" + formatedMinute(minute) + " PM";
        }
    }

    private String formatedMinute(int minute) {
        if (minute < 10) {
            return "0" + minute;
        } else {
            return String.valueOf(minute);
        }
    }

    private void imageupload() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, AppConstants.REQUEST_CODES.REQUEST_CODE_GALLARY);
    }

    public void selectVideoFromGallery() {
        Intent intent;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        } else {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        }
        intent.setType("video_event/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, AppConstants.REQUEST_CODES.VIDEO_CAPTURE);
    }

    private void video_upload() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("video_event/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), AppConstants.REQUEST_CODES.VIDEO_CAPTURE );
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_CODES.REQUEST_CODE_GALLARY:
                    final Uri imageUri = data.getData();
                    bitmap = Util.getBitmapFromUri(getActivity(), imageUri);
                    ImageView imageView = (ImageView) findView(R.id.iv_selected_image);
                    imageView.setImageBitmap(bitmap);
                    imageView.setVisibility(View.VISIBLE);
                    break;
                case AppConstants.REQUEST_CODES.VIDEO_CAPTURE:
                    String selectedVideoPath = Util.getPathFromURI(getActivity(),data.getData());
                    final TextView video_file_name = (TextView) findView(R.id.tv_video_path);
                    video_file_name.setText(selectedVideoPath);
                    showProgressBar();
                    final VideoUploadTask videoUploadTask = VideoUploadTask.uploadVideo(new VideoUploadTask.UploadListener() {
                        @Override
                        public void onUpload(String imageUrl) {
                            showToast("Success");
                            hideProgressBar();
                        }
                        @Override
                        public void onUploadFailed() {
                            hideProgressBar();
                        }
                    });
                    videoUploadTask.execute(data.getData());
                    break;
                case AppConstants.REQUEST_CODES.ADD_INVITAION:
                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                    break;
            }
        } else if (requestCode == AppConstants.REQUEST_CODES.ADD_INVITAION) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }

    private String getPath(Uri uri) {
//        int column_index = 0;
//        String[] projection = {MediaStore.Images.Media.DATA};
//        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
//
//        if (cursor != null) {
//            column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
//            cursor.moveToFirst();
//        }
//        return cursor.getString(column_index);
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = registerEventActivity.getContentResolver().query(uri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }   }
    @Override
    public int getViewID() {
        return R.layout.fragment_event_details;
    }
}
