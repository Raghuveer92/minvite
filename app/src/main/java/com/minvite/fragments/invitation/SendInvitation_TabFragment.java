package com.minvite.fragments.invitation;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.models.Contact;
import com.minvite.models.guest.GuestData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.widgets.CustomImageCheckBox;

/**
 * Created by RAHU on 15-07-2016.
 */
public class SendInvitation_TabFragment extends BaseFragment implements CustomListAdapterInterface {
    List<Contact> contactList = new ArrayList<>();
    List<Contact> filterList = new ArrayList<>();
    HashSet<Contact> selectedContacts =new HashSet<>();
    private CustomListAdapter customListAdapter;
    SelectContactListner listner;

    public static SendInvitation_TabFragment getInstance(List<Contact> contactList, HashSet<Contact> selectedContacts, SelectContactListner listner) {
        SendInvitation_TabFragment sendInvitationTabFragment = new SendInvitation_TabFragment();
        sendInvitationTabFragment.contactList = contactList;
        sendInvitationTabFragment.selectedContacts=selectedContacts;
        sendInvitationTabFragment.filterList.addAll(contactList);
        sendInvitationTabFragment.listner=listner;
        return sendInvitationTabFragment;
    }


    @Override
    public void initViews() {
        ListView listView = (ListView) findView(R.id.lv_send_invitation);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_send_invitation, contactList, this);
        listView.setAdapter(customListAdapter);
    }

    @Override
    public int getViewID() {
        return R.layout.send_invitation_fragment;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);

            Holder holder = new Holder(convertView);
            convertView.setTag(holder);
        }

        Holder holder = (Holder) convertView.getTag();
        final Contact contact = contactList.get(position);
        holder.tvName.setText(contact.getName());
        holder.tvNumber.setText(contact.getPhoneNumber());

        try {
            Uri uri = Uri.parse(contact.getPhotoUri());
            holder.imageView_invitation.setImageURI(uri);
        } catch (Exception e) {
            holder.imageView_invitation.setImageResource(R.drawable.ic_persnal);
        }
        if(contact.isDisable()){
            holder.imageCheckBox.setSelect(true);
            holder.outerView.setEnabled(false);
            holder.outerView.setBackgroundColor(getResourceColor(R.color.transparent_white));
        }else {
            holder.outerView.setEnabled(true);
            holder.outerView.setBackgroundColor(getResourceColor(R.color.full_transparent));
            if(selectedContacts.contains(contact)){
                holder.imageCheckBox.setSelect(true);
            }else {
                holder.imageCheckBox.setSelect(false);
            }
            holder.outerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addContact(contact);
                }
            });
        }

        return convertView;
    }

    private void addContact(Contact contact) {
        if (selectedContacts.contains(contact)) {
            selectedContacts.remove(contact);
        } else {
            selectedContacts.add(contact);
        }
        customListAdapter.notifyDataSetChanged();
        listner.onContactSelect();
    }

    public void filterList(CharSequence charSequence) {
        for(Contact contact:filterList){

        }
    }

    public void refreshData() {
        customListAdapter.notifyDataSetChanged();
    }

    public void setSelectedGuestList(List<GuestData> selectedGuestList) {
        for(Contact contact:contactList){
            for(GuestData guestData:selectedGuestList){
                String phoneNumber = contact.getPhoneNumber().trim();
                phoneNumber = phoneNumber.replaceAll(" ", "");
                phoneNumber = phoneNumber.replaceAll("-", "");
                phoneNumber = phoneNumber.replaceAll("\\(", "");
                phoneNumber = phoneNumber.replaceAll("\\)", "");
                if (phoneNumber.length() > 10) {
                    phoneNumber = phoneNumber.substring((phoneNumber.length()) - 10, phoneNumber.length());
                }
                if(guestData.getGuestMobile().equals(phoneNumber)){
                    contact.setDisable(true);
                }
            }
        }
        customListAdapter.notifyDataSetChanged();
    }


    class Holder {
        TextView tvName, tvNumber;
        ImageView imageView_invitation;
        CustomImageCheckBox imageCheckBox;
        FrameLayout outerView;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_name_invitation);
            tvNumber = (TextView) view.findViewById(R.id.tv_number_invitation);
            imageView_invitation = (ImageView) view.findViewById(R.id.iv_invitation_pic);
            imageCheckBox = (CustomImageCheckBox) view.findViewById(R.id.cb_send_invitation);
            outerView= (FrameLayout) view.findViewById(R.id.lay_outer_layer);
        }
    }
    public interface SelectContactListner{
        void onContactSelect();
    }
}
