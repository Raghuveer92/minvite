package com.minvite.models.sub_events;

import com.minvite.models.BaseApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raghu on 11/10/16.
 */
public class SubEventResponce extends BaseApi{
    ArrayList<SubEventData> data;

    public ArrayList<SubEventData> getData() {
        return data;
    }
}
