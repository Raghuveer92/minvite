package com.minvite.models.beacons;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.minvite.models.BaseApi;

import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by raghu on 10/12/16.
 */

public class BeaconResponse extends BaseApi {
    List<BeaconData> data;

    public List<BeaconData> getData() {
        return data;
    }

    public void saveToPreference() {
        Gson gson=new Gson();
        String json = gson.toJson(this);
        Log.i("Beacon","save new data: "+json);
        Preferences.saveData(AppConstants.PREF_KEYS.BEACONS_INSTANCE,json);
    }

    public static BeaconResponse getInstance() {
        String beaconJson = Preferences.getData(AppConstants.PREF_KEYS.BEACONS_INSTANCE, "");
        if(!TextUtils.isEmpty(beaconJson)){
            Gson gson=new Gson();
            Log.i("Beacon","saved data: "+beaconJson);
            return gson.fromJson(beaconJson,BeaconResponse.class);
        }
        return null;
    }
}
