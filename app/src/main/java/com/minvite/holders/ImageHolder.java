package com.minvite.holders;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.minvite.R;
import com.minvite.models.postbook.ImageModel;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.todddavies.utils.CommanMethodes;

/**
 * Created by raghu on 15/11/16.
 */

public class ImageHolder extends BaseHolder {
    private ImageView imageView, ivCross;

    public ImageHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) findView(R.id.iv_postbook);
        ivCross = (ImageView) findView(R.id.iv_cross);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final ImageModel imageModel = (ImageModel) obj;
        String imageUrl = imageModel.getImageUrl();
        String imagePath = imageModel.getImagePath();
        if (!TextUtils.isEmpty(imagePath)) {
            setImagePath(imagePath, imageView);
            ivCross.setVisibility(View.VISIBLE);
        } else if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl.trim()).placeholder(R.drawable.photo_place_holder).into(imageView);
        }
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageModel.getOnRemoveImageListener().onRemove(imageModel);
                itemView.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                            }
                        });

            }
        });
    }

    private void setImagePath(String imagePath, ImageView imageView) {
        try {
            Uri uri = CommanMethodes.getUriFromPath(imagePath);
            Bitmap bitmapFromUri = CommanMethodes.getBitmapFromUri(context, uri);
            Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(bitmapFromUri);
            imageView.setImageBitmap(resizedBitmap);
//            imageLoader = ImageLoader.getInstance();
//            imageLoader.displayImage(uri.toString(), imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
