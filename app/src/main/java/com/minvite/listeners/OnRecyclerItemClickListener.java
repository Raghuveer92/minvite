package com.minvite.listeners;

import android.view.View;

/**
 * Created by raghu on 6/12/16.
 */

public interface OnRecyclerItemClickListener {
    void onRecyclerItemClick(View view, int position,Object o,int actionType);
}
