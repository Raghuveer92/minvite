package com.minvite.async_tasks;

import android.os.AsyncTask;

import com.minvite.models.postbook.EventPost;
import com.minvite.models.sub_events.SubEventData;
import com.minvite.models.user.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.HttpRestService;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 16/11/16.
 */

public class LikeSubEventTask extends AsyncTask<SubEventData, Void, Void> {
    public static void like(SubEventData subEventData){
        List<UserProfile> likes = subEventData.getLikes();
        UserProfile userProfile = UserProfile.getInstance();
        boolean isLike=false;
        for (UserProfile profile : likes) {
            if (profile.getObjectId().equals(userProfile.getObjectId())) {
                isLike=true;
                userProfile=profile;
            }
        }
        if(isLike){
            likes.remove(userProfile);
        }else {
            likes.add(userProfile);
        }
        new LikeSubEventTask().execute(subEventData);
    }

    @Override
    protected Void doInBackground(SubEventData... eventPosts) {
        if (eventPosts.length == 0) {
            return null;
        }
        SubEventData eventPost = eventPosts[0];
        List<UserProfile> likes = eventPost.getLikes();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SUB_EVENTS + "/" + eventPost.getObjectId());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("likes", getLikeJson(likes));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setPutMethod();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(SubEventData.class);
        HttpRestService httpRestService = new HttpRestService();
        try {
            httpRestService.getData(httpParamObject);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONArray getLikeJson(List<UserProfile> likes) throws JSONException {
        if (likes == null) {
            likes = new ArrayList<>();
        }
        JSONArray jsonArray = new JSONArray();
        for (UserProfile userProfile : likes) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("___class", "Users");
            jsonObject.put("objectId", userProfile.getObjectId());
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }
}
