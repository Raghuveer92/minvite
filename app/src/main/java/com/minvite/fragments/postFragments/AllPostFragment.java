package com.minvite.fragments.postFragments;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.minvite.R;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.models.BaseAdapterModel;
import com.minvite.models.postbook.WriteSomeThingModel;
import com.minvite.models.invitation.EventData;
import com.minvite.models.postbook.EventPost;
import com.minvite.async_tasks.VideoThumbnailTask;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by RAHU on 09-08-2016.
 */
public class AllPostFragment extends BaseFragment {
    private RecyclerView recyclerViewProfile;
    private BaseRecycleAdapter<BaseAdapterModel> adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<BaseAdapterModel> profileList = new ArrayList();
    EventPost selectedEventPost;
    OnRefreshList onRefreshList;
    EventData eventData;

    public static AllPostFragment getInstance(List<EventPost> eventPostList, OnRefreshList onRefreshList, EventData eventData) {
        AllPostFragment allPostFragment = new AllPostFragment();
        allPostFragment.eventPostList = eventPostList;
        allPostFragment.onRefreshList = onRefreshList;
        allPostFragment.eventData = eventData;
        return allPostFragment;
    }

    private List<EventPost> eventPostList = new ArrayList<>();

    @Override
    public void initViews() {

        profileList.clear();
        WriteSomeThingModel writeSomeThingModel=new WriteSomeThingModel(eventData);
        profileList.add(writeSomeThingModel);
        profileList.addAll(eventPostList);
        adapter = new BaseRecycleAdapter<>(getActivity(), profileList);
        recyclerViewProfile = (RecyclerView) findView(R.id.rv_profile);
        recyclerViewProfile.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewProfile.setLayoutManager(mLayoutManager);
        recyclerViewProfile.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProfile.setAdapter(adapter);
        setVideoThumnile();
    }

    private void setVideoThumnile() {
        for(final EventPost eventPost:eventPostList){
            String videoUrl = eventPost.getVideoUrl();
            if(!TextUtils.isEmpty(videoUrl)){
                VideoThumbnailTask.getThumbnile(videoUrl, new VideoThumbnailTask.OnLoadThumbnile() {
                    @Override
                    public void onLoadThumbnile(String path) {
                        eventPost.setVideoThumbnile(path);
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        }
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_profile;
    }


    public interface OnRefreshList {
        void reFreshList();
    }

}
