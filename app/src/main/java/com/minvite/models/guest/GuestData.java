package com.minvite.models.guest;

import com.minvite.models.BaseApi;

/**
 * Created by raghu on 3/10/16.
 */

public class GuestData extends BaseApi implements Comparable<GuestData>{
    String eventCode,eventId,guestName,guestMobile,userId,status,subEventId;

    public String getEventCode() {
        return eventCode;
    }

    public String getEventId() {
        return eventId;
    }

    public String getGuestName() {
        return guestName;
    }

    public String getGuestMobile() {
        return guestMobile;
    }

    public String getUserId() {
        return userId;
    }

    public String getStatus() {
        return status;
    }

    public String getSubEventId() {
        return subEventId;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public void setGuestMobile(String guestMobile) {
        this.guestMobile = guestMobile;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSubEventId(String subEventId) {
        this.subEventId = subEventId;
    }

    @Override
    public int compareTo(GuestData guestData) {
        String guestName = guestData.getGuestName().toLowerCase();
        int compareTo = this.guestName.toLowerCase().compareTo(guestName);
        return compareTo;
    }
}
