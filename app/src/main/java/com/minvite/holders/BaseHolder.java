package com.minvite.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.listeners.OnRecyclerItemClickListener;
import com.minvite.models.BaseAdapterModel;

/**
 * Created by nbansal2211 on 25/08/16.
 */
public class BaseHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected Context context;
    protected OnRecyclerItemClickListener onItemClickListener;
    protected int position;
    protected Object obj;

    public BaseHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
    }

    protected void setOnClickListener(int... viewIds) {
        for(int id:viewIds){
            itemView.findViewById(id).setOnClickListener(this);
        }
    }

    protected View findView(int id) {
        return itemView.findViewById(id);
    }

    protected TextView findTv(int id) {
        return (TextView) findView(id);
    }

    public void onBind(int position, Object obj) {
        this.position=position;
        this.obj=obj;
        Log.i("msg","onBind of BaseHolder..!");
    }

    @Override
    public void onClick(View view) {

    }
    protected void callRecyclerClick(View view,int actionType) {
        if(onItemClickListener!=null){
            onItemClickListener.onRecyclerItemClick(view,position,obj,actionType);
        }
    }
    public void setOnItemClickListener(OnRecyclerItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
