package com.minvite.fragments.postbook;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.minvite.R;
import com.minvite.activities.more.MoreActivity;
import com.minvite.models.MoreData;
import com.minvite.models.invitation.EventData;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 18/11/16.
 */

public class MoreFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private EventData eventData;
    List<MoreData> moreDataList = new ArrayList<>();

    public static MoreFragment getInstance(EventData eventData) {
        MoreFragment moreFragment=new MoreFragment();
        moreFragment.eventData = eventData;
        return moreFragment;
    }

    @Override
    public void initViews() {
        initToolBar("More",false);
        initMoreData();
        ListView listView = (ListView) findView(R.id.list_view_more);
        CustomListAdapter customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_more, moreDataList, this);
        listView.setAdapter(customListAdapter);
        listView.setOnItemClickListener(this);
    }

    private void initMoreData() {
        moreDataList.clear();
        moreDataList.add(new MoreData("Venues", AppConstants.FRAGMENT_TYPE.VENUES,eventData));
        moreDataList.add(new MoreData("Our Story", AppConstants.FRAGMENT_TYPE.TIME_LINE,eventData));
        moreDataList.add(new MoreData("Album", AppConstants.FRAGMENT_TYPE.ALBUM,eventData));
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_more;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if(convertView==null){
            convertView=inflater.inflate(resourceID,parent,false);
        }
        MoreData moreData = moreDataList.get(position);
        setText(R.id.tv_more,moreData.getName(),convertView);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        MoreData moreData = moreDataList.get(position);
        Bundle bundle=new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,moreData);
        startNextActivity(bundle,MoreActivity.class);
    }
}
