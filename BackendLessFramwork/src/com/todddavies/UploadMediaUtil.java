package com.todddavies;

import android.app.Activity;

import com.todddavies.components.progressbar.R;
import com.todddavies.dialog_fragments.MultipalImageUploadDialog;
import com.todddavies.dialog_fragments.UploadImageDialog;
import com.todddavies.dialog_fragments.UploadVideoDialog;
import com.todddavies.listeners.OnUploadImage;
import com.todddavies.models.SlideImage;
import com.todddavies.models.SlideVideo;
import com.todddavies.utils.BackendLessUtil;

import java.util.List;

/**
 * Created by raghu on 8/11/16.
 */

public class UploadMediaUtil {
    public static void UploadImage(SlideImage slideImage, final OnUploadImage onUploadImage, Activity activity) {
        BackendLessUtil.initBackendLess(activity);
        if (slideImage.isUploaded()) {
            onUploadImage.onSuccess();
        } else {
            UploadImageDialog uploadImageDialog = UploadImageDialog.getInstance(slideImage, onUploadImage);
            uploadImageDialog.show(activity.getFragmentManager(), activity.getString(R.string.uploading_image));
            uploadImageDialog.setCancelable(false);
        }
    }

    public static void UploadVideo(SlideVideo slideVideo, final OnUploadImage onUploadImage, Activity activity) {
        BackendLessUtil.initBackendLess(activity);
        UploadVideoDialog uploadVideoDialog = UploadVideoDialog.getInstance(slideVideo, onUploadImage);
        uploadVideoDialog.show(activity.getFragmentManager(), activity.getString(R.string.uploading_video));
        uploadVideoDialog.setCancelable(false);
    }

    public static void UploadMultipleImage(List<SlideImage> slideImageList, MultipalImageUploadDialog.OnUploadListener onUploadImage, Activity activity) {
        BackendLessUtil.initBackendLess(activity);
        MultipalImageUploadDialog multipalImageUploadDialog = MultipalImageUploadDialog.getInstance(slideImageList, onUploadImage);
        multipalImageUploadDialog.show(activity.getFragmentManager(), activity.getString(R.string.uploading_image));
        multipalImageUploadDialog.setCancelable(false);
    }
}
