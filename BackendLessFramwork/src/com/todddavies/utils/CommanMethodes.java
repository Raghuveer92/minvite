package com.todddavies.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;

/**
 * Created by raghu on 8/11/16.
 */
public class CommanMethodes {
    public static Bitmap getBitmapFromUri(Context ctx, Uri imageUri) {
        try {
            return MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), imageUri);
        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }
    public static Bitmap getResizedBitmap(Bitmap image) {
        int maxSize = 512;
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static Uri getUriFromPath(String imagePath) throws Exception{
        File file = new File(imagePath);
        Uri uri = Uri.fromFile(file);
        return uri;
    }

    public static Bitmap getBitmapFromPath(Context context,String imgPath) {
        Uri uri = Uri.fromFile(new File(imgPath));
        return getBitmapFromUri(context,uri);
    }
    public static Bitmap getResizedBitmap(Context context, String imgPath) throws Exception {
        Uri uri = getUriFromPath(imgPath);
        Bitmap bitmap=getBitmapFromUri(context,uri);
        return getResizedBitmap(bitmap);
    }
}
