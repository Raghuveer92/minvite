package com.minvite.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.models.FeaturesList;
import com.minvite.models.FeatureEntry;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.BackendLessUtils;
import simplifii.framework.utility.ListUtil;
import simplifii.framework.utility.Util;

/**
 * Created by robin.bansal on 11/7/16.
 */
public class AddPagesFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener{

    GridView gvFeatures;
    private List<FeatureEntry> list;
    private CustomListAdapter<FeatureEntry> adapter;

    @Override
    public void initViews() {
        adapter = new CustomListAdapter<>(getActivity(), R.layout.featue_row, list, this);
        gvFeatures = (GridView) findView(R.id.grid_id);
        gvFeatures.setAdapter(adapter);
        gvFeatures.setOnItemClickListener(this);
        setFeaturesList();
    }

    private void setFeaturesList(){
        list = new ArrayList<>();
        HttpParamObject httpParamObject = BackendLessUtils.getBaseHttpParamObject();
        showToast(AppConstants.PAGE_URL.FEATURES_PAGE_URL);
        httpParamObject.setUrl(AppConstants.PAGE_URL.FEATURES_PAGE_URL);
        httpParamObject.setClassType(FeaturesList.class);
        executeTask(AppConstants.TASK_CODES.GET_FEATURES_LIST, httpParamObject);
    }

    @Override
    public void onPreExecute(int taskCode) {
        showProgressBar();
        super.onPreExecute(taskCode);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_FEATURES_LIST:
                if (response != null) {
                    FeaturesList listResponse = (FeaturesList) response;
                    List<FeatureEntry> features = listResponse.getData();
                    if(ListUtil.isNotEmpty(features)) {
                        list.clear();
                        list.addAll(features);
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
                showToast("Could Not Refresh Features");
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_add_pages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.getTvFeatureName().setText(list.get(position).getName());
        Util.loadImageInImageView(getActivity(),list.get(position).getIconUrl(),holder.getIvFeatureIcon());
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FeatureEntry f = list.get(position);
    }

    private class ViewHolder {
        private TextView tvFeatureName;
        private ImageView ivFeatureIcon;

        public TextView getTvFeatureName() {
            return tvFeatureName;
        }

        public void setTvFeatureName(TextView tvFeatureName) {
            this.tvFeatureName = tvFeatureName;
        }

        public ImageView getIvFeatureIcon() {
            return ivFeatureIcon;
        }

        public void setIvFeatureIcon(ImageView ivFeatureIcon) {
            this.ivFeatureIcon = ivFeatureIcon;
        }

        ViewHolder(View convertView) {
            tvFeatureName = (TextView) convertView.findViewById(R.id.grid_text);
            ivFeatureIcon = (ImageView) convertView.findViewById(R.id.grid_image);
        }
    }
}
