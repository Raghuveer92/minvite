package com.minvite.models.update;

import com.minvite.models.BaseApi;

import java.util.List;

/**
 * Created by raghu on 6/12/16.
 */

public class UpdateResponse extends BaseApi {
    List<UpdateData> data;

    public List<UpdateData> getData() {
        return data;
    }

    public void setData(List<UpdateData> data) {
        this.data = data;
    }
}
