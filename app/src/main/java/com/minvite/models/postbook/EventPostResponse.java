package com.minvite.models.postbook;

import com.minvite.models.BaseApi;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raghu on 17/9/16.
 */
public class EventPostResponse extends BaseApi implements Serializable{
    List<EventPost> data;

    public List<EventPost> getData() {
        return data;
    }
}
