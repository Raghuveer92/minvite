package com.minvite.models.chat;

import java.io.Serializable;

/**
 * Created by raghu on 29/11/16.
 */

public class FCM_ChatModel implements Serializable {
    String userToken;
    ChatModel chatModel;

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public ChatModel getChatModel() {
        return chatModel;
    }

    public void setChatModel(ChatModel chatModel) {
        this.chatModel = chatModel;
    }
}
