package com.todddavies.listeners;

/**
 * Created by raghu on 8/11/16.
 */

public interface OnUploadImage {
    void onSuccess();
    void onFailed();
}
