package com.minvite.enums;

/**
 * Created by raghu on 28/11/16.
 */

public enum  ChatStatus {
    PENDING(0),DELIVERED(1),READ(2),INCOMING(3);
    int status;

    ChatStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
    public static ChatStatus findByStatus(int status) {
        for (ChatStatus chatStatus : ChatStatus.values()) {
            if (chatStatus.status == status)
                return chatStatus;
        }
        return PENDING;
    }
}
