package com.minvite.fragments;

import android.text.TextUtils;
import android.widget.ImageView;

import com.minvite.R;
import com.minvite.models.album.AlbumData;
import com.squareup.picasso.Picasso;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by raghu on 18/11/16.
 */

public class AlbumPhotoFragment extends BaseFragment {
    private AlbumData albumData;

    public static AlbumPhotoFragment getInstance(AlbumData albumData) {
        AlbumPhotoFragment albumPhotoFragment=new AlbumPhotoFragment();
        albumPhotoFragment.albumData = albumData;
        return albumPhotoFragment;
    }

    @Override
    public void initViews() {
        if(albumData!=null){
            ImageView imageView= (ImageView) findView(R.id.iv_album);
            String imageUrl = albumData.getImageUrl();
            if(!TextUtils.isEmpty(imageUrl)){
                Picasso.with(getActivity()).load(imageUrl).into(imageView);
            }
            setText(albumData.getName(),R.id.tv_album_title);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_albm_photo;
    }
}
