package com.minvite.fragments.sub_event;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.minvite.R;
import com.minvite.activities.FullScreenVideoActivity;
import com.minvite.activities.postbook.SubEventsDetailActivity;
import com.minvite.activities.SendInvitationActivity;
import com.minvite.fragments.base.SubEventBaseFragment;
import com.minvite.models.user.UserProfile;
import com.minvite.models.invitation.EventData;
import com.minvite.models.invitation.EventTypeModel;
import com.minvite.models.postbook.Comment;
import com.minvite.models.sub_events.SubEventData;

import java.util.List;

import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import nz.co.delacour.exposurevideoplayer.FullScreenClickListener;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by raghu on 11/10/16.
 */

public class ImageVideoFragment extends SubEventBaseFragment implements OnLikeListener {

    private EventData eventData;
    private LikeButton likeButton;
    private final int REQ_CODE_GET_DETAIL = 12;

    public static ImageVideoFragment getInstance(SubEventData subEventData, EventData eventData) {
        ImageVideoFragment imageVideoFragment = new ImageVideoFragment();
        imageVideoFragment.subEventData = subEventData;
        imageVideoFragment.eventData = eventData;
        return imageVideoFragment;
    }

    @Override
    public void initViews() {
        likeButton = (LikeButton) findView(R.id.tv_like);
        if (subEventData != null) {
            setData(subEventData);

            if (eventData != null) {
                getStatus(subEventData);
            }
        }
        setOnClickListener(R.id.btn_invite, R.id.btn_accept, R.id.btn_may_be, R.id.btn_reject, R.id.view_detail, R.id.iv_edit);
        likeButton.setOnLikeListener(this);
    }

    private void setLikesComments(SubEventData subEventData, int tvLike, int tvComment) {
        List<UserProfile> likes = subEventData.getLikes();
        List<Comment> comments = subEventData.getComments();
        if (likes != null) {
            setText("" + likes.size(), tvLike);
        }
        if (comments != null) {
            setText("" + comments.size(), tvComment);
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.btn_invite:
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventData);
                startNextActivity(bundle, SendInvitationActivity.class);
                break;
            case R.id.view_detail:
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, subEventData);
                Intent intent = new Intent(getActivity(), SubEventsDetailActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQ_CODE_GET_DETAIL);
                break;
        }
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_image_video_subevent;
    }


    @Override
    protected void getSubEvents(String params) {

    }

    @Override
    protected void setButtonStatus(String status) {
        showVisibility(R.id.lay_status);
        hideVisibility(R.id.lay_btns);
        switch (status) {
            case AppConstants.INVITATION_STATUS.ACCEPTED:
                setText(getString(R.string.invite_accepted_status), R.id.tv_status);
                break;
            case AppConstants.INVITATION_STATUS.REJECTED:
                setText(getString(R.string.invite_rejected_status), R.id.tv_status);
                break;
            case AppConstants.INVITATION_STATUS.MAY_BE:
                setText(getString(R.string.invite_may_be_status), R.id.tv_status);
                break;
            default:
                hideVisibility(R.id.lay_status);
                showVisibility(R.id.lay_btns);
                break;
        }
    }


    private void setData(SubEventData subEventData) {
        this.subEventData=subEventData;
        setText(subEventData.getName(), R.id.tv_wedding_day);
        setText(subEventData.getLocation(), R.id.tv_venue_name);
        UserProfile user = subEventData.getUser();
        if (user != null) {
            setText(user.getMobile(), R.id.tv_contact);
            setText(user.getName(), R.id.tv_contact_name);
        }
        setLocation(subEventData);
        long datetime = subEventData.getDatetime();
        if (datetime > 0) {
            String date = Util.getDate(datetime, "dd MM yyyy");
            String time = Util.getDate(datetime, "hh:mm a");
            setText(date, R.id.tv_date);
            setText(time, R.id.tv_time_stamp);
        }
        EventTypeModel eventTypeModel = subEventData.getType();
        if (eventTypeModel != null) {
            String type = eventTypeModel.getType();
            switch (type) {
                case AppConstants.EVENT_TYPES.IMAGE:
                    String imageUrl = subEventData.getImageUrl();
                    loadImage(imageUrl, R.id.iv_event_detail);
                    hideVisibility(R.id.videoview);
                    break;
                case AppConstants.EVENT_TYPES.VIDEO:
                    String videoUrl = subEventData.getVideoUrl();
                    loadVideo(videoUrl);
                    hideVisibility(R.id.iv_event_detail);
                    break;
            }
        }
        setLikesComments(subEventData, R.id.tv_like_count, R.id.tv_comment_count);
        likeButton.setLiked(checkMyLike(subEventData.getLikes()));
    }

    private void setLocation(final SubEventData subEventData) {
        final String lat = subEventData.getLatitude();
        final String lng = subEventData.getLongitude();
        if ((!TextUtils.isEmpty(lat)) && (!TextUtils.isEmpty(lng))) {
            showVisibility(R.id.lay_location);
            findView(R.id.lay_location).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Uri mapUri = Uri.parse("geo:0,0?q=" + lat + "," + lng + "(" + subEventData.getName() + ")");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    } catch (Exception e) {
                        showToast("Cannot open Google Map..!");
                    }
                }
            });
        }
    }

    private void loadVideo(final String videoUrl) {
        ExposureVideoPlayer exposureVideoPlayer = (ExposureVideoPlayer) findView(R.id.videoview);
        exposureVideoPlayer.init(getActivity());
        exposureVideoPlayer.setFullScreen(false);
        exposureVideoPlayer.setAutoPlay(false);
        try {
            exposureVideoPlayer.setVideoSource(videoUrl);
        } catch (Exception e) {

        }
        exposureVideoPlayer.setOnFullScreenClickListener(new FullScreenClickListener() {
            @Override
            public void onToggleClick(boolean isFullscreen) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_KEYS.VIDEO_URL, videoUrl);
                startNextActivity(bundle, FullScreenVideoActivity.class);
            }
        });
    }


    @Override
    public void liked(LikeButton likeButton) {
        setLike(subEventData, R.id.tv_like_count);
    }


    @Override
    public void unLiked(LikeButton likeButton) {
        setLike(subEventData, R.id.tv_like_count);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_GET_DETAIL && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                SubEventData subEventData = (SubEventData) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                if (subEventData != null) {
                    setData(subEventData);
                }
            }
        }
    }
}
