package com.minvite.holders;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.models.products.ProductData;
import com.minvite.models.products.ProductsResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raghu on 17/11/16.
 */

public class ProductsHolder extends BaseHolder {

    private final LinearLayout linearLayoutMain;
    private final LayoutInflater layoutInflater;

    public ProductsHolder(View itemView) {
        super(itemView);
        linearLayoutMain = (LinearLayout) findView(R.id.lay_main);
        layoutInflater=LayoutInflater.from(context);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ProductsResponse productsResponse = (ProductsResponse) obj;
        List<ProductData> productDataList = productsResponse.getData();
        if(productDataList!=null){
            setData(productDataList);
        }
    }

    private void setData(List<ProductData> productDataList) {
        List<ProductsResponse> productsResponseList=new ArrayList<>();
        int size=productDataList.size();
        int count=0;
        while (count<size){
            if(size-count>1){
                ProductsResponse productsResponse=new ProductsResponse();
                List<ProductData> dataList=new ArrayList<>();
                dataList.add(productDataList.get(count++));
                dataList.add(productDataList.get(count++));
                productsResponse.setData(dataList);
                productsResponseList.add(productsResponse);
            }else {
                ProductsResponse productsResponse=new ProductsResponse();
                List<ProductData> dataList=new ArrayList<>();
                dataList.add(productDataList.get(count++));
                productsResponse.setData(dataList);
                productsResponseList.add(productsResponse);
            }
        }
      setPropucts(productsResponseList);
    }

    private void setPropucts(List<ProductsResponse> productsResponseList) {
        for(ProductsResponse productsResponse:productsResponseList){

            View inflate = layoutInflater.inflate(R.layout.row_product_container, null);
            LinearLayout containerProduct= (LinearLayout) inflate.findViewById(R.id.linear_layout);

            List<ProductData> dataList = productsResponse.getData();
            if(dataList!=null){
                for(ProductData productData:dataList){
                    View viewInner = layoutInflater.inflate(R.layout.row_product, containerProduct, false);
                    ImageView imageView= (ImageView) viewInner.findViewById(R.id.category_background_image);
                    TextView textView= (TextView) viewInner.findViewById(R.id.category_name);

                    String imageUrl = productData.getImageUrl();
                    if(!TextUtils.isEmpty(imageUrl)){
                        Picasso.with(context).load(imageUrl.trim()).into(imageView);
                    }

                    String name = productData.getName();
                    if(!TextUtils.isEmpty(name)){
                        textView.setText(name);
                    }
                    containerProduct.addView(viewInner);
                }
            }
            linearLayoutMain.addView(inflate);
        }
    }
}
