package com.minvite.models.postbook;

import com.minvite.models.BaseAdapterModel;
import com.minvite.models.invitation.EventData;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 17/9/16.
 */
public class WriteSomeThingModel extends BaseAdapterModel {
    EventData eventData;

    public WriteSomeThingModel(EventData eventData) {
        this.eventData = eventData;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.WRITE_SOMETHING;
    }

    public EventData getEventData() {
        return eventData;
    }
}
