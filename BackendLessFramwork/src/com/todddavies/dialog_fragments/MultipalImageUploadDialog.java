package com.todddavies.dialog_fragments;


import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.todddavies.ListAdapters.CustomListAdapter;
import com.todddavies.ListAdapters.CustomListAdapterInterface;
import com.todddavies.Main;
import com.todddavies.ProgressWheel;
import com.todddavies.components.progressbar.R;
import com.todddavies.models.SlideImage;
import com.todddavies.utils.BackendLessUtil;
import com.todddavies.utils.CommanMethodes;

import java.io.File;
import java.util.List;


/**
 * Created by raghu on 7/11/16.
 */

public class MultipalImageUploadDialog extends DialogFragment implements CustomListAdapterInterface {
    List<SlideImage> slideImages ;
    OnUploadListener onUploadListener;
    private CustomListAdapter customListAdapter;

    public static MultipalImageUploadDialog getInstance(List<SlideImage> slideImages, OnUploadListener onUploadListener) {
        MultipalImageUploadDialog uploadImageDialog=new MultipalImageUploadDialog();
        uploadImageDialog.slideImages=slideImages;
        uploadImageDialog.onUploadListener=onUploadListener;
        return uploadImageDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_upload_multipal_images, null);
        getDialog().setTitle("Uploading Images...");
        GridView gridView = (GridView) view.findViewById(R.id.grid_images);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_image_upload, slideImages, this);
        gridView.setAdapter(customListAdapter);
        startUploadImageTask();
        return view;
    }



    private void startUploadImageTask() {
//        customListAdapter.notifyDataSetChanged();
        for (SlideImage slideImage:slideImages){
            if(!slideImage.isUploaded()){
                uploadImage(slideImage);
                return;
            }
        }
        onUploadListener.onSuccess();
        dismissAllowingStateLoss();
    }

    private void uploadImage(final SlideImage slideImage) {
        String imgPath = slideImage.getImgPath();
        if(!TextUtils.isEmpty(imgPath)){
            Uri uri= Uri.fromFile(new File(imgPath));
            Bitmap bitmap = CommanMethodes.getBitmapFromUri(getActivity(), uri);
            BackendLessUtil.uploadImage(bitmap, new BackendLessUtil.OnResponseListener() {
                @Override
                public void onSuccess(String fileUrl) {
                    slideImage.setImageUrl(fileUrl);
                    startUploadImageTask();
                }

                @Override
                public void onFailed(String exception) {
                    onUploadListener.onFailed();
                    dismissAllowingStateLoss();
                }
            });
        }
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        SlideImage slideImage = slideImages.get(position);
        setRefreshData(slideImage,holder);
        holder.setData(slideImage);
        return convertView;
    }

    private void setRefreshData(final SlideImage slideImage, final Holder holder) {
        final CountDownTimer countDownTimer = new CountDownTimer(61 * 1000, 1000) {
            @Override
            public void onTick(long l) {
                if(slideImage.isUploaded()){
                    holder.setData(slideImage);
                    cancel();
                }
            }

            @Override
            public void onFinish() {
                start();
            }
        };
        countDownTimer.start();
    }


    class Holder {
        ImageView circularMusicProgressBar;
        ImageView ivSuccess;
        ProgressWheel progressWheel;


        public Holder(View view) {
            circularMusicProgressBar= (ImageView) view.findViewById(R.id.album_art);
            ivSuccess= (ImageView) view.findViewById(R.id.iv_success);
            progressWheel= (ProgressWheel) view.findViewById(R.id.pw_spinner);
            Main.styleRandom(progressWheel,getActivity());
        }

        public void setData(SlideImage slideImage) {
            if (slideImage.isUploaded()) {
                progressWheel.stopSpinning();
                ivSuccess.setVisibility(View.VISIBLE);
                String imgPath = slideImage.getImgPath();
                if (!TextUtils.isEmpty(imgPath)) {
                    try {
                        Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(getActivity(), imgPath);
                        circularMusicProgressBar.setImageBitmap(resizedBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    Picasso.with(getActivity()).load(slideImage.getImageUrl()).into(circularMusicProgressBar);
                }
            } else {
                progressWheel.startSpinning();
                ivSuccess.setVisibility(View.GONE);
                String imgPath = slideImage.getImgPath();
                if (!TextUtils.isEmpty(imgPath)) {
                    try {
                        Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(getActivity(), imgPath);
                        circularMusicProgressBar.setImageBitmap(resizedBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    public interface OnUploadListener{
        void onSuccess();
        void onFailed();
    }

}
