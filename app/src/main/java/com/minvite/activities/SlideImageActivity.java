package com.minvite.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.minvite.R;
import com.minvite.fragments.postFragments.FullImageFragment;
import com.minvite.models.BaseAdapterModel;

import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class SlideImageActivity extends BaseActivity implements CustomPagerAdapter.PagerAdapterInterface {

    private List<String> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setStatusBarColor(R.color.black);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_image);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        ViewPager viewPager= (ViewPager) findViewById(R.id.pager_slide_images);
        images= (List<String>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        int selectedPosition=bundle.getInt(AppConstants.BUNDLE_KEYS.SELECTED_POSITION);
        if(images!=null){
            CustomPagerAdapter customPagerAdapter=new CustomPagerAdapter(getSupportFragmentManager(),images,this);
            viewPager.setAdapter(customPagerAdapter);
            viewPager.setClipToPadding(false);
            viewPager.setCurrentItem(selectedPosition);
            int margin = Util.convertDpToPixels(8, this);
            viewPager.setPageMargin(margin);
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return FullImageFragment.getInstance(images.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return "";
    }
}
