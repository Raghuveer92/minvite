package com.minvite.models.album;

import com.minvite.models.BaseApi;

import java.util.List;

/**
 * Created by raghu on 18/11/16.
 */

public class AlbumResponse  extends BaseApi{
    List<AlbumData> data;

    public List<AlbumData> getData() {
        return data;
    }
}
