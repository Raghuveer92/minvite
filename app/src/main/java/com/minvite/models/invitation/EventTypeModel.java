package com.minvite.models.invitation;

import java.io.Serializable;

/**
 * Created by Admin on 20-Jul-16.
 */
public class EventTypeModel implements Serializable {
    String type;
    String objectId;

    public String getType() {
        return type;
    }

    public String getObjectId() {
        return objectId;
    }
}
