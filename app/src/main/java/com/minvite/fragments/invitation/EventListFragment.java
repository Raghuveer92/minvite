package com.minvite.fragments.invitation;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.activities.event.SubEventsActivity;
import com.minvite.activities.postbook.PostBookActivity;
import com.minvite.models.invitation.EventData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by Admin on 20-Jul-16.
 */
public class EventListFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<EventData> eventDatas;
    List<EventData> eventListMain=new ArrayList<>();
    private CustomListAdapter listAdapter;

    public static EventListFragment getInstance(List<EventData> eventDatas) {
        EventListFragment eventListFragment = new EventListFragment();
        eventListFragment.eventDatas = eventDatas;
        eventListFragment.eventListMain.addAll(eventDatas);
        return eventListFragment;
    }

    @Override
    public void initViews() {
        ListView listView = (ListView) findView(R.id.list_invitations);
        listView.setEmptyView(findView(R.id.emptyView));
        listAdapter = new CustomListAdapter(getActivity(), R.layout.invite_row, eventDatas, this);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_invitation_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final EventData eventData = eventDatas.get(position);
        holder.tvTitle.setText(eventData.getName());
        holder.tvSubTitle.setText(eventData.getLocation());
        String datetime = eventData.getDatetime();
        if(!TextUtils.isEmpty(datetime)){
            String date = Util.getDate(Long.parseLong(datetime), "dd-MM-yyyy");
            holder.tvDate.setText(date);
        }
        holder.tvTime.setText(eventData.getTime());
        if (!"".equals(eventData.getImageUrl()) && eventData.getImageUrl() != null)
            Picasso.with(getActivity()).load(eventData.getImageUrl()).placeholder(R.drawable.photo_place_holder).into(holder.imageView);

        if(eventData.isAdmin()){
            holder.ivAdmin.setVisibility(View.VISIBLE);
        }else {
            holder.ivAdmin.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Intent intent = new Intent(getActivity(), PostBookActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.TYPE,AppConstants.TYPE.LIST);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventDatas.get(position));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void setFilterList(boolean isAdmin) {
        if(isAdmin){
            List<EventData> adminList = getAdminList();
            eventDatas.clear();
            eventDatas.addAll(adminList);
            listAdapter.notifyDataSetChanged();
        }else {
            eventDatas.clear();
            eventDatas.addAll(eventListMain);
            listAdapter.notifyDataSetChanged();
        }
    }

    private List<EventData> getAdminList() {
        List<EventData> eventDatas=new ArrayList<>();
        for(EventData eventData:eventListMain){
            if(eventData.isAdmin()){
                eventDatas.add(eventData);
            }
        }
        return eventDatas;
    }

    class Holder {
        TextView tvTitle, tvSubTitle, tvDate, tvTime;
        ImageView imageView,ivAdmin;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_invite_coupleName);
            tvSubTitle = (TextView) view.findViewById(R.id.tv_invite_venueName);
            tvDate = (TextView) view.findViewById(R.id.tv_invite_date);
            tvTime = (TextView) view.findViewById(R.id.tv_invite_time);
            imageView = (ImageView) view.findViewById(R.id.iv_couple_pic);
            ivAdmin= (ImageView) view.findViewById(R.id.iv_bookmark);
        }
    }
}
