package com.minvite.models.time_line;

import com.minvite.models.BaseApi;

import java.util.List;

/**
 * Created by raghu on 18/11/16.
 */

public class TimeLineResponse extends BaseApi {
    List<TimeLineData> data;

    public List<TimeLineData> getData() {
        return data;
    }
}
