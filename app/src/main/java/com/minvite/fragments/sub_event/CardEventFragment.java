package com.minvite.fragments.sub_event;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.minvite.R;
import com.minvite.activities.postbook.SubEventsDetailActivity;
import com.minvite.fragments.base.SubEventBaseFragment;
import com.minvite.models.user.UserProfile;
import com.minvite.models.invitation.EventData;
import com.minvite.models.postbook.Comment;
import com.minvite.models.sub_events.SubEventData;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 18/11/16.
 */

public class CardEventFragment extends SubEventBaseFragment implements OnLikeListener {

    private EventData eventData;
    private LikeButton likeButton;

    public static CardEventFragment getInstance(SubEventData subEventData, EventData eventData) {
        CardEventFragment cardEventFragment = new CardEventFragment();
        cardEventFragment.subEventData = subEventData;
        cardEventFragment.eventData = eventData;
        return cardEventFragment;
    }

    @Override
    public void initViews() {
        likeButton = (LikeButton) findView(R.id.tv_like);
        hideVisibility(R.id.lay_btns);
        if (subEventData != null) {
            setData(subEventData);

            if (eventData != null) {
                getStatus(subEventData);
            }
        }
        setOnClickListener(R.id.btn_accept, R.id.btn_may_be, R.id.btn_reject, R.id.lay_like_comment, R.id.iv_edit);
        likeButton.setOnLikeListener(this);
    }

    private void setData(SubEventData subEventData) {
        this.subEventData=subEventData;
        String imageUrl = subEventData.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)) {
            loadImage(imageUrl);
        }
        setLikesComments(subEventData, R.id.tv_like_count, R.id.tv_comment_count);
        setText(subEventData.getName(), R.id.tv_caption);
        likeButton.setLiked(checkMyLike(subEventData.getLikes()));
    }

    private void setLikesComments(SubEventData subEventData, int tvLike, int tvComment) {
        List<UserProfile> likes = subEventData.getLikes();
        List<Comment> comments = subEventData.getComments();
        if (likes != null) {
            setText("" + likes.size(), tvLike);
        }
        if (comments != null) {
            setText("" + comments.size(), tvComment);
        }
    }

    private void loadImage(String imageUrl) {
        ImageView imageView = (ImageView) findView(R.id.image_view);
        Picasso.with(getActivity()).load(imageUrl).into(imageView);
    }

    @Override
    public void liked(LikeButton likeButton) {
        setLike(subEventData, R.id.tv_like_count);
    }


    @Override
    public void unLiked(LikeButton likeButton) {
        setLike(subEventData, R.id.tv_like_count);
    }

    @Override
    protected void getSubEvents(String params) {

    }

    @Override
    protected void setButtonStatus(String status) {
        showVisibility(R.id.lay_status);
        hideVisibility(R.id.lay_btns);
        switch (status) {
            case AppConstants.INVITATION_STATUS.ACCEPTED:
                setText(getString(R.string.invite_accepted_status), R.id.tv_status);
                break;
            case AppConstants.INVITATION_STATUS.REJECTED:
                setText(getString(R.string.invite_rejected_status), R.id.tv_status);
                break;
            case AppConstants.INVITATION_STATUS.MAY_BE:
                setText(getString(R.string.invite_may_be_status), R.id.tv_status);
                break;
            default:
                hideVisibility(R.id.lay_status);
                showVisibility(R.id.lay_btns);
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_GET_DETAIL && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                SubEventData subEventData = (SubEventData) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                if (subEventData != null) {
                    setData(subEventData);
                }
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_event_card;
    }
}
