package com.minvite.activities.updates;

import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.minvite.R;
import com.minvite.models.invitation.EventData;
import com.minvite.models.update.UpdateData;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class AddNewUpdateActivity extends BaseActivity {

    private EventData eventData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_update);
        initToolBar(getString(R.string.add_new_update));
        setOnClickListener(R.id.btn_submit);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        eventData = (EventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                validateData();
                break;
        }
    }

    private void validateData() {
        TextInputLayout tilTitle = (TextInputLayout) findViewById(R.id.til_title);
        TextInputLayout tilSubTitle = (TextInputLayout) findViewById(R.id.til_sub_title);
        String title = tilTitle.getEditText().getText().toString();
        if (TextUtils.isEmpty(title.trim())) {
            tilTitle.setError("Title cannot be empty");
            return;
        } else {
            tilTitle.setError(null);
            tilTitle.setErrorEnabled(false);
        }
        String subTitle = tilSubTitle.getEditText().getText().toString();
        if (TextUtils.isEmpty(subTitle.trim())) {
            tilSubTitle.setError("Description cannot be empty");
            return;
        } else {
            tilSubTitle.setError(null);
            tilSubTitle.setErrorEnabled(false);
        }
        submitData(title, subTitle);
    }

    private void submitData(String title, String subTitle) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE);
        httpParamObject.setClassType(UpdateData.class);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("eventId", eventData.getObjectId());
            jsonObject.put("title", title);
            jsonObject.put("subTitle", subTitle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        executeTask(AppConstants.TASK_CODES.ADD_UPDATE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.ADD_UPDATE:
                UpdateData updateData= (UpdateData) response;
                if(updateData!=null&&!updateData.isError()){
                    showToast(R.string.success_post);
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }
}
