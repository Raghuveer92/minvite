package simplifii.framework.utility;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.File;
import java.util.UUID;

import simplifii.framework.asyncmanager.HttpParamObject;

/**
 * Created by robin.bansal on 6/7/16.
 */
public class BackendLessUtils {

    public static HttpParamObject getBaseHttpParamObject(){
        HttpParamObject baseObject = new HttpParamObject();
        baseObject.getHeaders().put(AppConstants.BACKENDLESS_CONSTANTS.APPLICATION_ID_TEXT,AppConstants.BACKENDLESS_CONSTANTS.APPLICATION_ID_VALUE);
        baseObject.getHeaders().put(AppConstants.BACKENDLESS_CONSTANTS.SECRET_KEY_TEXT,AppConstants.BACKENDLESS_CONSTANTS.SECRET_KEY_VALUE);
        return baseObject;
    }
}
