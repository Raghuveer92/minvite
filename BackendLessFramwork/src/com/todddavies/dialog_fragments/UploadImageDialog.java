package com.todddavies.dialog_fragments;

import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.todddavies.Main;
import com.todddavies.ProgressWheel;
import com.todddavies.components.progressbar.R;
import com.todddavies.listeners.OnUploadImage;
import com.todddavies.models.SlideImage;
import com.todddavies.utils.BackendLessUtil;
import com.todddavies.utils.CommanMethodes;

import java.io.File;


/**
 * Created by raghu on 7/11/16.
 */

public class UploadImageDialog extends DialogFragment {
    SlideImage slideImage;
    OnUploadImage onUploadListener;
    private Holder holder;
    private ImageLoader imageLoader;

    public static UploadImageDialog getInstance(SlideImage slideImage, OnUploadImage onUploadListener) {
        UploadImageDialog uploadImageDialog = new UploadImageDialog();
        uploadImageDialog.slideImage = slideImage;
        uploadImageDialog.onUploadListener = onUploadListener;
        return uploadImageDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_upload_image, null);
        getDialog().setTitle("Uploading Images...");
        imageLoader = ImageLoader.getInstance();
        holder = new Holder(view);
        holder.setData(slideImage);
        startUploadImageTask();
        return view;
    }

    private void startUploadImageTask() {
        if (!slideImage.isUploaded()) {
            uploadImage(slideImage);
            return;
        }
        onUploadListener.onSuccess();
        dismissAllowingStateLoss();

    }

    private void uploadImage(final SlideImage slideImage) {
        if (slideImage.isUploaded()) {
            onUploadListener.onSuccess();
        }
        String imgPath = slideImage.getImgPath();
        if (!TextUtils.isEmpty(imgPath)) {
            Uri uri = Uri.fromFile(new File(imgPath));
            Bitmap bitmap = CommanMethodes.getBitmapFromUri(getActivity(), uri);
            BackendLessUtil.uploadImage(bitmap, new BackendLessUtil.OnResponseListener() {
                @Override
                public void onSuccess(String fileUrl) {
                    slideImage.setImageUrl(fileUrl);
                    startUploadImageTask();
                }

                @Override
                public void onFailed(String exception) {
                    onUploadListener.onFailed();
                    dismissAllowingStateLoss();

                }
            });
        } else {
            if (slideImage.getBitmap() != null) {
                BackendLessUtil.uploadImage(slideImage.getBitmap(), new BackendLessUtil.OnResponseListener() {
                    @Override
                    public void onSuccess(String fileUrl) {
                        slideImage.setImageUrl(fileUrl);
                        startUploadImageTask();
                    }

                    @Override
                    public void onFailed(String exception) {
                        onUploadListener.onFailed();
                        dismiss();
                    }
                });
            } else {
                onUploadListener.onFailed();
            }
        }
    }


    class Holder {
        ImageView imageView;
        ImageView ivSuccess;
        ProgressWheel progressWheel;

        public Holder(View view) {
            imageView = (ImageView) view.findViewById(R.id.album_art);
            ivSuccess = (ImageView) view.findViewById(R.id.iv_success);
            progressWheel = (ProgressWheel) view.findViewById(R.id.pw_spinner);
            Main.styleRandom(progressWheel, getActivity());
        }

        public void setData(SlideImage slideImage) {
            if (slideImage.isUploaded()) {
                progressWheel.stopSpinning();
                ivSuccess.setVisibility(View.VISIBLE);
                String imgPath = slideImage.getImgPath();
                if (!TextUtils.isEmpty(imgPath)) {
                    try {
                        Uri uri = CommanMethodes.getUriFromPath(imgPath);
                        imageLoader.displayImage(uri.toString(), imageView);
                    } catch (Exception e) {
                        e.printStackTrace();
                        onUploadListener.onFailed();
                    }
                } else if (slideImage.getBitmap() != null) {
                    Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(slideImage.getBitmap());
                    imageView.setImageBitmap(resizedBitmap);
                } else {
                    String imageUrl = slideImage.getImageUrl();
                    if (!TextUtils.isEmpty(imageUrl))
                        Picasso.with(getActivity()).load(imageUrl).into(imageView);
                }
            } else {
                progressWheel.startSpinning();
                ivSuccess.setVisibility(View.GONE);
                String imgPath = slideImage.getImgPath();
                if (!TextUtils.isEmpty(imgPath)) {
                    try {
                        Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(getActivity(), imgPath);
                        imageView.setImageBitmap(resizedBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                        onUploadListener.onFailed();
                    }
                } else if (slideImage.getBitmap() != null) {
                    Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(slideImage.getBitmap());
                    imageView.setImageBitmap(resizedBitmap);
                }
            }
        }
    }
}
