package com.minvite.activities;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.content.ContentProvider;
import com.minvite.R;
import com.minvite.enums.ChatStatus;
import com.minvite.enums.ChatType;
import com.minvite.enums.MessageType;
import com.minvite.models.user.GetUserResponse;
import com.minvite.models.user.UserProfile;
import com.minvite.models.chat.ChatModel;
import com.minvite.models.chat.FCM_ChatModel;
import com.minvite.models.guest.GuestData;
import com.minvite.services.ChatService;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.ListAdapters.CustomCursorAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class ChatActivity extends BaseActivity implements CustomCursorAdapter.CustomCursorAdapterInterface {

    private GuestData guestData;
    private EditText editText;
    private ListView listView;
    private CustomCursorAdapter customCursorAdapter;
    private UserProfile userData;
    private TextView tvEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        editText = (EditText) findViewById(R.id.et_chat);
        tvEmpty= (TextView) findViewById(R.id.tv_empty);
        listView = (ListView) findViewById(R.id.list_chat);
        listView.setEmptyView(tvEmpty);
        customCursorAdapter = new CustomCursorAdapter(this, null, false, R.layout.row_chat, this);
        listView.setAdapter(customCursorAdapter);
        setOnClickListener(R.id.iv_send);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        guestData = (GuestData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if(guestData!=null){
            initToolBar(guestData.getGuestName());
            initLoader();
            getUserData(guestData);
        }
    }

    private void getUserData(GuestData guestData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.USERS);
        httpParamObject.addParameter("where", "mobile_no=" + guestData.getGuestMobile());
        httpParamObject.setClassType(GetUserResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_USER, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_USER:
                GetUserResponse getUserResponse = (GetUserResponse) response;
                if (getUserResponse != null) {
                    List<UserProfile> data = getUserResponse.getData();
                    if (data != null && data.size() > 0) {
                        setUserData(data.get(0));
                        return;
                    }
                }
                userNotRegistered();
                break;
        }
    }

    private void userNotRegistered() {
        tvEmpty.setText("This user is not on platform now..");
        tvEmpty.setVisibility(View.VISIBLE);
    }

    private void setUserData(UserProfile userProfile) {
        userData = userProfile;
        initToolBar(userData.getName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send:
                String msg = editText.getText().toString();
                if (!TextUtils.isEmpty(msg.trim())) {
                    sendMessage(msg.trim());
                    editText.setText("");
                }
                break;
        }
    }

    private void sendMessage(String message) {
        if (userData == null) {
            showToast("User Not Registered..!");
            return;
        }
        String fcmToken = userData.getFcmToken();
        if (TextUtils.isEmpty(fcmToken)) {
            showToast("This user need to update application..!");
            return;
        }
        ChatModel chatModel = ChatModel.getInstance(ChatStatus.PENDING, ChatType.OUT_GOING, guestData.getEventId(), message, MessageType.TEXT, null, guestData.getGuestMobile());
        Long aLong = chatModel.save();
        if (aLong > 0) {
            FCM_ChatModel fcm_chatModel = new FCM_ChatModel();
            fcm_chatModel.setChatModel(chatModel);
            fcm_chatModel.setUserToken(fcmToken);
            ChatService.sendMessage(this, fcm_chatModel);
        }
    }

    private void initLoader() {
        getSupportLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int arg0, Bundle cursor) {
                return new CursorLoader(ChatActivity.this,
                        ContentProvider.createUri(ChatModel.class, null),
                        null, "toUserNumber=? and eventId=?", new String[]{guestData.getGuestMobile(), guestData.getEventId()}, null
                );
            }

            @Override
            public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
                if (cursor != null) {
                    customCursorAdapter.swapCursor(cursor);
                }
            }

            @Override
            public void onLoaderReset(Loader<Cursor> arg0) {
                customCursorAdapter.swapCursor(null);
            }
        });
    }
    @Override
    public void bindView(View convertView, Context context, Cursor c) {
        Holder holder;
        holder = new Holder(convertView);
        ChatModel chatModel = new ChatModel();
        chatModel.loadFromCursor(c);
        String message = chatModel.getMessage();
        String time = chatModel.getFormatedTime();
        holder.tvMessage.setText(message);
        holder.tvTime.setText(time);
        int type = chatModel.getType();
        if (ChatType.findByStatus(type) == ChatType.INCOMING) {
            if(checkPrevious(c,chatModel)){
                holder.ivUserProfile.setVisibility(View.INVISIBLE);
            }else {
                holder.ivUserProfile.setVisibility(View.VISIBLE);
            }
            holder.ivMyProfile.setVisibility(View.INVISIBLE);
            holder.tvMessage.setGravity(Gravity.RIGHT);
            holder.tvTime.setGravity(Gravity.RIGHT);
            holder.cardView.setGravity(Gravity.RIGHT);
            if (userData != null) {
                String imageUrl = userData.getImageUrl();
                setImage(holder.ivUserProfile, imageUrl);
            }
        } else {
            if(checkPrevious(c,chatModel)){
                holder.ivMyProfile.setVisibility(View.INVISIBLE);
            }else {
                holder.ivMyProfile.setVisibility(View.VISIBLE);
            }

            holder.ivUserProfile.setVisibility(View.INVISIBLE);
            holder.tvMessage.setGravity(Gravity.LEFT);
            holder.tvTime.setGravity(Gravity.LEFT);
            holder.cardView.setGravity(Gravity.LEFT);

            UserProfile userProfile = UserProfile.getInstance();
            if (userProfile != null) {
                String imageUrl = userProfile.getImageUrl();
                setImage(holder.ivMyProfile, imageUrl);
            }
        }
    }

    private boolean checkPrevious(Cursor c, ChatModel chatModel) {
        if(c.isFirst()){
            return false;
        }
        if(c.moveToPrevious()){
            ChatModel chatModelnew = new ChatModel();
            chatModelnew.loadFromCursor(c);
            c.moveToNext();
            if(chatModel.getType()==chatModelnew.getType()){
                return true;
            }
        }
        return false;
    }

    private void setImage(ImageView imageView, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(this).load(imageUrl.trim()).placeholder(R.drawable.rahul).into(imageView);
        }
    }

    class Holder {
        TextView tvMessage, tvTime;
        ImageView ivMyProfile, ivUserProfile;
        LinearLayout cardView;

        public Holder(View view) {
            tvMessage = (TextView) view.findViewById(R.id.tv_message);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            ivMyProfile = (ImageView) view.findViewById(R.id.iv_my_profile);
            ivUserProfile = (ImageView) view.findViewById(R.id.iv_user_profile);
            cardView= (LinearLayout) view.findViewById(R.id.card_message);
        }
    }
}