package com.minvite.util;

/**
 * Created by robin.bansal on 6/7/16.
 */
public enum EventCat {

    PUBLIC("3"),PERSONAL("2"),PROFESSIONAL("1");
    private String type;
    public static final String KEY = "eventType";

    public String getType() {
        return type;
    }

    private EventCat(String type){
        this.type = type;
    }

    public static EventCat getEventCatFromValue(String type){
        for (EventCat eventCat : EventCat.values()) {
            if(eventCat.getType().equals(type)){
                return eventCat;
            }
        }
        return null;
    }
}
