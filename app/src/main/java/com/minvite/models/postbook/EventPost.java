package com.minvite.models.postbook;

import android.text.TextUtils;

import com.minvite.models.BaseAdapterModel;
import com.minvite.models.user.UserProfile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 17/9/16.
 */
public class EventPost extends BaseAdapterModel implements Serializable {
    String eventId;
    String imageUrl;
    String status;
    String videoUrl;
    List<Comment> comments = new ArrayList<>();
    List<UserProfile> likes = new ArrayList<>();
    UserProfile user;
    private String videoThumbnile;

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setLikes(List<UserProfile> likes) {
        this.likes = likes;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }

    public String getEventId() {
        return eventId;
    }

    public String getStatus() {
        return status;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public List<Comment> getComments() {
        if (comments != null) {
            return comments;
        } else {
            return new ArrayList<>();
        }
    }

    public List<UserProfile> getLikes() {
        return likes;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.POST_CARD;
    }

    public UserProfile getUser() {
        return user;
    }

    public String getImageUrl() {
        if (imageUrl != null) {
            String replace1 = imageUrl.replace("[", "");
            String replace2 = replace1.replace("]", "");
            return replace2;
        }
        return null;
    }

    public List<String> getImages() {
        List<String> list = new ArrayList<>();
        if (!TextUtils.isEmpty(imageUrl)) {
            String imageString = imageUrl;
            String replace1 = imageString.replace("[", "");
            String replace2 = replace1.replace("]", "");
            if (!TextUtils.isEmpty(replace2.trim())) {
                String[] strings = replace2.split(",");
                list = Arrays.asList(strings);
            }
        }
        return list;
    }

    public void setVideoThumbnile(String videoThumbnile) {
        this.videoThumbnile = videoThumbnile;
    }

    public String getVideoThumbnile() {
        return videoThumbnile;
    }
}
