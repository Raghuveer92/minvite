package com.minvite.fragments.sub_event;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.activities.event.SubEventsActivity;
import com.minvite.activities.postbook.PostBookActivity;
import com.minvite.fragments.base.SubEventBaseFragment;
import com.minvite.models.invitation.EventData;
import com.minvite.models.sub_events.SubEventData;
import com.minvite.models.sub_events.SubEventResponce;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.BackendLessUtils;

/**
 * Created by raghu on 26/11/16.
 */

public class SubEventsListFragment extends SubEventBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private EventData eventData;
    private ArrayList<SubEventData> subEventDataList=new ArrayList<>();
    private CustomListAdapter customListAdapter;

    public static SubEventsListFragment getInstance(EventData eventData) {
        SubEventsListFragment subEventsListFragment=new SubEventsListFragment();
        subEventsListFragment.eventData=eventData;
        return subEventsListFragment;
    }

    @Override
    public void initViews() {
        subEventDataList.clear();
        ListView listView= (ListView) findView(R.id.list_sub_events);
        customListAdapter=new CustomListAdapter(getActivity(),R.layout.row_sub_event,subEventDataList,this);
        listView.setAdapter(customListAdapter);
        listView.setOnItemClickListener(this);
        initToolBar(eventData.getName());
        if (eventData != null) {
            getMySubEvents(eventData);
        }
    }
    @Override
    protected void getSubEvents(String params) {
        HttpParamObject httpParamObject = BackendLessUtils.getBaseHttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SUB_EVENTS);
        httpParamObject.addParameter("where", params);
//        httpParamObject.addParameter("where", "eventId='"+eventData.getObjectId()+"'");
        httpParamObject.setClassType(SubEventResponce.class);
        executeTask(AppConstants.TASK_CODES.GET_SUB_EVENTS, httpParamObject);
    }
    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            showVisibility(R.id.emptyView);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_SUB_EVENTS:
                SubEventResponce subEventResponce = (SubEventResponce) response;

                if (subEventResponce.isError()) {
                    showToast(getString(R.string.server_error));
                    showVisibility(R.id.emptyView);
                    return;
                }
                ArrayList<SubEventData> eventDataList = subEventResponce.getData();
                if (eventDataList != null) {
                    if (eventDataList.size() > 0) {
                        setSubEvents(eventDataList);
                    } else {
                        showVisibility(R.id.emptyView);
                    }
                }
                break;
        }
    }

    private void setSubEvents(ArrayList<SubEventData> eventDataList) {
        subEventDataList.addAll(eventDataList);
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void setButtonStatus(String status) {

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_sub_event_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if(convertView==null){
            convertView=inflater.inflate(resourceID,parent,false);
            holder=new Holder(convertView);
            convertView.setTag(holder);
        }else {
            holder= (Holder) convertView.getTag();
        }
        SubEventData subEventData = subEventDataList.get(position);
        holder.tvTitle.setText(subEventData.getName());
        holder.tvTime.setText(subEventData.getTime());
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        final Intent intent = new Intent(getActivity(), SubEventsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventData);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SUB_EVENTS, subEventDataList);
        bundle.putInt(AppConstants.BUNDLE_KEYS.SELETED_SUB_EVENT,position);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    class Holder{
        TextView tvTitle,tvTime;

        public Holder(View view) {
            tvTitle= (TextView) view.findViewById(R.id.tv_title);
            tvTime= (TextView) view.findViewById(R.id.tv_time);
        }
    }
}
