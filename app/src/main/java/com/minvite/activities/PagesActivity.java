package com.minvite.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.minvite.R;
import com.minvite.fragments.AddPagesFragment;
import com.minvite.util.FragmentType;

import simplifii.framework.activity.BaseActivity;

public class PagesActivity extends BaseActivity {

    int currentFragment = FragmentType.ADD_PAGES.getType();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_pages);
        initToolBar(FragmentType.ADD_PAGES.getTitle());
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_fragment, getFragment(currentFragment, getIntent().getExtras())).commit();
    }

    public static Fragment getFragment(int fragmentType, Bundle bundle) {
        Fragment f = null;
        FragmentType fragTypeEnum = FragmentType.getFragmentTypeFromValue(fragmentType);
        switch (fragTypeEnum) {
            case ADD_PAGES:
                f = new AddPagesFragment();
                f.setArguments(bundle);
                break;
        }
        return f;
    }

}
