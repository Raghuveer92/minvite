package com.minvite.enums;

/**
 * Created by raghu on 29/11/16.
 */

public enum NotificationType {
    CHAT(1), BEACON_ON(2),BEACON_OFF(3);
    int type;

    NotificationType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static NotificationType findByStatus(int status) {
        for (NotificationType chatType : NotificationType.values()) {
            if (chatType.getType() == status)
                return chatType;
        }
        return CHAT;
    }}
