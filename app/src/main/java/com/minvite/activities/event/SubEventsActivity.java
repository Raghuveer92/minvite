package com.minvite.activities.event;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.minvite.R;
import com.minvite.fragments.sub_event.SubEventsCardFragment;
import com.minvite.fragments.sub_event.SubEventsListFragment;
import com.minvite.models.invitation.EventData;
import com.minvite.models.sub_events.SubEventData;

import java.util.ArrayList;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class SubEventsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_events);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        String type = bundle.getString(AppConstants.BUNDLE_KEYS.TYPE,"");
        int selectedPosition = bundle.getInt(AppConstants.BUNDLE_KEYS.SELETED_SUB_EVENT);
        EventData eventData = (EventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        ArrayList<SubEventData> subEventList= (ArrayList<SubEventData>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SUB_EVENTS);
        if (eventData != null) {
            switch (type) {
                case AppConstants.TYPE.LIST:
                    SubEventsListFragment subEventsListFragment = SubEventsListFragment.getInstance(eventData);
                    showFragment(subEventsListFragment);
                    break;
                default:
                    SubEventsCardFragment subEventsCardFragment = SubEventsCardFragment.getInstance(null, eventData, false, subEventList, selectedPosition);
                    showFragment(subEventsCardFragment);
            }
        }
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.lay_fragment_container, fragment).commit();
    }
}
