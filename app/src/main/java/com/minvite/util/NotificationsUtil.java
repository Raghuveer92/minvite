package com.minvite.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.minvite.R;
import com.minvite.activities.SplashActivity;
import com.minvite.enums.NotificationType;
import com.minvite.models.beacons.BeaconData;
import com.minvite.models.guest.GuestData;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 12/12/16.
 */

public class NotificationsUtil {
    public static void showChatNotification(Context context, GuestData guestData){
        Intent intent = new Intent(context, SplashActivity.class);
        Bundle bundle=new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.TYPE, NotificationType.CHAT.getType());
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,guestData);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_logo)
                .setContentTitle("Minvite Chat Message")
                .setContentText("You have received a new message")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
    public static void showBeaconNotification(Context context,BeaconData beaconData){
        Intent intent = new Intent(context, SplashActivity.class);
        Bundle bundle=new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.TYPE, NotificationType.BEACON_ON.getType());
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,beaconData);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_logo)
                .setContentTitle("Beacon Received")
                .setContentText(beaconData.getMessageReminder())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int id=0;
        String beaconMajor = beaconData.getBeaconMajor();
        if(!TextUtils.isEmpty(beaconMajor)){
            id=Integer.parseInt(beaconMajor);
        }
        notificationManager.notify(id, notificationBuilder.build());
    }
}
