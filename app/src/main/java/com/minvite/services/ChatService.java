package com.minvite.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.minvite.R;
import com.minvite.enums.NotificationType;
import com.minvite.models.chat.ChatModel;
import com.minvite.models.chat.FCM_ChatModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.HttpRestService;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;

public class ChatService extends IntentService {
    private static final String ACTION_SEND = "com.minvite.services.action.Send";

    public ChatService() {
        super("ChatService");
    }

    public static void sendMessage(Context context, FCM_ChatModel fcm_chatModel) {
        Intent intent = new Intent(context, ChatService.class);
        intent.setAction(ACTION_SEND);
        Bundle bundle=new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,fcm_chatModel);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_SEND:
                    Bundle extras = intent.getExtras();
                    if(extras!=null){
                        FCM_ChatModel fcm_chatModel= (FCM_ChatModel) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                        sendPushNotifiction(fcm_chatModel);
                    }
                    break;
            }
        }
    }

    private void sendPushNotifiction(FCM_ChatModel fcm_chatModel) {
        ChatModel chatModel = fcm_chatModel.getChatModel();
        HttpParamObject httpParamObject=new HttpParamObject();
        httpParamObject.setUrl("https://fcm.googleapis.com/fcm/send");
        httpParamObject.setJSONContentType();
        httpParamObject.addHeader("Authorization","key="+getResources().getString(R.string.fcm_server_key));
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("to",fcm_chatModel.getUserToken());
            JSONObject object=new JSONObject();
            object.put(AppConstants.FCM_CONSTANTS.TYPE, NotificationType.CHAT.getType());
            object.put(AppConstants.FCM_CONSTANTS.CONTENT,getChalObject(chatModel));
            jsonObject.put("data",object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        Log.i("http","FCM Request: "+jsonObject.toString());
        httpParamObject.setPostMethod();


        HttpRestService instance = (HttpRestService) ServiceFactory.getInstance(this, 102);
        try {
            Object data = instance.getData(httpParamObject);
            if(data==null){

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
    }

    private JSONObject getChalObject(ChatModel chatModel) {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("status",chatModel.getStatus());
            jsonObject.put("type",chatModel.getType());
            jsonObject.put("eventId",chatModel.getEventId());
            jsonObject.put("message",chatModel.getMessage());
            jsonObject.put("messageType",chatModel.getMessageType());
            jsonObject.put("fileUrl",chatModel.getFileUrl());
            jsonObject.put("timeStamp",chatModel.getTimeStamp());
            jsonObject.put("myNumber",chatModel.getMyNumber());
            jsonObject.put("toUserNumber",chatModel.getToUserNumber());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
