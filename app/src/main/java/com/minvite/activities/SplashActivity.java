package com.minvite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.minvite.R;
import com.minvite.activities.event.EventListActivity;
import com.minvite.enums.DeepLinkParams;
import com.minvite.models.InviteUtils;
import com.minvite.services.GetMyEventService;
import com.minvite.services.MinviteService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.MultiCallback;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class SplashActivity extends BaseActivity {

    private boolean isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        GetMyEventService.startGetEvents(this);
        isLogin = Preferences.getData(AppConstants.PREF_KEYS.IS_LOGIN, false);
        GetMyEventService.startGetEvents(this);
        try {
            GifDrawable gifFromAssets = new GifDrawable(getAssets(), "row/splash.gif");
            MultiCallback multiCallback = new MultiCallback();
            ImageView imageView = (ImageView) findViewById(R.id.iv_gif);
            imageView.setImageDrawable(gifFromAssets);
            multiCallback.addView(imageView);
            gifFromAssets.setCallback(multiCallback);

        } catch (IOException e) {
            e.printStackTrace();
        }


        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (isLogin) {
                        Bundle bundle = getIntent().getExtras();
                        if (bundle == null) {
                            startNextActivity(EventListActivity.class);
                        } else {
                            startNextActivity(bundle, EventListActivity.class);
                        }
                    } else {
                        startNextActivity(RegisterActivity.class);
                    }
                    finish();
                }

            }
        }.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance();
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    handleDeepLink(referringParams);
                } else {
                    Log.i("SplashActivity", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }

    private void handleDeepLink(JSONObject jsonObject) {
        Log.i("SplashActivity","Deep Link:"+jsonObject.toString());
        try {
            if (jsonObject.has(DeepLinkParams.LINK_TYPE.getParamName())) {
                String type = jsonObject.getString(DeepLinkParams.LINK_TYPE.getParamName());
                switch (type) {
                    case AppConstants.LINK_TYPE.INVITE:
                        InviteUtils.saveData(jsonObject);
                        if(isLogin){
                            MinviteService.inviteUser(this);
                        }
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }
}
