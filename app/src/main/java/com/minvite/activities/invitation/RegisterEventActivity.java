package com.minvite.activities.invitation;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.minvite.R;
import com.minvite.fragments.invitation.ChooseEventFragment;
import com.minvite.fragments.invitation.EventDetailsFragment;
import com.minvite.listeners.EventDetailListener;
import com.minvite.util.EventCat;
import com.minvite.util.EventType;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by RAHU on 14-07-2016.
 */
public class RegisterEventActivity extends BaseActivity implements EventDetailListener {
    private String event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        setAddPagesFragment();
        initToolBar("Choose");
    }

    private void setAddPagesFragment() {
        ChooseEventFragment chooseEventFragment=ChooseEventFragment.getInstance(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_fragment,chooseEventFragment).commit();
    }

    @Override
    public void setEventType(EventType eventType) {
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            event=bundle.getString(AppConstants.BUNDLE_KEYS.EVENT_CAT);
            final EventCat eventCat = EventCat.getEventCatFromValue(event);

            EventDetailsFragment eventDetailsFragment=EventDetailsFragment.getInstance(eventType,eventCat,this);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.frame_fragment,eventDetailsFragment).commit();
        }
    }

    @Override
    public void setTitle(String title) {
        initToolBar(title);
    }
}
