package com.minvite.util;

/**
 * Created by robin.bansal on 6/7/16.
 */
public enum EventType {

    TEXT("Text"),TEXT_IMAGE("Text & Image"),IMAGE("Image"),VIDEO("Video");
    private String type;
    public static final String KEY = "eventType";

    public String getType() {
        return type;
    }

    private EventType(String type){
        this.type = type;
    }

    public static EventType getEventTypeFromValue(String type){
        for (EventType eventType : EventType.values()) {
            if(eventType.getType().equals(type)){
                return eventType;
            }
        }
        return null;
    }
}
