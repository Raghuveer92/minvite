package com.minvite.services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

//import com.cloudinary.Cloudinary;
//import com.cloudinary.utils.ObjectUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 22-Jul-16.
 */
public class VideoUploadTask extends AsyncTask<Uri,Void,String> {
    UploadListener uploadListener;
    public static VideoUploadTask uploadVideo(VideoUploadTask.UploadListener uploadListener){
        VideoUploadTask videoUploadTask=new VideoUploadTask();
        videoUploadTask.uploadListener= uploadListener;
        return videoUploadTask;
    }
    @Override
    protected String doInBackground(Uri... params) {
        String imageUrl = null;
        try {
            imageUrl = uploadViaCloudinary(params[0]);
            uploadListener.onUpload(imageUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageUrl;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s==null){
            uploadListener.onUploadFailed();
        }else if(s.equals("")) {
            uploadListener.onUploadFailed();
        }else {
            uploadListener.onUpload(s);
        }
    }
    private String uploadViaCloudinary(Uri uri) throws Exception {
        Map config = new HashMap();
        config.put("cloud_name", "dm7mirpdi");
        config.put("api_key", "851662549713963");
        config.put("api_secret", "cgCnxhnjyboiriSTBTjCVLHVKBk");
//        Cloudinary cloudinary = new Cloudinary(config);
//        FileInputStream fileInputStream = new FileInputStream(getFile(uri));
//        final Map map = cloudinary.uploader().uploadLarge(fileInputStream, ObjectUtils.asMap("resource_type", "video_event"));
//        if(map.containsKey("url")){
//            final String url = map.get("url").toString();
//            Log.i("msg","video_event url:"+url);
//            return url;
//        }
        throw new Exception();
    }

    private File getFile(Uri uri) throws Exception {
        File file=new File(uri.getPath());
        return file;
    }


    public interface UploadListener{
        void onUpload(String imageUrl);
        void onUploadFailed();
    }
}
