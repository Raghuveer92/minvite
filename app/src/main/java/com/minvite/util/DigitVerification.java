package com.minvite.util;

import android.text.TextUtils;

import com.minvite.R;
import com.minvite.application.AppController;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsAuthConfig;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import io.fabric.sdk.android.Fabric;
import simplifii.framework.utility.Logger;

/**
 * Created by nitin on 08/02/16.
 */
public class DigitVerification {
    public static final String TAG = "DigitVerification";
    private AppController instance;
    private static AuthCallback authCallback;
    private static DigitVerification digitVerification = new DigitVerification();
    private static final String TWITTER_KEY = "7xlCpq33nqOm74dhXDc2KxznX";
    private static final String TWITTER_SECRET = "0dGo7gPFqwLh1MjP5BOj4bI2krBQhxBj58I8Y0ffYzK7t95gvz";

    public static DigitVerification getInstance(AppController instance) {
        digitVerification.instance = instance;
       return digitVerification;
    }

    public static void authenticate(String phoneNumber, final DigitCallback digitCallback) {
        authCallback = new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                Logger.info(TAG, "on Success" + phoneNumber);
                if (null != digitCallback) {
                    digitCallback.success(session, phoneNumber);
                }
            }

            @Override
            public void failure(DigitsException exception) {
                digitCallback.failure(exception);
            }
        };
        DigitsAuthConfig.Builder builder = new DigitsAuthConfig.Builder();
        DigitsAuthConfig config = builder.withAuthCallBack(authCallback).withPhoneNumber(phoneNumber).withThemeResId(R.style.CustomDigitsTheme).build();
        Digits.authenticate(config);
    }

    public void initDigitVerification() {

       TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(digitVerification.instance, new TwitterCore(authConfig), new Digits());

    }

    public static AuthCallback getAuthCallback() {
        return authCallback;
    }

    public static interface DigitCallback {
        public void success(DigitsSession session, String phoneNumber);
        public void failure(DigitsException exception);
    }
}
