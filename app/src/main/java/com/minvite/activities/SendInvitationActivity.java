package com.minvite.activities;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.MenuItem;

import com.minvite.R;
import com.minvite.fragments.invitation.SendInvitation_TabFragment;
import com.minvite.models.Contact;
import com.minvite.models.guest.GuestData;
import com.minvite.models.guest.GuestListResponse;
import com.minvite.models.sub_events.SubEventData;
import com.minvite.services.MinviteService;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class SendInvitationActivity extends BaseActivity implements PermissionListener, LoaderManager.LoaderCallbacks<Cursor>, SendInvitation_TabFragment.SelectContactListner {
    HashSet<Contact> selectedContacts = new HashSet<>();
    ArrayList<Contact> contactList=new ArrayList<>();
    private SendInvitation_TabFragment sendInvitationTabFragment;
    private ArrayList<SubEventData> subEventDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_invitation);
        initToolBar(getString(R.string.send_invite));
        new TedPermission(this)
                .setPermissions(Manifest.permission.READ_CONTACTS)
                .setPermissionListener(this).check();
        setFragment();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        subEventDataArrayList = (ArrayList<SubEventData>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        loadList(subEventDataArrayList);
    }
    private void loadList(ArrayList<SubEventData> eventDataArrayList) {
        if(eventDataArrayList!=null&&eventDataArrayList.size()>0){
            StringBuilder stringBuilder=new StringBuilder();
            String eventId = subEventDataArrayList.get(0).getEventId();
            stringBuilder.append("eventId='"+eventId+"' and ");
            for (int x=0;x<eventDataArrayList.size();x++){
                SubEventData subEventData = eventDataArrayList.get(x);
                if(x==0){
                    stringBuilder.append("subEventId='"+subEventData.getObjectId()+"'");
                }else {
                    stringBuilder.append(" or subEventId='"+subEventData.getObjectId()+"'");
                }
            }
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
            httpParamObject.addParameter("where",stringBuilder.toString());
            httpParamObject.setClassType(GuestListResponse.class);
            executeTask(AppConstants.TASK_CODES.GUEST_LIST, httpParamObject);
        }
    }
    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GUEST_LIST:
                GuestListResponse guestListResponse= (GuestListResponse) response;
                if(guestListResponse!=null){
                    List<GuestData> selectedGuestList = guestListResponse.getData();
                    if(selectedGuestList!=null){
                        setSeletedGuestList(selectedGuestList);
                    }
                }
                break;
        }
    }

    private void setSeletedGuestList(List<GuestData> selectedGuestList) {
        sendInvitationTabFragment.setSelectedGuestList(selectedGuestList);
    }

    public void setFragment() {
        sendInvitationTabFragment = SendInvitation_TabFragment.getInstance(contactList, selectedContacts, this);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_send_invitation, sendInvitationTabFragment).commit();
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(this, CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            final String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            final String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            final String imageUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
            contactList.add(Contact.getInstance(id, name, number, imageUri));
            cursor.moveToNext();
        }
        Collections.sort(contactList);
        sendInvitationTabFragment.refreshData();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send_invite, menu);
//        SearchView.SearchAutoComplete searchAutoComplete = setSearchView(menu);
//        searchAutoComplete.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                sendInvitationTabFragment.filterList(charSequence);
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            if (selectedContacts.size() == 0) {
                showToast(getString(R.string.error_to_select_user));
                return false;
            } else {
                if(subEventDataArrayList !=null){
                    MinviteService.startUploadGuestList(this,selectedContacts, subEventDataArrayList);
                    showToast(getString(R.string.success_invited));
                }
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPermissionGranted() {
        getSupportLoaderManager().initLoader(1, null, this);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        showToast("Permission denied, you cannot sent invitations");
    }

    @Override
    public void onContactSelect() {

    }
}
