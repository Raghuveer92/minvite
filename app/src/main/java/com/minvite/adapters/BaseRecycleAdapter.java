package com.minvite.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minvite.R;
import com.minvite.holders.BaseHolder;
import com.minvite.holders.ImageGridHolder;
import com.minvite.holders.ImageHolder;
import com.minvite.holders.PostHolder;
import com.minvite.holders.ProductsHolder;
import com.minvite.holders.VideoGridHolder;
import com.minvite.holders.WriteSomethingHolder;
import com.minvite.holders.VendorHolder;
import com.minvite.listeners.OnRecyclerItemClickListener;
import com.minvite.models.BaseAdapterModel;

import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 25/08/16.
 */
public class BaseRecycleAdapter<T extends BaseAdapterModel> extends RecyclerView.Adapter<BaseHolder> {
    private List<T> list;
    private Context context;
    private LayoutInflater inflater;
    private OnRecyclerItemClickListener onItemClickListener;

    public BaseRecycleAdapter(Context context, List<T> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseHolder holder = null;
        View itemView = null;
        switch (viewType) {
            case AppConstants.VIEW_TYPE.POST_CARD:
                itemView = inflater.inflate(R.layout.card_pb_image, null);
                holder = new PostHolder(itemView, this);
                break;
            case AppConstants.VIEW_TYPE.WRITE_SOMETHING:
                itemView = inflater.inflate(R.layout.row_profile_write_smthng, null);
                holder = new WriteSomethingHolder(itemView);
                break;
            case AppConstants.VIEW_TYPE.IMAGE:
                itemView = inflater.inflate(R.layout.row_postbook_image, parent, false);
                holder = new ImageHolder(itemView);
                break;
            case AppConstants.VIEW_TYPE.VENDOR_DETAIL:
                itemView = inflater.inflate(R.layout.row_vendor_detail, parent, false);
                holder = new VendorHolder(itemView);
                break;
            case AppConstants.VIEW_TYPE.PRODUCTS:
                itemView = inflater.inflate(R.layout.row_products_grid, parent, false);
                holder = new ProductsHolder(itemView);
                break;
            case AppConstants.VIEW_TYPE.GRIDE_IMAGE:
                itemView = inflater.inflate(R.layout.row_image, parent, false);
                holder = new ImageGridHolder(itemView);
                break;
            case AppConstants.VIEW_TYPE.GRIDE_VIDEO:
                itemView = inflater.inflate(R.layout.row_video, parent, false);
                holder = new VideoGridHolder(itemView);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        holder.onBind(position, this.list.get(position));
        holder.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public int getItemViewType(int position) {
        BaseAdapterModel model = list.get(position);
        return model.getViewType();
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public void setOnItemClickListener(OnRecyclerItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
