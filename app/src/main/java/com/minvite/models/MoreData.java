package com.minvite.models;

import com.minvite.models.invitation.EventData;

import java.io.Serializable;

/**
 * Created by raghu on 18/11/16.
 */

public class MoreData implements Serializable {
    private String name;
    private int fragmentType;
    private EventData eventData;

    public MoreData(String name, int fragmentType, EventData eventData) {
        this.name = name;
        this.fragmentType = fragmentType;
        this.eventData = eventData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFragmentType() {
        return fragmentType;
    }

    public EventData getEventData() {
        return eventData;
    }
}
