package com.minvite.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import com.minvite.R;

import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class FullScreenVideoActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_video);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        String videoUrl = bundle.getString(AppConstants.BUNDLE_KEYS.VIDEO_URL);
        if (!TextUtils.isEmpty(videoUrl)) {
            ExposureVideoPlayer exposureVideoPlayer = (ExposureVideoPlayer) findViewById(R.id.videoview);
            exposureVideoPlayer.init(this);
            exposureVideoPlayer.setFullScreen(false);
            exposureVideoPlayer.setAutoPlay(false);
            try {
                exposureVideoPlayer.setVideoSource(videoUrl);
            }catch (Exception e){

            }
        } else {
            finish();
        }
    }
}
