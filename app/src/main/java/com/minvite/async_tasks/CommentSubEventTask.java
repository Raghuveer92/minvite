package com.minvite.async_tasks;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.minvite.models.postbook.Comment;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.sub_events.SubEventData;
import com.minvite.models.user.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.HttpRestService;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by raghu on 16/11/16.
 */

public class CommentSubEventTask extends AsyncTask<SubEventData, Void, EventPost> {
    public static Comment sendComment(String commentText,SubEventData subEventData){
        Comment comment = new Comment();
        comment.setComment(commentText);
        comment.setUser(UserProfile.getInstance());
        comment.setCreated(System.currentTimeMillis());
        new CommentSubEventTask().execute(subEventData);
        return comment;
    }

    @Override
    protected EventPost doInBackground(SubEventData... subEvents) {
        if (subEvents.length == 0) {
            return null;
        }
        Comment comment = null;
        SubEventData subEvent = subEvents[0];
        List<Comment> comments = subEvent.getComments();
        if (comments != null) {
            for (Comment c : comments) {
                if (TextUtils.isEmpty(c.getObjectId())) {
                    comment = c;
                }
            }
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setPostMethod();
        httpParamObject.setUrl(AppConstants.PAGE_URL.COMMENT);
        JSONObject jsonObject = new JSONObject();
        try {
            if (comment != null) {
                jsonObject.put("comment", comment.getComment());
            }
            jsonObject.put("user", Util.getUser());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(Comment.class);
        HttpRestService httpRestService = new HttpRestService();
        try {
            Comment commentResponse = (Comment) httpRestService.getData(httpParamObject);
            if (comment != null) {
                comment.setObjectId(commentResponse.getObjectId());
                comment.setCreated(commentResponse.getCreated());
                return updateEvent(comment, subEvent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
        return null;
    }

    private EventPost updateEvent(Comment comment, SubEventData eventPost) {
        List<Comment> comments = eventPost.getComments();
        comments.add(comment);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SUB_EVENTS + "/" + eventPost.getObjectId());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("comments", getCommentJson(comments));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setPutMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(EventPost.class);
        HttpRestService httpRestService = new HttpRestService();
        try {
            return (EventPost) httpRestService.getData(httpParamObject);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONArray getCommentJson(List<Comment> comments) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Comment comment : comments) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("___class", "Comments");
            jsonObject.put("objectId", comment.getObjectId());
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }
}
