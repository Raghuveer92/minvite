package com.minvite.services;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 22-Jul-16.
 */
public class ImageUploadTask extends AsyncTask<Bitmap,Void,String> {
    UploadListener uploadListener;
    public static ImageUploadTask uploadImage(UploadListener uploadListener){
        ImageUploadTask imageUploadTask=new ImageUploadTask();
        imageUploadTask.uploadListener=uploadListener;
        return imageUploadTask;
    }
    @Override
    protected String doInBackground(Bitmap... params) {
        String imageUrl = null;
        try {
            imageUrl = uploadViaCloudinary(params[0]);
            uploadListener.onUpload(imageUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageUrl;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s==null){
            uploadListener.onUploadFailed();
        }else if(s.equals("")) {
            uploadListener.onUploadFailed();
        }else {
            uploadListener.onUpload(s);
        }
    }
    private String uploadViaCloudinary(Bitmap bMap) throws Exception {
        Map config = new HashMap();
        config.put("cloud_name", "dm7mirpdi");
        config.put("api_key", "851662549713963");
        config.put("api_secret", "cgCnxhnjyboiriSTBTjCVLHVKBk");
//        Cloudinary cloudinary = new Cloudinary(config);
//        FileInputStream fileInputStream = new FileInputStream(getFile(bMap));
//        final Map map = cloudinary.uploader().upload(fileInputStream, ObjectUtils.emptyMap());
//        if(map.containsKey("url")){
//            return map.get("url").toString();
//        }
        throw new Exception();
    }

    private File getFile(Bitmap bMap) throws Exception {
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/MInvite";
        File dir = new File(file_path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, System.currentTimeMillis() + ".png");
        FileOutputStream fOut = new FileOutputStream(file);

        bMap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
        fOut.flush();
        fOut.close();
        return file;
    }


    public interface UploadListener{
        void onUpload(String imageUrl);
        void onUploadFailed();
    }
}
