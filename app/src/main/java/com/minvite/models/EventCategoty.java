package com.minvite.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 20-Jul-16.
 */
public class EventCategoty extends BaseApi{
    String cat_id;
    String cat_name;
    @SerializedName("___class")
    String className;

    public String getCat_id() {
        return cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public String getClassName() {
        return className;
    }
}
