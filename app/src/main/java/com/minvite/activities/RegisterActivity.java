package com.minvite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.minvite.R;
import com.minvite.activities.event.EventListActivity;
import com.minvite.models.user.UserProfile;
import com.minvite.models.guest.GuestData;
import com.minvite.models.guest.GuestListResponse;
import com.minvite.services.MinviteService;
import com.minvite.util.DigitVerification;
import com.minvite.util.FireBaseFBLoginUtil;
import com.minvite.util.FireBaseGoogleLoginUtil;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Logger;
import simplifii.framework.utility.Preferences;

public class RegisterActivity extends BaseActivity {
    private FireBaseFBLoginUtil fireBaseFbLoginUtil;
    //    GoogleUtil googleUtil;
    private UserProfile profile;
    private FireBaseGoogleLoginUtil fireBaseGoogleLoginUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initGoogleFaceBook();
        setOnClickListener(R.id.fb_login_button, R.id.google_login_button);

    }

    private void initGoogleFaceBook() {
        fireBaseFbLoginUtil = new FireBaseFBLoginUtil(this, new FireBaseFBLoginUtil.FBLoginCallback() {
            @Override
            public void onSuccess(Bundle bundle) {
                setProfile(bundle);
            }

            @Override
            public void onFailure() {
                showToast("Failed to Facebook login..!");
            }
        });
        fireBaseGoogleLoginUtil = FireBaseGoogleLoginUtil.getInstance(this, new FireBaseGoogleLoginUtil.GoogleSignInListener() {
            @Override
            public void onSuccess(GoogleSignInAccount account) {
                handleSignInResult(account);
            }

            @Override
            public void onFailed() {
                showToast(R.string.failed_to_google);
            }
        });
//        googleUtil = GoogleUtil.getInstance(RegisterActivity.this);
//        googleUtil.setListener(new GoogleUtil.GoogleLoginCallBack() {
//            @Override
//            public void onSuccess(GoogleSignInAccount googleSignInAccount) {
//                handleSignInResult(googleSignInAccount);
//            }
//
//            @Override
//            public void onFailed() {
//                showToast("Failed to login with google..!");
//            }
//        });
    }


    private void sendDigitAuthentication() {
        DigitVerification.authenticate("+91", new DigitVerification.DigitCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                Logger.info(TAG, "Phone Number Received:" + phoneNumber);
                phoneNumber = session.getPhoneNumber();
                if (session != null) {
                    Digits.getSessionManager().clearActiveSession();
                }
                if (!TextUtils.isEmpty(phoneNumber)) {
                    if (!TextUtils.isEmpty(phoneNumber)) {
                        if (phoneNumber.length() > 10) {
                            phoneNumber = phoneNumber.substring((phoneNumber.length()) - 10, phoneNumber.length());
                        }
                    }

                    profile.setMobile(phoneNumber);
                    uploadData(profile);
                }
            }

            @Override
            public void failure(DigitsException exception) {
                Logger.info(TAG, "Phone Number Verification Failed:");
            }
        });
    }

    private void uploadData(UserProfile profile) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", profile.getName());
            jsonObject.put("email", profile.getEmail());
            jsonObject.put("mobile_no", profile.getMobile());
            jsonObject.put("password", profile.getMobile());
            jsonObject.put("image_url", profile.getImageUrl());
            jsonObject.put("account_id", profile.getAccountId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.USERS);
        object.setJson(jsonObject.toString());
        Log.i("msg", "json=" + jsonObject.toString());
        object.setPostMethod();
        object.setClassType(UserProfile.class);
        executeTask(AppConstants.TASK_CODES.REGISTER, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.REGISTER:
                if (response != null) {
                    UserProfile userProfile = (UserProfile) response;
                    if (!"".equals(userProfile.getMessage()))
                        showToast(userProfile.getMessage());
                }
                login();
                break;
            case AppConstants.TASK_CODES.LOGIN:
                if (response != null) {
                    UserProfile userProfile = (UserProfile) response;
                    if (userProfile != null) {
                        if (userProfile.getObjectId() != null) {
                            UserProfile.saveUser(userProfile);
                            Preferences.saveData(AppConstants.PREF_KEYS.OBJECT_ID, userProfile.getObjectId());
                            Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, true);
                            getMyEvent();
                        } else if (!"".equals(userProfile.getMessage()))
                            showToast(userProfile.getMessage());
                    }
                }
                break;
            case AppConstants.TASK_CODES.GET_MY_EVENTS:
                GuestListResponse guestListResponse = (GuestListResponse) response;
                if (guestListResponse != null) {
                    List<GuestData> dataList = guestListResponse.getData();
                    if (dataList != null) {
                        saveData(dataList);
                    }
                }
                MinviteService.updateToken(this);
                startNextActivity(EventListActivity.class);
                finish();
                break;
        }
    }

    private void saveData(List<GuestData> dataList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int x = 0; x < dataList.size(); x++) {
            GuestData guestData = dataList.get(x);
            if (x == 0) {
                stringBuilder.append("objectId='" + guestData.getEventId() + "'");
            } else {
                stringBuilder.append(" or objectId='" + guestData.getEventId() + "'");
            }
        }
        Preferences.saveData(AppConstants.PREF_KEYS.EVENT_IDS, stringBuilder.toString());
    }

    private void getMyEvent() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
        UserProfile profile = UserProfile.getInstance();
        httpParamObject.addParameter("where", "guestMobile=" + profile.getMobile());
        httpParamObject.addParameter("props", "eventId");
        httpParamObject.setClassType(GuestListResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_MY_EVENTS, httpParamObject);
    }

    private void login() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("login", profile.getMobile());
            jsonObject.put("password", profile.getMobile());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.LOGIN);
        object.setJson(jsonObject.toString());
        object.setPostMethod();
        object.setClassType(UserProfile.class);
        executeTask(AppConstants.TASK_CODES.LOGIN, object);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fb_login_button:
                fireBaseFbLoginUtil.initiateFbLogin();
                break;
            case R.id.google_login_button:
                if (!isNetworkAvailable()) {
                    showToast("Internet not connected..!");
                    return;
                }
                fireBaseGoogleLoginUtil.login();
//                googleUtil.signInWithGoogle();
                break;
        }
    }

    private void setProfile(Bundle bundle) {
        profile = new UserProfile();
        profile.setName(bundle.getString("name"));
        profile.setEmail(bundle.getString("email"));
        profile.setImageUrl(bundle.getString(""));
        sendDigitAuthentication();
    }

    private void handleSignInResult(GoogleSignInAccount acct) {
        profile = new UserProfile();
        profile.setEmail(acct.getEmail());
        profile.setName(acct.getDisplayName());
        if (acct.getPhotoUrl() != null)
            profile.setImageUrl(acct.getPhotoUrl().toString());
        profile.setAccountId(acct.getId());
        sendDigitAuthentication();
        Log.i("msg", profile.getEmail() + " " + profile.getName() + " " + profile.getAccountId());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;
        fireBaseFbLoginUtil.onActivityResult(requestCode, resultCode, data);
        fireBaseGoogleLoginUtil.onActivityResult(requestCode, resultCode, data);
    }

}
