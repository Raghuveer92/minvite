package com.minvite.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MinviteReceiver extends BroadcastReceiver {
    OnReceiveListener onReceiveListener;

    public MinviteReceiver(OnReceiveListener onReceiveListener) {
        this.onReceiveListener = onReceiveListener;
    }

    public static final String ACTION_REFRESH_EVENTS = "com.minvite.RefreshEvents";

    public static void sendBroadCast(Context context) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MinviteReceiver.ACTION_REFRESH_EVENTS);
        context.sendBroadcast(broadcastIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(onReceiveListener!=null){
            onReceiveListener.onReceiveResult();
        }
    }
    public interface OnReceiveListener{
        void onReceiveResult();
    }
}