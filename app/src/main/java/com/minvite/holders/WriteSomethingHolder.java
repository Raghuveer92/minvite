package com.minvite.holders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.minvite.activities.postbook.AddPostActivity;
import com.minvite.models.postbook.WriteSomeThingModel;
import com.minvite.models.invitation.EventData;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 16/9/16.
 */
public class WriteSomethingHolder extends BaseHolder {
private View view;
    public WriteSomethingHolder(View itemView) {
        super(itemView);
        this.view=itemView;
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final WriteSomeThingModel writeSomeThingModel= (WriteSomeThingModel) obj;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventData eventData = writeSomeThingModel.getEventData();
                Bundle bundle=new Bundle();
                if(eventData !=null){
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventData);
                }
                Activity activity= (Activity) context;
                Intent intent = new Intent(context, AddPostActivity.class);
                intent.putExtras(bundle);
                activity.startActivityForResult(intent,AppConstants.REQUEST_CODES.REFRESH_DATA);
            }
        });
    }
}
