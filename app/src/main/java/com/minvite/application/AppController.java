package com.minvite.application;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;


import com.activeandroid.ActiveAndroid;
import com.minvite.models.beacons.BeaconData;
import com.minvite.models.beacons.BeaconResponse;
import com.minvite.services.BeaconRefreshService;
import com.minvite.services.GetEventService;

import com.minvite.services.GetMyEventService;
import com.minvite.services.MinviteService;
import com.minvite.util.DigitVerification;
import com.facebook.FacebookSdk;
import com.todddavies.utils.BackendLessUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import io.branch.referral.Branch;
import simplifii.framework.utility.Preferences;

public class AppController extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Preferences.initSharedPreferences(this);
        DigitVerification.getInstance(this).initDigitVerification();
        FacebookSdk.sdkInitialize(this);
//        printHash();
        ActiveAndroid.initialize(this);
//        BeaconRefreshService.startActionRefresh(this);
        Branch.getAutoInstance(this);
    }

    private void getEvents() {
        GetMyEventService.startGetEvents(this);
    }

    private void getEventCatagories() {
        GetEventService.startActionEVENT_CAT(this);
    }

    private void getEventTypes() {
        GetEventService.startActionEVENT_TYPE(this);
    }

    public void printHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.minvite", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static BeaconData getServerBeacon(String major) {
        BeaconResponse beaconResponse = BeaconResponse.getInstance();
        List<BeaconData> beaconDataList = beaconResponse.getData();
        for (BeaconData beaconData : beaconDataList) {
            if (beaconData.getBeaconMajor().equals(major)) {
                return beaconData;
            }
        }
        return null;
    }

    public static boolean isPermissionGranted(Context context,String permossion) {
        if(ContextCompat.checkSelfPermission(context,permossion)==PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }
}
