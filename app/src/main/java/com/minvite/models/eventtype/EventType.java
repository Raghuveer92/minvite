package com.minvite.models.eventtype;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 14-Jul-16.
 */
public class EventType {
    String type;
    String objectId;
    @SerializedName("___class")
    String className;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
