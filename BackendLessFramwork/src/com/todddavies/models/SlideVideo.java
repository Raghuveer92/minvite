package com.todddavies.models;

/**
 * Created by raghu on 15/11/16.
 */

public class SlideVideo {
    String videoUrl;
    String videoPath;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }
}
