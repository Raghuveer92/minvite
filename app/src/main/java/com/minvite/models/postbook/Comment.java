package com.minvite.models.postbook;

import com.minvite.models.BaseApi;
import com.minvite.models.user.UserProfile;

import java.util.Date;

/**
 * Created by raghu on 17/9/16.
 */
public class Comment extends BaseApi  implements Comparable{
    String comment;
    UserProfile user;

    public String getComment() {
        return comment;
    }

    public UserProfile getUser() {
        return user;
    }


    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }
    @Override
    public int compareTo(Object o) {
        Comment comment = (Comment) o;
        Date date = new Date(getCreated());
        Date objectDate = new Date(comment.getCreated());
        return objectDate.compareTo(date);
    }

}
