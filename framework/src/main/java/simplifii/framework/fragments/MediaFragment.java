package simplifii.framework.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.Util;

public class MediaFragment extends Fragment {
    public static final int AUDIO = 2;
    public static final int VIDEO = 3;
    public static final int IMAGE = 4;

    private final int REQUEST_CODE_GALLARY = 50;
    private final int REQUEST_CODE_CAMERA = 51;
    private final int REQUEST_CODE_AUDIO = 52;
    private final int REQUEST_CODE_PICK_VIDEO = 53;
    private MediaListener mediaListener;
    private ImageListener imageListener;
    private AudioListener audioListener;
    private MultipleImageListener multipleImageListener;

    public void getImage(final ImageListener imageListener) {

        this.imageListener = imageListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, new String[]{"Camera", "Gallery"});
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            getImageFromCamera();
                        } else if (which == 1) {
                            getImageFromGallery();
                        }
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.setTitle("Choose a picture");
                dialog.show();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        });
    }


    public void getVideo(final MediaListener mediaListener) {
        this.mediaListener = mediaListener;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, new String[]{"Camera", "Gallery"});
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, final int which) {
                if (which == 0) {
                    new TedPermission(getActivity())
                            .setPermissions(Manifest.permission.CAMERA)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            getVideoFromCamera();
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                        }
                    }).check();
                } else if (which == 1) {
                    checkPerMission(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            getVideoFromGallary();
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                        }
                    });
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Select your video");
        dialog.show();
    }

    private void checkPerMission(PermissionListener permissionListener) {
//  add this dependency compile 'gun0912.ted:tedpermission:1.0.0'
        new TedPermission(getActivity())
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setPermissionListener(permissionListener).check();
    }

    private void getImageFromCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA);
    }

    private void getImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_GALLARY);
    }

    public void getAudio(AudioListener mediaListener) {
        this.audioListener = mediaListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent intent = new Intent();
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "Select Audio "), REQUEST_CODE_AUDIO);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        });
    }

    private void getVideoFromCamera() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_CODE_PICK_VIDEO);
        }
    }

    private void getVideoFromGallary() {
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_CODE_PICK_VIDEO);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if(bitmap!=null){
                    imageListener.onGetBitMap(bitmap);
                }
                break;
            case REQUEST_CODE_GALLARY:
                Bitmap bitmap1 = Util.getBitmapFromUri(getActivity(), data.getData());
                if(bitmap1!=null){
                    imageListener.onGetBitMap(bitmap1);
                }
                break;
            case REQUEST_CODE_AUDIO:
                Uri uri = data.getData();
                if(uri!=null){
                    audioListener.onGetUri(uri, AUDIO);
                }
                break;
            case REQUEST_CODE_PICK_VIDEO:
                Uri videoUri = data.getData();
                if(videoUri!=null){
                    mediaListener.onGetVideo(getRealPathFromURI(getActivity(),videoUri), VIDEO);
                }
                break;
        }

    }
    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Video.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    public interface MediaListener {
        void onGetVideo(String path, int mediaType);
    }
    public interface AudioListener {
        void onGetUri(Uri uri, int mediaType);
    }

    public interface ImageListener {
        void onGetBitMap(Bitmap bitmap);
    }

    public interface MultipleImageListener {
        void onGetBitMaps(List<Bitmap> bitmap);
    }

}
