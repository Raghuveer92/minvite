package com.minvite.activities.updates;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.minvite.R;
import com.minvite.models.invitation.EventData;
import com.minvite.models.update.UpdateData;
import com.minvite.models.update.UpdateResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class UpdatesActivity extends BaseActivity implements CustomListAdapterInterface {
    private static final int ADD_UPDATE = 121;
    List<UpdateData> updateDataList = new ArrayList<>();
    private CustomListAdapter customPagerAdapter;
    private EventData eventData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updates);
        initToolBar(getString(R.string.updates));
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        initToolBar(getString(R.string.updates));
        ListView listView = (ListView) findViewById(R.id.list_update);
        customPagerAdapter = new CustomListAdapter(this, R.layout.row_update, updateDataList, this);
        listView.setAdapter(customPagerAdapter);
        eventData = (EventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if(eventData!=null){
            getData();
        }
    }

    private void getData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE);
        httpParamObject.addParameter("where","eventId='"+eventData.getObjectId()+"'");
        httpParamObject.setClassType(UpdateResponse.class);
        executeTask(AppConstants.TASK_CODES.UPDATES, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.UPDATES:
                UpdateResponse updateResponse= (UpdateResponse) response;
                if(updateResponse!=null){
                    List<UpdateData> data = updateResponse.getData();
                    if(data!=null&&data.size()>0){
                        setData(data);
                    }
                }
                break;
        }
    }

    private void setData(List<UpdateData> data) {
        updateDataList.addAll(data);
        customPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.our_invitation, menu);
        if(!eventData.isAdmin()){
            MenuItem item = menu.findItem(R.id.action_add);
            item.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent=new Intent(this,AddNewUpdateActivity.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,eventData);
                intent.putExtras(bundle);
                startActivityForResult(intent, ADD_UPDATE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK&&requestCode==ADD_UPDATE){
            updateDataList.clear();
            getData();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if(convertView==null){
            convertView=inflater.inflate(resourceID,null);
        }
        UpdateData updateData = updateDataList.get(position);
        setText(updateData.getTitle(),R.id.tv_title,convertView);
        setText(updateData.getSubTitle(),R.id.tv_sub_title,convertView);
        setText(updateData.getCreateTime(),R.id.tv_time,convertView);
        return convertView;
    }
}
