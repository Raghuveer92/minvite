package com.minvite.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.models.guest.GuestData;
import com.minvite.models.guest.GuestListResponse;
import com.minvite.models.sub_events.SubEventData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class GuestListActivity extends BaseActivity implements CustomListAdapterInterface {

    private CustomListAdapter customListAdapter;
    private List<GuestData> guestList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_guest_list);
        initToolBar(getString(R.string.guest_list));
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        SubEventData subEventData= (SubEventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if(subEventData!=null){
            ListView listView = (ListView) findViewById(R.id.lv_guest_list);
            customListAdapter = new CustomListAdapter(this, R.layout.row_guest_list, guestList, this);
            listView.setAdapter(customListAdapter);
            loadList(subEventData);
        }
    }

    private void loadList(SubEventData subEventData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
        httpParamObject.addParameter("where","eventId='"+ subEventData.getEventId()+"' and subEventId='"+subEventData.getObjectId()+"'");
        httpParamObject.setClassType(GuestListResponse.class);
        executeTask(AppConstants.TASK_CODES.GUEST_LIST, httpParamObject);
    }
    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GUEST_LIST:
                GuestListResponse guestListResponse= (GuestListResponse) response;
                if(guestListResponse!=null){
                    List<GuestData> guestDataList = guestListResponse.getData();
                    if(guestDataList!=null){
                        setList(guestDataList);
                    }
                }
                break;
        }
    }
    private void setList(List<GuestData> guestDataList) {
        guestList.clear();
        guestList.addAll(guestDataList);
        Collections.sort(guestList);
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder=new Holder(convertView);
            convertView.setTag(holder);
        }else {
            holder= (Holder) convertView.getTag();
        }
        GuestData guestData = guestList.get(position);
        String guestName = guestData.getGuestName();
        holder.tvName.setText(guestName);
        if(guestName.length()>0){
            String s = String.valueOf(guestName.charAt(0));
            if(!TextUtils.isEmpty(s)){
                holder.tvPic.setText(s);
            }
        }
//        setStatus(guestData,holder);
        return convertView;
    }

    private void setStatus(GuestData guestData, Holder holder) {
        String status = guestData.getStatus();
        holder.tvStatus.setVisibility(View.GONE);
        holder.ivStatus.setVisibility(View.VISIBLE);

        switch (status){
            case AppConstants.INVITATION_STATUS.ACCEPTED:
                holder.ivStatus.setImageResource(R.drawable.dot_green);
                break;
            case AppConstants.INVITATION_STATUS.REJECTED:
                holder.ivStatus.setImageResource(R.drawable.dot_red);
                break;
            case AppConstants.INVITATION_STATUS.MAY_BE:
                holder.ivStatus.setImageResource(R.drawable.dot_orange);
                break;
            default:
                holder.ivStatus.setVisibility(View.GONE);
                holder.tvStatus.setVisibility(View.VISIBLE);
        }
    }

    class Holder{
        TextView tvName,tvStatus,tvPic;
        ImageView ivStatus;
        public Holder(View view) {
            tvName= (TextView) view.findViewById(R.id.tv_name_invitation);
            tvStatus= (TextView) view.findViewById(R.id.tv_status);
            tvPic= (TextView) view.findViewById(R.id.tv_pic);
            ivStatus= (ImageView) view.findViewById(R.id.iv_status);
            ivStatus.setVisibility(View.GONE);
        }
    }

}
