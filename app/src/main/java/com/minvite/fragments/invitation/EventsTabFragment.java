package com.minvite.fragments.invitation;

import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.models.invitation.EventData;
import com.minvite.models.invitation.EventsResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Admin on 20-Jul-16.
 */
public class EventsTabFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface {
    private CustomPagerAdapter pagerAdapter;

    @Override
    public void initViews() {
        getDataFramServer();
    }

    private void setTabs() {
        TabLayout tabLayout = (TabLayout) findView(R.id.tab_invitation);
        ViewPager viewPager = (ViewPager) findView(R.id.pager_invitation);
        pagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), tabs, this);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        if (tabs.size() < 3) {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        } else {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        setTabFont(tabLayout);
    }

    public void setTabFont(View view) {
        if (!(view instanceof ViewGroup)) {
            return;
        }
        ViewGroup group = (ViewGroup) view;
        for (int i = 0; i < group.getChildCount(); i++) {
            View child = group.getChildAt(i);
            if (child instanceof TextView) {
                TextView textView = (TextView) child;
                Typeface tf;
                try {
                    String asset = "fonts/Lato-Regular.ttf";
                    tf = Typeface.createFromAsset(getActivity().getAssets(), asset);
                } catch (Exception e) {
                    Log.e(TAG, "Error to get typeface: " + e.getMessage());
                    return;
                }
                textView.setTypeface(tf);
            } else {
                setTabFont(child);
            }
        }
    }

    private void getDataFramServer() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.EVENTS);
        String data = Preferences.getData(AppConstants.PREF_KEYS.EVENT_IDS, "");
        if(TextUtils.isEmpty(data)){
            object.addParameter("where", "eventId=''"+data);
        }else {
            object.addParameter("where", data);
        }
        object.setClassType(EventsResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_ALL_INVITAIONS, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (AppConstants.TASK_CODES.GET_ALL_INVITAIONS == taskCode && response != null) {
            EventsResponse eventsResponse = (EventsResponse) response;
            catagorizedData(eventsResponse);
        }
    }

    ArrayList<ArrayList> baseInvitaionslist = new ArrayList();
    List<EventListFragment> fragments = new ArrayList<>();
    List<String> tabs = new ArrayList<>();

    private void catagorizedData(EventsResponse eventsResponse) {
        tabs.clear();
        baseInvitaionslist.clear();
        fragments.clear();
        List<EventData> dataList = eventsResponse.getData();
        final List<EventData> data = new ArrayList<>();
        for (EventData eventData : dataList) {
            if (eventData != null) {
                data.add(eventData);
            }
        }
        if (data.size() == 0) {
            showVisibility(R.id.emptyView);
        }
        for (EventData eventData : data) {
            final String cat_name = eventData.getCategory();
            if (!tabs.contains(cat_name)) {
                tabs.add(cat_name);
                ArrayList childInvitationList = new ArrayList();
                childInvitationList.add(eventData);
                baseInvitaionslist.add(childInvitationList);
            } else {
                int possition = tabs.indexOf(cat_name);
                ArrayList childInvitationList = baseInvitaionslist.get(possition);
                childInvitationList.add(eventData);
            }
        }
        for (int x = 0; x < baseInvitaionslist.size(); x++) {
            List<EventData> eventDataList = baseInvitaionslist.get(x);
            EventListFragment eventListFragment = EventListFragment.getInstance(eventDataList);
            fragments.add(eventListFragment);
        }
        setTabs();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_invitation;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tabs.get(position);
    }

    public void setFilterList(boolean isAdmin) {
        for (EventListFragment eventListFragment : fragments) {
            eventListFragment.setFilterList(isAdmin);
        }
    }

    public void refreshData() {
        getDataFramServer();
    }
}
