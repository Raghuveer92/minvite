package com.minvite.models;

import android.text.TextUtils;
import android.text.format.DateUtils;

import com.minvite.models.postbook.Comment;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Admin on 14-Jul-16.
 */
public class BaseApi implements Serializable {
    long code;
    String message = "";
    String objectId;
    long offset;
    long totalObjects;
    long created;

    public String getCreateTime() {
        return DateUtils.getRelativeTimeSpanString(created, System.currentTimeMillis(), 0L).toString();
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public long getOffset() {
        return offset;
    }

    public long getTotalObjects() {
        return totalObjects;
    }

    public boolean isError() {
        if (TextUtils.isEmpty(message)) {
            return false;
        } else {
            return true;
        }
    }

}
