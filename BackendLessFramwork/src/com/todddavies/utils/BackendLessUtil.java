package com.todddavies.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.todddavies.listeners.OnUploadImage;
import com.todddavies.models.SlideImage;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * Created by raghu on 25/9/16.
 */

public class BackendLessUtil {
    public static final String APPLICATION_ID = "8D01CA59-6226-2622-FF06-1A0F33A34200";
    public static final String SECRET_KEY = "9C91F2C0-CAEB-EBC0-FF97-6FDE46591300";
    public static final String VERSION = "v1";
    public static final String SERVER_URL = "https://api.backendless.com";
    public static final String ROOT_FILE_PATH = "media";
    private static final String TAG = "BackendLess";
    public static Context mContext;
    //private static int imageCounter;

    public static void initBackendLess(Context context) {
        mContext=context;
        Backendless.setUrl(SERVER_URL);
        Backendless.initApp(context, APPLICATION_ID, SECRET_KEY, VERSION);
    }

    public static void uploadImage(Bitmap bitmap, final OnResponseListener listener) {
        Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(bitmap);
        String name = UUID.randomUUID().toString() + ".png";
        Backendless.Files.Android.upload(resizedBitmap, Bitmap.CompressFormat.PNG, 100, name, ROOT_FILE_PATH, new MyFileUploadTask(listener));
    }

        public static void uploadImage(final SlideImage slideImage, final OnUploadImage onUploadImage) {

        new AsyncTask<Void, Integer, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                    Bitmap bitmap = CommanMethodes.getBitmapFromPath(mContext, slideImage.getImgPath());
                    Bitmap resizedBitmap = CommanMethodes.getResizedBitmap(bitmap);
                    BackendLessUtil.uploadImage(resizedBitmap, new OnResponseListener() {
                        @Override
                        public void onSuccess(String fileUrl) {
                            Log.i(TAG, "imageUrl=" + fileUrl);
                            slideImage.setImageUrl(fileUrl);
                                onUploadImage.onSuccess();
                        }

                        @Override
                        public void onFailed(String exception) {
                            Log.i("msg", "fail to upload=" + exception);
                            onUploadImage.onFailed();
                        }
                    });
                return null;
            }
        }.execute();

    }

    public static void uploadFile(File file, OnResponseListener listener) {
        Backendless.Files.upload(file, ROOT_FILE_PATH, true, new MyFileUploadTask(listener));
    }

    public static void deleteFile(String filePath, final OnDeleteListener listener) {
        Backendless.Files.remove(filePath, new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void aVoid) {
                listener.onSuccess();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                listener.onFailed(backendlessFault.toString());
            }
        });
    }

    public interface OnResponseListener {
        void onSuccess(String fileUrl);

        void onFailed(String exception);
    }

    public interface OnDeleteListener {
        void onSuccess();

        void onFailed(String exception);
    }

    public interface OnUploadAllListener {
        void onUploadAll();
        void onFailed();
        void onProgress(int uploaded);
    }

    private static class MyFileUploadTask implements AsyncCallback<BackendlessFile> {
        OnResponseListener listener;

        public MyFileUploadTask(OnResponseListener listener) {
            this.listener = listener;
        }

        @Override
        public void handleResponse(BackendlessFile backendlessFile) {
            listener.onSuccess(backendlessFile.getFileURL());
        }

        @Override
        public void handleFault(BackendlessFault backendlessFault) {
            listener.onFailed(backendlessFault.toString());
        }
    }
}
