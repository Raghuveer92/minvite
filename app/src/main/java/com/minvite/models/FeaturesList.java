package com.minvite.models;

import com.minvite.models.FeatureEntry;

import java.util.List;

/**
 * Created by robin.bansal on 6/7/16.
 */
public class FeaturesList {

    private List<FeatureEntry> data;

    public List<FeatureEntry> getData() {
        return data;
    }

    public void setData(List<FeatureEntry> data) {
        this.data = data;
    }
}
