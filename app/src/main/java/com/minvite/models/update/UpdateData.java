package com.minvite.models.update;

import com.minvite.models.BaseApi;

/**
 * Created by raghu on 6/12/16.
 */

public class UpdateData extends BaseApi{
    private String title;
    private String subTitle;
    private String eventId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
