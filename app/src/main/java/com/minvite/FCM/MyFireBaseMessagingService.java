package com.minvite.FCM;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.minvite.enums.NotificationType;
import com.minvite.services.BeaconRefreshService;
import com.minvite.services.ScanService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 28/11/16.
 */

public class MyFireBaseMessagingService extends FirebaseMessagingService {
    String TAG = "FCM";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        Map<String, String> data = remoteMessage.getData();
        if(data!=null){
            handleFCMData(data.toString());
          }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            String body = remoteMessage.getNotification().getBody();
            Log.d(TAG, "Message Notification Body: " + body);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void handleFCMData(String object) {
        try {
            JSONObject jsonObject = new JSONObject(object);
            int type = jsonObject.getInt(AppConstants.FCM_CONSTANTS.TYPE);
            JSONObject contentObject = jsonObject.getJSONObject(AppConstants.FCM_CONSTANTS.CONTENT);
            NotificationType notificationType = NotificationType.findByStatus(type);
            switch (notificationType) {
                case CHAT:
                    PushNotificationUtil.sendChatNotification(this,contentObject);
                    break;
                case BEACON_ON:
                    BeaconRefreshService.startActionRefresh(this);
                    break;
                case BEACON_OFF:
                    ScanService.stopScanService(this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
