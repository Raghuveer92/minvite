package com.minvite.util;

import com.minvite.models.EventCategoty;
import com.minvite.models.EventCategotyResponce;
import com.minvite.models.eventtype.EventTypeResponce;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Admin on 15-Jul-16.
 */
public class CommanMethodes {

    public static JSONObject getUsersJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("objectId", Preferences.getData(AppConstants.PREF_KEYS.OBJECT_ID,""));
            jsonObject.put("___class", "Users");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    public static JSONObject getEventTypeJSON(EventType eventType) {
        JSONObject jsonObject = new JSONObject();
        try {
            String eventsTypeInstance= Preferences.getData(AppConstants.PREF_KEYS.EVENT_TYPES_INSTANCE,"");
            Gson gson=new Gson();
            if("".equals(eventsTypeInstance))
                return new JSONObject();
            EventTypeResponce eventTypeResponce=gson.fromJson(eventsTypeInstance, EventTypeResponce.class);
            final List<com.minvite.models.eventtype.EventType> data = eventTypeResponce.getData();
            for(com.minvite.models.eventtype.EventType eventType1:data){

                if(eventType1.getType().equals(eventType.getType())){
                    jsonObject.put("objectId", eventType1.getObjectId());
                    jsonObject.put("___class", eventType1.getClassName());
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static JSONObject getEventCatJSON(EventCat eventCat) {
        JSONObject jsonObject = new JSONObject();
        try {
            String eventsCatInstance= Preferences.getData(AppConstants.PREF_KEYS.EVENT_CAT_INSTANCE,"");
            Gson gson=new Gson();
            if("".equals(eventsCatInstance))
                return new JSONObject();
            final EventCategotyResponce eventCategotyResponce = gson.fromJson(eventsCatInstance, EventCategotyResponce.class);
            final List<EventCategoty> data = eventCategotyResponce.getData();
            for(EventCategoty eventCategoty:data){
                if(eventCategoty.getCat_id().equals(eventCat.getType())){
                    jsonObject.put("objectId", eventCategoty.getObjectId());
                    jsonObject.put("___class", eventCategoty.getClassName());
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
