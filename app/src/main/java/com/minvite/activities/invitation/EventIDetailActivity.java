package com.minvite.activities.invitation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.minvite.R;
import com.minvite.fragments.invitation.SendInvitationFragment;
import com.minvite.models.invitation.EventData;
import com.squareup.picasso.Picasso;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class EventIDetailActivity extends BaseActivity {
    private EventData eventData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_info);
        initToolBar("Event Info");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            setData(bundle);
        setOnClickListener(R.id.btn_share, R.id.btn_send_invitation);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_share:
                shareMsg();
                break;
            case R.id.btn_send_invitation:
                startActivityForResult(new Intent(this, SendInvitationFragment.class), AppConstants.REQUEST_CODES.ADD_INVITAION);
                break;
        }
    }

    private void shareMsg() {
        if (eventData != null) {
            String msg = "You are invited for " + eventData.getName() + "\n"
                    + "to View this event download M-Invite" + "\n"
                    + "Your event code is: " + eventData.getObjectId();
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "EventData from M-Invite");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
            startActivity(Intent.createChooser(sharingIntent, "Share your eventData"));
        }
    }

    private void setData(Bundle bundle) {
        eventData = (EventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        setText(eventData.getName(), R.id.tv_event_name);
        setText(eventData.getDatetime(), R.id.tv_event_date);
        setText(" " + eventData.getLocation(), R.id.tv_event_location);
        setText(eventData.getDescription(), R.id.tv_event_description);
        ImageView imageView = (ImageView) findViewById(R.id.iv_profile);
        if (!"".equals(eventData.getImageUrl()))
            Picasso.with(this).load(eventData.getImageUrl()).placeholder(R.drawable.rahul).into(imageView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.REQUEST_CODES.ADD_INVITAION && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
