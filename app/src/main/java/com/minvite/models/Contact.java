package com.minvite.models;

import java.io.Serializable;

/**
 * Created by Admin on 21-Jul-16.
 */
public class Contact implements Comparable,Serializable{
    String id;
    String name;
    String PhoneNumber;
    String photoUri;
    boolean isDisable;

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public static Contact getInstance(String id, String name, String phoneNo,String photoUri) {
        Contact contact=new Contact();
        contact.setId(id);
        contact.setName(name);
        contact.setPhoneNumber(phoneNo);
        contact.setPhotoUri(photoUri);
        return contact;
    }

    public boolean isDisable() {
        return isDisable;
    }

    public void setDisable(boolean disable) {
        isDisable = disable;
    }

    @Override
    public int compareTo(Object another) {
        Contact contact= (Contact) another;
        return name.compareTo(contact.name);
    }
}
