package com.minvite.fragments.more;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.models.album.AlbumData;
import com.minvite.models.album.AlbumResponse;
import com.minvite.models.invitation.EventData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.flipview.FlipView;
import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 18/11/16.
 */

public class AlbumFragment extends BaseFragment implements CustomListAdapterInterface, FlipView.OnFlipListener {
    private EventData eventData;
    List<AlbumData> albumDataList =new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private FlipView flipView;
    private TextView tvCount;

    public static AlbumFragment getInstance(EventData eventData) {
        AlbumFragment albumFragment = new AlbumFragment();
        albumFragment.eventData = eventData;
        return albumFragment;
    }

    @Override
    public void initViews() {
        initToolBar(getString(R.string.album));
        tvCount= (TextView) findView(R.id.tv_count);
        flipView = (FlipView) findView(R.id.flip_view);
        customListAdapter=new CustomListAdapter(getActivity(),R.layout.fragment_albm_photo,albumDataList,this);
        flipView.setAdapter(customListAdapter);
        flipView.setOnFlipListener(this);
        if (eventData != null) {
            getData(eventData);
        }
    }

    private void getData(EventData eventData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ALBUM);
        httpParamObject.addParameter("where","eventId='"+eventData.getObjectId()+"'");
        httpParamObject.setClassType(AlbumResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_ALBUM, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        flipView.setEmptyView(findView(R.id.tv_empty_view));
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_ALBUM:
                AlbumResponse albumResponse= (AlbumResponse) response;
                if(albumResponse!=null){
                    List<AlbumData> data = albumResponse.getData();
                    if(data!=null){
                        setData(data);
                    }
                }
                break;
        }
    }

    private void setData(List<AlbumData> dataList) {
        albumDataList.addAll(dataList);
        customListAdapter.notifyDataSetChanged();
        tvCount.setText((flipView.getCurrentPage()+1)+"/"+albumDataList.size());
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_album;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if(convertView==null){
            convertView=inflater.inflate(resourceID,parent,false);
            holder=new Holder(convertView);
            convertView.setTag(holder);
        }else {
            holder= (Holder) convertView.getTag();
        }
        AlbumData albumData = albumDataList.get(position);
        String imageUrl = albumData.getImageUrl();
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.photo_place_holder).into(holder.touchImageView);
        }
        holder.textView.setText(albumData.getName());
        return convertView;
    }

    @Override
    public void onFlippedToPage(FlipView v, int position, long id) {
        tvCount.setText((position+1)+"/"+albumDataList.size());
    }

    public class Holder{
        ImageView touchImageView;
        TextView textView;

        public Holder(View view) {
            touchImageView = (ImageView) view.findViewById(R.id.iv_album);
            textView= (TextView) view.findViewById(R.id.tv_album_title);
        }
    }
}
