package com.minvite.activities.more;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.activities.more.VendorDetailsActivity;
import com.minvite.models.vendor.VendorData;
import com.minvite.models.vendor.VendorResponse;
import com.minvite.models.venues.VenueData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class VendorsListActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<VendorData> vendorDataList = new ArrayList<>();
    CustomListAdapter customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors_list);
        initToolBar(getString(R.string.vendors));
        ListView listView = (ListView) findViewById(R.id.listView_vendors);
        customListAdapter = new CustomListAdapter(this, R.layout.row_vendors_list, vendorDataList, this);
        listView.setAdapter(customListAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        VenueData venueData = (VenueData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if (venueData != null) {
            getData(venueData);
        }
    }

    private void getData(VenueData venueData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.VENDORS);
        httpParamObject.addParameter("where","venueId='"+venueData.getObjectId()+"'");
        httpParamObject.setClassType(VendorResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_VENDORS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_VENDORS:
                VendorResponse vendorResponse= (VendorResponse) response;
                if(vendorResponse!=null){
                    List<VendorData> dataList = vendorResponse.getData();
                    if(dataList!=null){
                        setData(dataList);
                    }
                }
                break;
        }
    }

    private void setData(List<VendorData> dataList) {
        vendorDataList.addAll(dataList);
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        VendorData vendorData = vendorDataList.get(position);
        holder.tv_title.setText(vendorData.getShopName());
        holder.tv_subtitle.setText(vendorData.getName());
        holder.tv_location.setText(vendorData.getAddress());
        String imageUrl = vendorData.getImageUrl();
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.with(this).load(imageUrl.trim()).into(holder.iv_vendor_image);
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        VendorData vendorData = vendorDataList.get(position);
        Bundle bundle=new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,vendorData);
        startNextActivity(bundle,VendorDetailsActivity.class);
    }

    class Holder {
        TextView tv_title, tv_subtitle, tv_location;
        ImageView iv_vendor_image;

        public Holder(View v) {
            tv_title = (TextView) v.findViewById(R.id.vendorTitle);
            iv_vendor_image = (ImageView) v.findViewById(R.id.vendor_image);
            tv_subtitle = (TextView) v.findViewById(R.id.tv_subtitle_vendor);
            tv_location = (TextView) v.findViewById(R.id.tv_location_vendor);
        }
    }
}
