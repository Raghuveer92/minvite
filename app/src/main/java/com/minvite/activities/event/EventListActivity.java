package com.minvite.activities.event;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;

import com.minvite.R;
import com.minvite.activities.ChatActivity;
import com.minvite.activities.invitation.AddEventActivity;
import com.minvite.activities.postbook.PostBookActivity;
import com.minvite.enums.NotificationType;
import com.minvite.fragments.invitation.EventsTabFragment;
import com.minvite.models.beacons.BeaconData;
import com.minvite.models.invitation.EventData;
import com.minvite.receivers.MinviteReceiver;
import com.minvite.services.BeaconRefreshService;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class EventListActivity extends BaseActivity implements MinviteReceiver.OnReceiveListener {

    private DrawerLayout drawerLayout;
    private EventsTabFragment eventsTabFragment;
    private Menu menu;
    private boolean isAdmin, isBluetooth;
    private MinviteReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        initToolBar(getString(R.string.invitation));
//        Preferences.saveData(AppConstants.PREF_KEYS.OBJECT_ID, "087AD95A-0D61-D29E-FF4E-785D5ADA6E00");
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        eventsTabFragment = new EventsTabFragment();
        addInvitationFragment(eventsTabFragment);
        registerEventRefreshReciver();
    }

    private void registerEventRefreshReciver() {
        IntentFilter filter = new IntentFilter(MinviteReceiver.ACTION_REFRESH_EVENTS);
        receiver = new MinviteReceiver(this);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        int type = bundle.getInt(AppConstants.BUNDLE_KEYS.TYPE);
        if (type == 0) {
            return;
        }
        NotificationType notificationType = NotificationType.findByStatus(type);
        switch (notificationType) {
            case CHAT:
                startNextActivity(bundle, ChatActivity.class);
                break;
            case BEACON_ON:
                BeaconData beaconData = (BeaconData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                EventData event = beaconData.getEvent();
                Intent intent = new Intent(this, PostBookActivity.class);
                Bundle b = new Bundle();
                b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, event);
                intent.putExtras(b);
                startActivity(intent);
                break;
        }
    }

    private void addDrawerFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.lay_drawer, fragment).commit();
    }

    private void addInvitationFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.lay_home_fragment_container, fragment).commitAllowingStateLoss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_invitations, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startActivityForResult(new Intent(this, AddEventActivity.class), AppConstants.REQUEST_CODES.ADD_INVITAION);
                break;
            case R.id.action_admin:
                if (isAdmin) {
                    isAdmin = false;
                } else {
                    isAdmin = true;
                }
                setList(isAdmin);
                break;
            case R.id.action_bluetooth:
                if (isBluetooth) {
                    isBluetooth = false;
                } else {
                    isBluetooth = true;
                }
                setBluetoothState();
                break;
        }
        return false;
    }

    private void setBluetoothState() {
        MenuItem item = menu.findItem(R.id.action_bluetooth);
        if (isBluetooth) {
            item.setIcon(R.drawable.bluetooth_select);
            BeaconRefreshService.startActionRefresh(this);
        } else {
            item.setIcon(R.drawable.bluetooth_unselect);
            BeaconRefreshService.stopActionRefresh(this);
        }
    }

    private void setList(boolean isAdmin) {
        MenuItem item = menu.findItem(R.id.action_admin);
        if (isAdmin) {
            item.setIcon(R.drawable.fav_select);
        } else {
            item.setIcon(R.drawable.fav_unselect);
        }
        eventsTabFragment.setFilterList(isAdmin);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.REQUEST_CODES.ADD_INVITAION && resultCode == RESULT_OK) {
            addInvitationFragment(new EventsTabFragment());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onReceiveResult() {
        eventsTabFragment.refreshData();
    }
}
