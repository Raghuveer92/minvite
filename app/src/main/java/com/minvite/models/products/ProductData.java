package com.minvite.models.products;

import com.minvite.models.BaseAdapterModel;
import com.minvite.models.BaseApi;

/**
 * Created by raghu on 17/11/16.
 */

public class ProductData extends BaseApi{
    String name;
    String ImageUrl;
    String price;
    String vendorId;
    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getPrice() {
        return price;
    }

    public String getVendorId() {
        return vendorId;
    }
}
