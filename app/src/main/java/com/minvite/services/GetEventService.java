package com.minvite.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.HttpRestService;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class GetEventService extends IntentService {
    private static final String ACTION_EVENT_TYPE = "com.minvite.action.EVENT_TYPE";
    private static final String ACTION_EVENT_CAT = "com.minvite.action.EVENT_CAT";

    public GetEventService() {
        super("GetEventService");
    }

    public static void startActionEVENT_TYPE(Context context) {
        Intent intent = new Intent(context, GetEventService.class);
        intent.setAction(ACTION_EVENT_TYPE);
        context.startService(intent);
    }
    public static void startActionEVENT_CAT(Context context) {
        Intent intent = new Intent(context, GetEventService.class);
        intent.setAction(ACTION_EVENT_CAT);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_EVENT_TYPE.equals(action)) {
                HttpRestService service = new HttpRestService();
                try {
                    HttpParamObject object = new HttpParamObject();
                    object.setUrl(AppConstants.PAGE_URL.EVENT_TYPE);
                    Object response = service.getData(object);
                    if (response != null) {
                        Preferences.saveData(AppConstants.PREF_KEYS.EVENT_TYPES_INSTANCE, response.toString());
                    }
                    Log.i("msg", "event type response: " + response.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (ACTION_EVENT_CAT.equals(action)) {
                HttpRestService service = new HttpRestService();
                try {
                    HttpParamObject object = new HttpParamObject();
                    object.setUrl(AppConstants.PAGE_URL.EVENT_CAT);
                    Object response = service.getData(object);
                    if (response != null) {
                        Preferences.saveData(AppConstants.PREF_KEYS.EVENT_CAT_INSTANCE, response.toString());
                    }
                    Log.i("msg", "event cat response: " + response.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
