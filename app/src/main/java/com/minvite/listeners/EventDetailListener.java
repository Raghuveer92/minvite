package com.minvite.listeners;

import com.minvite.util.EventType;

/**
 * Created by RAHU on 14-07-2016.
 */
public interface EventDetailListener {
    void setEventType(EventType eventType);
    void setTitle(String title);
}
