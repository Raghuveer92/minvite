package com.minvite.async_tasks;

import android.os.AsyncTask;

import com.minvite.models.user.UserProfile;
import com.minvite.models.postbook.EventPost;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.HttpRestService;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 16/11/16.
 */

public class LikePostTask extends AsyncTask<EventPost, Void, EventPost> {

    public static void like(EventPost eventPost){
        List<UserProfile> likes = eventPost.getLikes();
        boolean isLike = false;
        UserProfile userProfile = UserProfile.getInstance();
        for (UserProfile profile : likes) {
            if (profile.getObjectId().equals(userProfile.getObjectId())) {
                isLike=true;
                userProfile=profile;
                break;
            }
        }
        if(isLike){
            likes.remove(userProfile);
        }else {
            likes.add(userProfile);
        }
        new LikePostTask().execute(eventPost);
    }

    @Override
    protected EventPost doInBackground(EventPost... eventPosts) {
        if (eventPosts.length == 0) {
            return null;
        }
        EventPost eventPost = eventPosts[0];
        List<UserProfile> likes = eventPost.getLikes();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_EVENT_POST + "/" + eventPost.getObjectId());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("likes", getLikeJson(likes));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setPutMethod();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(EventPost.class);
        HttpRestService httpRestService = new HttpRestService();
        try {
            return (EventPost) httpRestService.getData(httpParamObject);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONArray getLikeJson(List<UserProfile> likes) throws JSONException {
        if (likes == null) {
            likes = new ArrayList<>();
        }
        JSONArray jsonArray = new JSONArray();
        for (UserProfile userProfile : likes) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("___class", "Users");
            jsonObject.put("objectId", userProfile.getObjectId());
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }
}
