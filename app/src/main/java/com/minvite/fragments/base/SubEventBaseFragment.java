package com.minvite.fragments.base;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.minvite.R;
import com.minvite.activities.postbook.SubEventsDetailActivity;
import com.minvite.async_tasks.LikeSubEventTask;
import com.minvite.models.user.UserProfile;
import com.minvite.models.guest.GuestData;
import com.minvite.models.guest.GuestListResponse;
import com.minvite.models.invitation.EventData;
import com.minvite.models.sub_events.SubEventData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by raghu on 21/10/16.
 */

public abstract class SubEventBaseFragment extends BaseFragment {
    private String objectId;
    private String eventId;
    protected GuestListResponse myGuestResponce;
    private boolean isLike;
    protected SubEventData subEventData;
    private int progressTaskCode;
    protected static final int REQ_CODE_GET_DETAIL = 12;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_accept:
                setStatus(AppConstants.INVITATION_STATUS.ACCEPTED);
                setButtonStatus(AppConstants.INVITATION_STATUS.ACCEPTED);
                break;
            case R.id.btn_may_be:
                setStatus(AppConstants.INVITATION_STATUS.MAY_BE);
                setButtonStatus(AppConstants.INVITATION_STATUS.MAY_BE);
                break;
            case R.id.btn_reject:
                setStatus(AppConstants.INVITATION_STATUS.REJECTED);
                setButtonStatus(AppConstants.INVITATION_STATUS.REJECTED);
                break;
            case R.id.lay_like_comment:
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, subEventData);
                Intent intent = new Intent(getActivity(), SubEventsDetailActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQ_CODE_GET_DETAIL);
                break;
            case R.id.iv_edit:
                showVisibility(R.id.lay_btns);
                hideVisibility(R.id.lay_status);
                break;
        }
    }

    protected void setStatus(String status) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST + "/" + objectId);
        httpParamObject.setPutMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(GuestData.class);
        executeTask(AppConstants.TASK_CODES.UPDATE_STATUS, httpParamObject);
    }

    public String getObjectId() {
        return objectId;
    }

    protected void getStatus(SubEventData subEventData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
        String mobile = "";
        UserProfile userProfile = UserProfile.getInstance();
        if (userProfile != null) {
            mobile = userProfile.getMobile();
        }
        if (!TextUtils.isEmpty(mobile)) {
            if (mobile.length() > 10) {
                mobile = mobile.substring((mobile.length()) - 10, mobile.length());
            }
        }
        httpParamObject.addParameter("where", "subEventId='" + subEventData.getObjectId() + "' and guestMobile=" + mobile);
        httpParamObject.setClassType(GuestListResponse.class);
        executeTask(AppConstants.TASK_CODES.GUEST_LIST, httpParamObject);
    }

    protected void getMySubEvents(EventData eventData) {
        eventId = eventData.getObjectId();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
        String mobile = "";
        UserProfile userProfile = UserProfile.getInstance();
        if (userProfile != null) {
            mobile = userProfile.getMobile();
        }
        if (!TextUtils.isEmpty(mobile)) {
            if (mobile.length() > 10) {
                mobile = mobile.substring((mobile.length()) - 10, mobile.length());
            }
        }
        httpParamObject.addParameter("where", "eventId='" + eventData.getObjectId() + "' and guestMobile=" + mobile);
        httpParamObject.setClassType(GuestListResponse.class);
        executeTask(AppConstants.TASK_CODES.MY_SUB_EVENTS, httpParamObject);
    }

    protected void setLike(SubEventData subEventData, int tvLikeId) {
        LikeSubEventTask.like(subEventData);
        List<UserProfile> likes = subEventData.getLikes();
        setText("" + likes.size(), tvLikeId);
    }

    @Override
    public void showProgressBar() {
        switch (progressTaskCode) {
            case AppConstants.TASK_CODES.UPDATE_EVENT:
            case AppConstants.TASK_CODES.GUEST_LIST:
            case AppConstants.TASK_CODES.UPDATE_STATUS:
                return;
        }
        super.showProgressBar();
    }

    @Override
    public void onPreExecute(int taskCode) {
        progressTaskCode = taskCode;
        super.onPreExecute(taskCode);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            return;
        }
        GuestListResponse guestListResponse;
        switch (taskCode) {
            case AppConstants.TASK_CODES.GUEST_LIST:
                guestListResponse = (GuestListResponse) response;
                if (guestListResponse != null) {
                    List<GuestData> guestDataList = guestListResponse.getData();
                    if (guestDataList != null) {
                        if (guestDataList.size() > 0) {
                            GuestData guestData = guestDataList.get(0);
                            objectId = guestData.getObjectId();
                            String status = guestData.getStatus();
                            setButtonStatus(status);
                        }
                    }
                }
                break;
            case AppConstants.TASK_CODES.MY_SUB_EVENTS:
                guestListResponse = (GuestListResponse) response;
                this.myGuestResponce = guestListResponse;
                if (guestListResponse != null) {
                    List<GuestData> guestDataList = guestListResponse.getData();
                    if (guestDataList != null) {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int x = 0; x < guestDataList.size(); x++) {
                            GuestData guestData = guestDataList.get(x);
                            if (x == 0) {
                                stringBuilder.append("objectId='" + guestData.getSubEventId() + "'");
                            } else {
                                stringBuilder.append(" or objectId='" + guestData.getSubEventId() + "'");
                            }
                        }
                        stringBuilder.append(" and eventId='" + eventId + "'");
                        getSubEvents(stringBuilder.toString());
                    }
                }
                break;
            case AppConstants.TASK_CODES.UPDATE_STATUS:
                GuestData guestData = (GuestData) response;
                if (guestData != null) {
                    setButtonStatus(guestData.getStatus());
                }
                break;
            case AppConstants.TASK_CODES.UPDATE_EVENT:
                SubEventData subEventData = (SubEventData) response;
                if (subEventData != null) {
                    this.subEventData = subEventData;
                }
                break;
        }
    }

    protected abstract void getSubEvents(String params);

    protected boolean checkMyLike(List<UserProfile> likes) {
        if (likes == null) {
            return false;
        }
        String objectId = Preferences.getData(AppConstants.PREF_KEYS.OBJECT_ID, "");
        if (!TextUtils.isEmpty(objectId)) {
            for (UserProfile userProfile : likes) {
                if (objectId.equals(userProfile.getObjectId())) {
                    return true;
                }
            }
        }
        return false;
    }

    protected abstract void setButtonStatus(String status);
}
