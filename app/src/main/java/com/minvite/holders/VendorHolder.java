package com.minvite.holders;

import android.view.View;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.holders.BaseHolder;
import com.minvite.models.vendor.VendorData;

import simplifii.framework.utility.Util;

/**
 * Created by raghu on 15/11/16.
 */

public class VendorHolder extends BaseHolder {
    private final TextView tvDescription;
    private VendorData vendorData;

    public VendorHolder(View itemView) {
        super(itemView);
        setOnClickListener(R.id.rl_contact,R.id.lay_location);
        tvDescription=findTv(R.id.tv_description);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        vendorData = (VendorData) obj;
        tvDescription.setText(vendorData.getDescription());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_contact:
                Util.call(context, vendorData.getContact());
                break;
            case R.id.lay_location:
                Util.openGoogleMap(context,vendorData.getLat(),vendorData.getLng(),vendorData.getAddress());
                break;
        }
    }
}
