package com.minvite.models.chat;

import android.provider.BaseColumns;
import android.text.format.DateUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.minvite.enums.ChatStatus;
import com.minvite.enums.ChatType;
import com.minvite.enums.MessageType;
import com.minvite.models.user.UserProfile;

import java.io.Serializable;

/**
 * Created by raghu on 28/11/16.
 */

@Table(name = "ChatModel", id= BaseColumns._ID)
public class ChatModel extends Model implements Serializable{

    @Column(name = "status")
    int status;

    @Column(name = "type")
    int type;

    @Column(name = "eventId")
    String eventId;

    @Column(name = "message")
    String message;

    @Column(name = "messageType")
    int messageType;

    @Column(name = "fileUrl")
    String fileUrl;

    @Column(name = "timeStamp")
    long timeStamp;

    @Column(name = "myNumber")
    String myNumber;

    @Column(name = "toUserNumber")
    String toUserNumber;

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventId() {
        return eventId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int  getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMyNumber() {
        return myNumber;
    }

    public void setMyNumber(String myNumber) {
        this.myNumber = myNumber;
    }

    public String getToUserNumber() {
        return toUserNumber;
    }

    public void setToUserNumber(String toUserNumber) {
        this.toUserNumber = toUserNumber;
    }
    public static ChatModel getInstance(ChatStatus status, ChatType type, String eventId, String message, MessageType messageType, String fileUrl, String toUser) {
        ChatModel chatModel=new ChatModel();
        chatModel.status = status.getStatus();
        chatModel.type = type.getStatus();
        chatModel.eventId = eventId;
        chatModel.message = message;
        chatModel.messageType = messageType.getType();
        chatModel.fileUrl = fileUrl;
        chatModel.timeStamp = System.currentTimeMillis();
        chatModel.myNumber = UserProfile.getInstance().getMobile();
        chatModel.toUserNumber = toUser;
        return chatModel;
    }

    public String getFormatedTime() {
        return  DateUtils.getRelativeTimeSpanString(timeStamp, System.currentTimeMillis(), 0L).toString();
    }
}
