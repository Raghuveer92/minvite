package com.minvite.models;

/**
 * Created by nbansal2211 on 25/08/16.
 */
public class BaseAdapterModel extends BaseApi {
    private int viewType;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
