package com.minvite.models.postbook;

import android.net.Uri;

import com.minvite.models.BaseAdapterModel;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 15/11/16.
 */

public class VideoModel extends BaseAdapterModel {
    String videoUrl;
    String videoPath;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.VIDEO;
    }
}
