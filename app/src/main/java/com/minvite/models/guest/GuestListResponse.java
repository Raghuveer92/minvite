package com.minvite.models.guest;

import com.minvite.models.BaseApi;

import java.util.List;

/**
 * Created by raghu on 3/10/16.
 */

public class GuestListResponse extends BaseApi{
    List<GuestData> data;

    public List<GuestData> getData() {
        return data;
    }
}
