package com.minvite.services;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.minvite.activities.event.EventListActivity;
import com.minvite.application.AppController;
import com.minvite.models.beacons.BeaconResponse;
import com.minvite.util.BluetoothUtils;

import org.json.JSONException;

import java.sql.SQLException;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

import static weborb.util.ThreadContext.context;

public class BeaconRefreshService extends IntentService {
    private static final String ACTION_REFRESH = "com.minvite.services.action.FOO";

    public BeaconRefreshService() {
        super("BeaconRefreshService");
    }

    public static void startActionRefresh(Context context) {
        Intent intent = new Intent(context, BeaconRefreshService.class);
        intent.setAction(ACTION_REFRESH);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_REFRESH.equals(action)) {
                if (AppController.isPermissionGranted(this, Manifest.permission.BLUETOOTH)) {
                    if (AppController.isPermissionGranted(this, Manifest.permission.BLUETOOTH_ADMIN)) {
                        Util.setBluetooth(true);
                    }
                }
                handleRefreshData();
            }
        }
    }

    private void handleRefreshData() {
        boolean connectingToInternet = Util.isConnectingToInternet(this);
        if (!connectingToInternet) {
            BeaconResponse beaconResponse = BeaconResponse.getInstance();
            if (null != beaconResponse) {
                startScan();
            }
        } else {
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.BEACONS);
            httpParamObject.setClassType(BeaconResponse.class);
            Service service = ServiceFactory.getInstance(this, AppConstants.TASK_CODES.GET_MY_EVENTS);
            try {
                BeaconResponse beaconResponse = (BeaconResponse) service.getData(httpParamObject);
                beaconResponse.saveToPreference();
                startScan();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (RestException e) {
                e.printStackTrace();
            }
        }
    }

    private void startScan() {
        if (AppController.isPermissionGranted(this, Manifest.permission.BLUETOOTH)) {
            if (AppController.isPermissionGranted(this, Manifest.permission.BLUETOOTH_ADMIN)) {
                Util.setBluetooth(true);
                ScanService.startServiceToScan(this);
            }
        }
    }

    public static void stopActionRefresh(Context context) {
        ScanService.stopScanService(context);
    }
}
