package com.minvite.models.venues;

import com.minvite.models.BaseApi;

import java.io.Serializable;

/**
 * Created by raghu on 17/11/16.
 */

public class VenueData extends BaseApi implements Serializable{
    String name;
    String imageUrl;
    String iconUrl;

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getIconUrl() {
        return iconUrl;
    }
}
