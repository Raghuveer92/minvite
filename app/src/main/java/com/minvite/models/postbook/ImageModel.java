package com.minvite.models.postbook;

import com.minvite.models.BaseAdapterModel;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 15/11/16.
 */

public class ImageModel extends BaseAdapterModel {
    private String imageUrl;
    private String imagePath;
    private EventPost eventPost;
    private OnRemoveImageListener onRemoveImageListener;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.IMAGE;
    }

    public OnRemoveImageListener getOnRemoveImageListener() {
        return onRemoveImageListener;
    }

    public void setOnRemoveImageListener(OnRemoveImageListener onRemoveImageListener) {
        this.onRemoveImageListener = onRemoveImageListener;
    }

    public EventPost getEventPost() {
        return eventPost;
    }

    public void setEventPost(EventPost eventPost) {
        this.eventPost = eventPost;
    }

    public interface OnRemoveImageListener{
        void onRemove(ImageModel imageModel);
    }
}
