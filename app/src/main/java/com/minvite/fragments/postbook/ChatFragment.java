package com.minvite.fragments.postbook;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.content.ContentProvider;
import com.minvite.R;
import com.minvite.enums.ChatStatus;
import com.minvite.enums.ChatType;
import com.minvite.enums.MessageType;
import com.minvite.models.user.UserProfile;
import com.minvite.models.chat.ChatModel;
import com.minvite.models.chat.FCM_ChatModel;
import com.minvite.services.ChatService;

import simplifii.framework.ListAdapters.CustomCursorAdapter;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by raghu on 28/11/16.
 */

public class ChatFragment extends BaseFragment implements CustomCursorAdapter.CustomCursorAdapterInterface {
    String toUserMobile;
    String eventId;
    private ListView listView;
    private CustomCursorAdapter customCursorAdapter;
    private EditText editText;
    private UserProfile userProfile;

    public static ChatFragment getInstance(String toUserMobile, String eventId) {
        ChatFragment chatFragment = new ChatFragment();
        chatFragment.toUserMobile = toUserMobile;
        chatFragment.eventId = eventId;
        return chatFragment;
    }

    @Override
    public void initViews() {
        userProfile = UserProfile.getInstance();
        editText = (EditText) findView(R.id.et_chat);
        listView = (ListView) findView(R.id.list_chat);
        customCursorAdapter = new CustomCursorAdapter(getActivity(), null, false, R.layout.row_chat, ChatFragment.this);
        listView.setAdapter(customCursorAdapter);
        setOnClickListener(R.id.iv_send);
        initLoader();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send:
                String msg = editText.getText().toString();
                if(!TextUtils.isEmpty(msg.trim())){
                    sendMessage(msg.trim());
                    editText.setText("");
                }
                break;
        }
    }

    private void sendMessage(String message) {
        ChatModel chatModel = ChatModel.getInstance(ChatStatus.PENDING, ChatType.OUT_GOING, eventId, message, MessageType.TEXT, null, toUserMobile);
        Long aLong = chatModel.save();
        if(aLong>0){
            customCursorAdapter.notifyDataSetChanged();
            FCM_ChatModel fcm_chatModel=new FCM_ChatModel();
            fcm_chatModel.setChatModel(chatModel);
            ChatService.sendMessage(getActivity(),fcm_chatModel);
        }
    }

    private void initLoader() {
        getActivity().getSupportLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int arg0, Bundle cursor) {
                return new CursorLoader(getActivity(),
                        ContentProvider.createUri(ChatModel.class, null),
                        null, null, null, null
                );
            }

            @Override
            public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
                if (cursor != null) {
                    customCursorAdapter.swapCursor(cursor);
                }
            }

            @Override
            public void onLoaderReset(Loader<Cursor> arg0) {
                customCursorAdapter.swapCursor(null);
            }
        });
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_chat;
    }

    @Override
    public void bindView(View convertView, Context context, Cursor c) {
        Holder holder;
//        if (convertView == null) {
//            convertView = LayoutInflater.from(context).inflate(R.layout.row_chat, null);
//            holder = new Holder(convertView);
//            convertView.setTag(holder);
//        }else {
//            holder= (Holder) convertView.getTag();
//        }
        holder = new Holder(convertView);
        String message = c.getString(4);
        String time = c.getString(7);

        holder.tvMessage.setText(message);
        holder.tvTime.setText(time);
    }

    class Holder {
        TextView tvMessage, tvTime;

        public Holder(View view) {
            tvMessage = (TextView) view.findViewById(R.id.tv_message);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
        }
    }
}
