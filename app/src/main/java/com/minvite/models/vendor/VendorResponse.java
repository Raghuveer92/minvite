package com.minvite.models.vendor;

import com.minvite.models.BaseApi;
import com.minvite.models.venues.VenueData;

import java.util.List;

/**
 * Created by raghu on 17/11/16.
 */

public class VendorResponse extends BaseApi {
    List<VendorData> data;

    public List<VendorData> getData() {
        return data;
    }
}
