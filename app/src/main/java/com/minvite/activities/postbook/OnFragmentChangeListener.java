package com.minvite.activities.postbook;

import android.support.v4.app.Fragment;

/**
 * Created by raghu on 15/9/16.
 */
public interface OnFragmentChangeListener {
    void onChangeFragment(Fragment fragment,boolean isAddToBackStack);
}
