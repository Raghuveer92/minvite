package com.minvite.models.invitation;

import com.minvite.models.BaseApi;
import com.minvite.models.user.UserProfile;

/**
 * Created by Admin on 20-Jul-16.
 */
public class EventData extends BaseApi {
    String name;
    String time;
    String location;
    String description;
    EventTypeModel type;
    String videoUrl;
    String category;
    String imageUrl;
    String datetime;
    UserProfile user;
    String eventCode;
    String subEventName;

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public EventTypeModel getType() {
        return type;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getCategory() {
        return category;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getSubEventName() {
        return subEventName;
    }

    public UserProfile getUser() {
        return user;
    }

    public boolean isAdmin(){
        UserProfile instance = UserProfile.getInstance();
        if(user!=null&&instance!=null){
            if(user.getObjectId().equals(instance.getObjectId())){
                return true;
            }
        }
        return false;
    }
}
