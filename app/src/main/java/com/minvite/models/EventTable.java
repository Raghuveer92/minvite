package com.minvite.models;

import com.minvite.models.invitation.EventData;

/**
 * Created by raghu on 26/9/16.
 */

public class EventTable extends BaseApi{
    String userId;
    String eventCode;
    EventData event;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public EventData getEvent() {
        return event;
    }
}
