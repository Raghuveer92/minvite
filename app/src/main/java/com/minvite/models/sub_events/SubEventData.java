package com.minvite.models.sub_events;

import com.minvite.models.BaseApi;
import com.minvite.models.user.UserProfile;
import com.minvite.models.invitation.EventTypeModel;
import com.minvite.models.postbook.Comment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by raghu on 11/10/16.
 */
public class SubEventData extends BaseApi{
    private String name;
    private String location;
    private String description;
    private EventTypeModel type;
    private String videoUrl;
    private String category;
    private String imageUrl;
    private long datetime;
    private String website;
    private String eventId;
    private UserProfile user;
    private String eventCode;
    private String latitude;
    private String longitude;
    private List<UserProfile> likes;
    private List<Comment> comments;

    public long getDatetime() {
        return datetime;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getWebsite() {
        return website;
    }

    public String getName() {
        return name;
    }

    public String getTime() {
        if(datetime>0){
            Calendar calendar=Calendar.getInstance();
            calendar.setTimeInMillis(datetime);
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd MMM,yyyy hh:mm a");
            String format = simpleDateFormat.format(calendar.getTime());
            return format;
        }
        return "";
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public EventTypeModel getType() {
        return type;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getCategory() {
        return category;
    }

    public String getImageUrl() {
        return imageUrl;
    }


    public String getEventId() {
        return eventId;
    }

    public UserProfile getUser() {
        return user;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public List<UserProfile> getLikes() {
        return likes;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
