package com.minvite.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.minvite.R;
import com.minvite.activities.SplashActivity;
import com.minvite.application.AppController;
import com.minvite.enums.NotificationType;
import com.minvite.models.beacons.BeaconData;
import com.minvite.util.BluetoothLeDeviceStore;
import com.minvite.util.BluetoothLeScanner;
import com.minvite.util.BluetoothUtils;
import com.minvite.util.NotificationsUtil;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;
import uk.co.alt236.bluetoothlelib.device.BluetoothLeDevice;
import uk.co.alt236.bluetoothlelib.device.beacon.ibeacon.IBeaconDevice;

import static weborb.util.ThreadContext.context;

public class ScanService extends Service {

    public static final String ACTION_START_SCAN = "startScan";
    public static final String ACTION_STOP_SCAN = "stopScan";
    private BluetoothUtils mBluetoothUtils;
    private BluetoothLeScanner mScanner;
    private BluetoothLeDeviceStore mDeviceStore;

    private static final String TAG = "ScanService";
    private static final double MIN_ACCURACY = 5.0;
    private static final long MIN_TIMEGAP = 1 * 10 * 1000;//5 min

    private int id=0;
    private final BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
//            Log.d(TAG, "Le scan :" + device.getName() + ", UDID : " + device.getAddress());
            final BluetoothLeDevice deviceLe = new BluetoothLeDevice(device, rssi, scanRecord, System.currentTimeMillis());
            mDeviceStore.addDevice(deviceLe);
            IBeaconDevice d = null;
            try {
                d = new IBeaconDevice(deviceLe);
            } catch (Exception e) {
                Log.e(TAG, "Not beacon data" + deviceLe.getAddress());
            }
//            Log.e(TAG, "Scanning");
            if (d == null) return;
            final String accuracy = AppConstants.DOUBLE_TWO_DIGIT_ACCURACY.format(d.getAccuracy());
            if ("IMMEDIATE".equalsIgnoreCase(d.getDistanceDescriptor().toString()) || "NEAR".equalsIgnoreCase(d.getDistanceDescriptor().toString())) {
                BeaconData sb = AppController.getServerBeacon(d.getMajor() + "");
                if (sb != null && checkTimestamp(sb.getBeaconMajor())) {
                    Preferences.saveData(sb.getBeaconMajor(), System.currentTimeMillis());
                    NotificationsUtil.showBeaconNotification(ScanService.this,sb);
                }
            }
        }
    };

    private boolean checkTimestamp(String major) {
        long currentTimeInMillis = System.currentTimeMillis();
        long lastKnownTime = Preferences.getData(major, 0L) + MIN_TIMEGAP;
        return currentTimeInMillis > lastKnownTime;
    }

    public ScanService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBluetoothUtils = new BluetoothUtils(this);
        mScanner = new BluetoothLeScanner(mLeScanCallback, mBluetoothUtils);
        mDeviceStore = new BluetoothLeDeviceStore();
    }

    public static void startServiceToScan(Context context) {
        Intent i = new Intent(context, ScanService.class);
        i.setAction(ACTION_START_SCAN);
        context.startService(i);
    }
    public static void stopScanService(Context context) {
        Intent i = new Intent(context, ScanService.class);
        i.setAction(ACTION_START_SCAN);
        context.stopService(i);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (ACTION_START_SCAN.equalsIgnoreCase(action)) {
                startScan();
            }
            if (ACTION_STOP_SCAN.equalsIgnoreCase(action)) {
                stopScan();
            }
        } else {
            return START_NOT_STICKY;
        }
        return START_STICKY;
    }

    private void startScan() {
        final boolean mIsBluetoothOn = mBluetoothUtils.isBluetoothOn();
        final boolean mIsBluetoothLePresent = mBluetoothUtils.isBluetoothLeSupported();
        mDeviceStore.clear();

        if (mIsBluetoothOn && mIsBluetoothLePresent) {
            mScanner.scanLeDevice(-1, true);
        } else {
            stopSelf();
        }
    }

    private void stopScan() {
    }
}
