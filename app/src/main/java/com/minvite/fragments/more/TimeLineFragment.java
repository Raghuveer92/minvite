package com.minvite.fragments.more;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.minvite.R;
import com.minvite.models.invitation.EventData;
import com.minvite.models.time_line.TimeLineData;
import com.minvite.models.time_line.TimeLineResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 15/9/16.
 */
public class TimeLineFragment extends BaseFragment implements CustomListAdapterInterface {
    private List<TimeLineData> ourStories = new ArrayList<>();
    private EventData eventData;
    private CustomListAdapter customListAdapter;
    private ListView listView;

    public static TimeLineFragment getInstance(EventData eventData) {
        TimeLineFragment timeLineResponse = new TimeLineFragment();
        timeLineResponse.eventData = eventData;
        return timeLineResponse;
    }

    @Override
    public void initViews() {
        initToolBar(getString(R.string.our_story));
        setHasOptionsMenu(true);
        listView = (ListView) findView(R.id.list_our_story);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_our_story, ourStories, this);
        listView.setAdapter(customListAdapter);
        if (eventData != null) {
            getData();
        }
    }

    private void getData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.TIME_LINE);
        httpParamObject.addParameter("where", "eventId='" + eventData.getObjectId() + "'");
        httpParamObject.setClassType(TimeLineResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_TIME_LINE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        listView.setEmptyView(findView(R.id.emptyView));
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_TIME_LINE:
                TimeLineResponse timeLineResponse = (TimeLineResponse) response;
                if (timeLineResponse != null) {
                    List<TimeLineData> data = timeLineResponse.getData();
                    if (data != null) {
                        setData(data);
                    }
                }
                break;
        }
    }

    private void setData(List<TimeLineData> data) {
        ourStories.addAll(data);
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_time_line;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
        }
        TimeLineData timeLineData = ourStories.get(position);
        setText(R.id.tv_title, timeLineData.getTitle(), convertView);
        setText(R.id.tv_year, timeLineData.getStoryTime(), convertView);
        setText(R.id.tv_description, timeLineData.getDescription(), convertView);
        if (position % 2 == 0) {
            convertView.setBackgroundColor(getResourceColor(R.color.light_grayy));
        } else {
            convertView.setBackgroundColor(getResourceColor(R.color.white));
        }
        String imageUrl = timeLineData.getImageUrl();
        if(!TextUtils.isEmpty(imageUrl)){
            ImageView imageView= (ImageView) convertView.findViewById(R.id.iv_Our_Story_Image);
            Picasso.with(getActivity()).load(imageUrl).into(imageView);
        }
        return convertView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.our_story, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {

        }
        return false;
    }
}
