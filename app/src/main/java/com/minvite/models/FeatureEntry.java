package com.minvite.models;

/**
 * Created by robin.bansal on 6/7/16.
 */
public class FeatureEntry {

    private String iconUrl;
    private String name;

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}