package com.minvite.models.venues;

import com.minvite.models.BaseApi;

import java.util.List;

/**
 * Created by raghu on 17/11/16.
 */

public class VenuesResponse extends BaseApi {
    List<VenueData> data;

    public List<VenueData> getData() {
        return data;
    }
}
