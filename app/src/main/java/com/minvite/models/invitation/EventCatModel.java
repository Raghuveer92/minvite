package com.minvite.models.invitation;

import java.io.Serializable;

/**
 * Created by Admin on 20-Jul-16.
 */
public class EventCatModel implements Serializable {
    String cat_name;
    String cat_id;
    String objectId;

    public String getCat_name() {
        return cat_name;
    }

    public String getCat_id() {
        return cat_id;
    }

    public String getObjectId() {
        return objectId;
    }
}
