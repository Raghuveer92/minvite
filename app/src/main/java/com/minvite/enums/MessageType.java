package com.minvite.enums;

/**
 * Created by raghu on 28/11/16.
 */

public enum MessageType {
    TEXT(0),IMAGE(1),VIDEO(2),MAP(3);
    int type;

    MessageType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
    public static MessageType findByStatus(int status) {
        for (MessageType chatStatus : MessageType.values()) {
            if (chatStatus.type == status)
                return chatStatus;
        }
        return TEXT;
    }
}
