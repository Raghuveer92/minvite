package com.minvite.activities.postbook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.minvite.R;
import com.minvite.activities.FullScreenVideoActivity;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.async_tasks.CommentPostTask;
import com.minvite.async_tasks.LikePostTask;
import com.minvite.models.BaseAdapterModel;
import com.minvite.models.user.UserProfile;
import com.minvite.models.postbook.Comment;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.postbook.ImageModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import nz.co.delacour.exposurevideoplayer.FullScreenClickListener;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class PostDetailActivity extends BaseActivity {

    private LayoutInflater inflater;
    private LinearLayout layCommentContainer;
    private EditText etComment;
    private EventPost eventPost;
    private RecyclerView recyclerView;
    private List<BaseAdapterModel> adapterModels = new ArrayList<>();
    private BaseRecycleAdapter baseRecycleAdapter;
    private boolean refresh;
    private LikeButton likeButton;
    private boolean isLikedFromUser;
    private Comment newComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postbook_detail);
        initToolBar("Live Post");
        likeButton = (LikeButton) findViewById(R.id.btn_like);
        layCommentContainer = (LinearLayout) findViewById(R.id.lay_comment_container);
        setRecycler();
        inflater = LayoutInflater.from(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            eventPost = (EventPost) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (eventPost != null) {
                setData(eventPost);
                String videoUrl = eventPost.getVideoUrl();
                setVideo(videoUrl);
            }
        }
        setOnClickListener(R.id.sent_comment);
        etComment = (EditText) findViewById(R.id.et_post_comment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sent_comment:
                String comment = etComment.getText().toString().trim();
                if (!TextUtils.isEmpty(comment)) {
                    Comment newComment = CommentPostTask.sendComment(comment, eventPost);
                    eventPost.getComments().add(newComment);
                    setComments(eventPost.getComments());
                    Util.hideKeyboard(PostDetailActivity.this,etComment);
                    etComment.setText("");
                }
                break;
        }
    }

    private void setRecycler() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        baseRecycleAdapter = new BaseRecycleAdapter(this, adapterModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(baseRecycleAdapter);
    }
    private void setLikes(EventPost eventPost) {
        isLikedFromUser = false;
        final List<UserProfile> likes = eventPost.getLikes();
        likeButton.setLiked(false);
        if (likes != null) {
            setText("" + likes.size(), R.id.tv_noOfLike);
            UserProfile instance = UserProfile.getInstance();
            for (UserProfile userProfile : likes) {
                if (instance.getObjectId().equals(userProfile.getObjectId())) {
                    likeButton.setLiked(true);
                    break;
                }
                likeButton.setLiked(false);
            }
            isLikedFromUser = true;
            likeButton.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    if (isLikedFromUser) {
                        changeLike();
                    }
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    if (isLikedFromUser) {
                        changeLike();
                    }
                }
            });
        }
    }

    private void changeLike() {
        LikePostTask.like(eventPost);
        setLikes(eventPost);
    }

    private void setData(EventPost eventPost) {
        setText(eventPost.getStatus(), R.id.tv_status);
        showVisibility(R.id.tv_status);
        String imageUrl = eventPost.getImageUrl();
        List<String> imageList = eventPost.getImages();
        if (imageList != null) {
            if (imageList.size() > 1) {
                setImageList(imageList);
            } else if (imageList.size() == 1) {
                setImageUrl(imageList.get(0), R.id.iv_post);
                showVisibility(R.id.iv_post);
            } else if (!TextUtils.isEmpty(imageUrl)) {
                setImage(imageUrl);
            } else {
                recyclerView.setVisibility(View.GONE);
            }
        }
        setLikes(eventPost);
        List<Comment> comments = eventPost.getComments();
        Collections.sort(comments);
        if (comments != null) {
            setComments(comments);
        }
        UserProfile user = eventPost.getUser();
        if (user != null) {
            setImageUrl(user.getImageUrl(), R.id.iv_event_user);
            setText(user.getName(), R.id.tv_post_user_name);
        }

        String time = DateUtils.getRelativeTimeSpanString(eventPost.getCreated(), System.currentTimeMillis(), 0L).toString();
        setText(time, R.id.tv_time_stamp);
    }

    private void setComments(List<Comment> comments) {
        setText(String.valueOf(comments.size()), R.id.tv_no_comments);
        layCommentContainer.removeAllViews();
        for (Comment comment : comments) {
            setComment(comment);
        }
    }

    private void setImage(String imageUrl) {
        ImageModel imageModel = new ImageModel();
        imageModel.setImageUrl(imageUrl);
        adapterModels.add(imageModel);
        baseRecycleAdapter.notifyDataSetChanged();
    }

    private void setImageList(List<String> imageList) {
        for (String imgUrl : imageList) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImageUrl(imgUrl);
            adapterModels.add(imageModel);
        }
        baseRecycleAdapter.notifyDataSetChanged();
    }

    private void setVideo(final String videoUrl) {
        if (!TextUtils.isEmpty(videoUrl)) {
            ExposureVideoPlayer exposureVideoPlayer = (ExposureVideoPlayer) findViewById(R.id.videoview);
            exposureVideoPlayer.init(this);
            exposureVideoPlayer.setFullScreen(false);
            exposureVideoPlayer.setAutoPlay(false);
            try {
                exposureVideoPlayer.setVideoSource(videoUrl);
            }catch (Exception e){

            }
            exposureVideoPlayer.setOnFullScreenClickListener(new FullScreenClickListener() {
                @Override
                public void onToggleClick(boolean isFullscreen) {
                    Bundle bundle=new Bundle();
                    bundle.putString(AppConstants.BUNDLE_KEYS.VIDEO_URL,videoUrl);
                    startNextActivity(bundle, FullScreenVideoActivity.class);
                }
            });
            recyclerView.setVisibility(View.GONE);
        } else {
            hideVisibility(R.id.videoview);
        }
    }

    private void setComment(Comment comment) {
        View view = inflater.inflate(R.layout.row_comment, null);
        setText(comment.getComment(), R.id.tv_comment_text, view);
        String time = DateUtils.getRelativeTimeSpanString(comment.getCreated(), System.currentTimeMillis(), 0L).toString();
        setText(time, R.id.tv_time_stamp, view);
        UserProfile user = comment.getUser();
        if (user != null) {
            setText(user.getName(), R.id.tv_comment_user, view);
            setImageUrl(user.getImageUrl(), R.id.iv_comment_row, view);
        }

        layCommentContainer.addView(view);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.our_invitation, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (refresh) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventPost);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
