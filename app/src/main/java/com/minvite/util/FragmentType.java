package com.minvite.util;

/**
 * Created by robin.bansal on 10/7/16.
 */
public enum FragmentType {

    EVENT_DETAILS(1,"Event Details"),ADD_PAGES(2,"Add Pages");
    private int type;
    private String title;

    public int getType() {
        return type;
    }
    public String getTitle() {
        return title;
    }

    private FragmentType(int type,String title){
        this.type = type;
        this.title = title;
    }

    public static FragmentType getFragmentTypeFromValue(int type){
        for (FragmentType fragmentType : FragmentType.values()) {
            if(fragmentType.getType()==type){
                return fragmentType;
            }
        }
        return null;
    }
}
