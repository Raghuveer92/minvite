package simplifii.framework.utility;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;

public interface AppConstants {

    String DEF_REGULAR_FONT = "Lato-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    LinkedHashMap<Integer,String> storeCategory=new LinkedHashMap<Integer, String>();
    String BASE_PAGE_URL = "http://api.backendless.com/v1";
    String PAGE_SIZE = "20";
    public static final DecimalFormat DOUBLE_TWO_DIGIT_ACCURACY = new DecimalFormat("#.##");


    interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }
    interface REQUEST_CODES {
        int VIDEO_CAPTURE = 10;
        int GOOGLE_SIGHN_IN = 11;
        int REQUEST_CODE_GALLARY = 12;
        int ADD_INVITAION = 13;
        int REFRESH_DATA = 14;
        int BLUETOOTH = 15;
    }


    public static interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }
    public static interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }
    interface EVENT_TYPES {
        String TEXT = "Text";
        String TEXT_AND_IMAGE = "Text & Image";
        String IMAGE = "Image";
        String VIDEO = "Video";
        String WEB = "Web";
        String FULL_VIDEO="Full Video";
        String CARD="Card";
    }

    public static interface STRING_CONSTANTS{
        public static String SLASH = "/";
    }

    interface PAGE_URL {
        String DATA = "data";
        String FEATURES_PAGE_URL = BASE_PAGE_URL + STRING_CONSTANTS.SLASH + DATA + STRING_CONSTANTS.SLASH + FEATURE_TABLE_CONSTANTS.TABLE_NAME;
        String EVENT_REG = "https://api.backendless.com/v1/data/EventDetails?loadRelations=event_type%2Cevent_cat%2Cuser";
        String EVENT_TYPE = "https://api.backendless.com/v1/data/EventType";
        String USERS = "https://api.backendless.com/v1/data/Users";
        String LOGIN = "https://api.backendless.com/v1/users/login";
        String EVENT_CAT = "https://api.backendless.com/v1/data/EventCategory";
        String EVENTS = "https://api.backendless.com/v1/data/Events";
        String GET_INVITATION = "https://api.backendless.com/v1/data/EventDetails";
        String GET_EVENT_POST = "https://api.backendless.com/v1/data/EventPost";
        String COMMENT = "https://api.backendless.com/v1/data/Comments";
        String EVENT_TABLE = "https://api.backendless.com/v1/data/EventTable";
        String GUEST_LIST = "https://api.backendless.com/v1/data/GuestList";
        String SUB_EVENTS = "https://api.backendless.com/v1/data/SubEvents";
        String VENUE = "https://api.backendless.com/v1/data/Venues";
        String VENDORS = "https://api.backendless.com/v1/data/Vendors";
        String GET_PRODUCTS = "https://api.backendless.com/v1/data/Products";
        String ALBUM = "https://api.backendless.com/v1/data/Album";
        String TIME_LINE ="https://api.backendless.com/v1/data/TimeLine" ;
        String UPDATE = "https://api.backendless.com/v1/data/Updates";
        String BEACONS = "https://api.backendless.com/v1/data/beacons";
    }

    public static interface FEATURE_TABLE_CONSTANTS{
        public static String TABLE_NAME = "Features";
        public static String COLUMN_ICON_URL = "IconUrl";
        public static String COLUMN_NAME = "Name";
    }

    public static interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";
        String EVENT_TYPES_INSTANCE = "event_types";
        String USER_INSTANCE = "user_instance";
        String IS_LOGIN = "is_login";
        String OBJECT_ID = "object_id";
        String EVENT_CAT_INSTANCE = "event_cat";
        String EVENT_IDS = "event_ids";
        String FCM_TOKEN = "fcm_token";
        String BEACONS_INSTANCE = "beacon_instance";
        String DEEP_LIST_DATA = "deep_list_data";
    }

    public static interface BUNDLE_KEYS {
        public static final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String NUMBER = "number";
        String SEARCH = "search";
        String EVENT = "event";
        String EVENT_CAT = "event_cat";
        String INVITATION = "invitation";
        String IS_INVITED = "is_invited";
        String SUB_EVENT = "sub_event";
        java.lang.String TYPE = "view_type";
        String KEY_SUB_EVENTS = "sub_events_list";
        String SELETED_SUB_EVENT = "sub_event";
        String VIDEO_URL = "video_url";
        String SELECTED_POSITION = "selected_postion";
    }

    public static interface FRAGMENT_TYPE {
        int VENUES = 1;
        int ALBUM = 2;
        int TIME_LINE = 3;
    }

    public static interface VIEW_TYPE {
        int POST_CARD = 0;
        int WRITE_SOMETHING = 1;
        int VENDOR_DETAIL = 2;
        int IMAGE = 3;
        int VIDEO = 4;
        int PRODUCTS = 5;
        int GRIDE_IMAGE = 6;
        int GRIDE_VIDEO = 7;
    }

    public static interface TASK_CODES {
        int GET_FEATURES_LIST = 0;
        int EVENT_REG = 1;
        int REGISTER = 2;
        int LOGIN = 3;
        int GET_ALL_INVITAIONS = 4;
        int GET_INVITAION = 5;
        int GET_EVENT_POST = 7;
        int COMMENT = 8;
        int UPDATE_EVENT = 9;
        int POST_EVENT = 10;
        int EVENT_TABLE = 11;
        int GET_EVENT_TABLE = 12;
        int GUEST_LIST = 13;
        int UPDATE_STATUS = 14;
        int GET_SUB_EVENTS = 15;
        int GET_MY_EVENTS = 16;
        int MY_SUB_EVENTS = 17;
        int LIKE = 18;
        int GET_VENUES = 19;
        int GET_VENDORS = 20;
        int GET_PRODUCTS = 21;
        int GET_ALBUM = 22;
        int GET_TIME_LINE = 23;
        int GET_USER = 24;
        int UPDATES = 25;
        int ADD_UPDATE = 26;
    }

    public static interface BACKENDLESS_CONSTANTS{
        public static final String APPLICATION_ID_TEXT = "application-id";
        public static final String APPLICATION_ID_VALUE = "8D01CA59-6226-2622-FF06-1A0F33A34200";
        public static final String SECRET_KEY_TEXT = "secret-key";
        public static final String SECRET_KEY_VALUE = "10951BF1-3FF4-ECD0-FF47-8425C16F3F00";
    }

    public static interface EVENT_DETAILS_PAGE_CONSTANTS{
        public static final Integer IMAGES_SELECT_LIMIT_VALUE = 10;
    }

    public interface INVITATION_STATUS {
        String WAITING = "W";
        String ACCEPTED = "A";
        String REJECTED = "R";
        String MAY_BE = "M";
    }

    public interface TYPE {
        String LIST = "list";
    }

    public interface FCM_CONSTANTS {
        String TYPE = "type";
        String CONTENT = "content";
    }

    public interface LINK_TYPE {
        String INVITE = "1";
    }
}
