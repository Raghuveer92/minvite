package com.minvite.holders;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.minvite.R;
import com.minvite.async_tasks.LikePostTask;
import com.minvite.models.postbook.Comment;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.postbook.ImageModel;
import com.minvite.models.user.UserProfile;
import com.squareup.picasso.Picasso;
import com.todddavies.utils.CommanMethodes;

import java.util.List;

/**
 * Created by raghu on 6/12/16.
 */

public class VideoGridHolder extends BaseHolder {
    private ImageView ivThumbnile;
    private TextView numLikes, numComments, tvUserName,timeTv;
    private ImageView ivUser;
    private com.like.LikeButton likeButton;
    private boolean isLikedFromButton;
    private EventPost eventPost;

    public VideoGridHolder(View itemView) {
        super(itemView);
        ivThumbnile = (ImageView) findView(R.id.iv_thumbmile);
        tvUserName = (TextView) itemView.findViewById(R.id.tv_userName);
        likeButton = (com.like.LikeButton) itemView.findViewById(R.id.btn_like);
        numLikes = findTv(R.id.tv_noOfLike);
        timeTv = findTv(R.id.tv_time);
        numComments = findTv(R.id.tv_no_comments);
        ivUser = (ImageView) itemView.findViewById(R.id.iv_user);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ImageModel imageModel = (ImageModel) obj;
        eventPost = imageModel.getEventPost();
        if (eventPost != null) {
            UserProfile user = eventPost.getUser();
            if(user!=null){
                setUserData(user);
            }
            List<UserProfile> likes = eventPost.getLikes();
            if(likes!=null){
                setLikes(likes);
            }
            List<Comment> comments = eventPost.getComments();
            if(comments!=null){
                int size = comments.size();
                numComments.setText(""+size);
            }
            String time = DateUtils.getRelativeTimeSpanString(eventPost.getCreated(), System.currentTimeMillis(), 0L).toString();
            timeTv.setText(time);
            String videoThumbnile = eventPost.getVideoThumbnile();
            if(!TextUtils.isEmpty(videoThumbnile)){
                Bitmap bitmapFromPath = CommanMethodes.getBitmapFromPath(context, videoThumbnile);
                if(bitmapFromPath!=null){
                    ivThumbnile.setImageBitmap(bitmapFromPath);
                }
            }
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRecyclerClick(view, 0);
            }
        });
    }
    private void setUserData(UserProfile user) {
        if (user != null) {
            tvUserName.setText(user.getName());
            String imageUrl = user.getImageUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(context).load(imageUrl).into(ivUser);
            }
        }
    }
    private void setLikes(final List<UserProfile> likes) {
        isLikedFromButton=false;
        likeButton.setLiked(false);
        if (likes != null) {
            numLikes.setText(String.valueOf(likes.size()));
            UserProfile instance = UserProfile.getInstance();
            for (UserProfile userProfile : likes) {
                if (instance.getObjectId().equals(userProfile.getObjectId())) {
                    likeButton.setLiked(true);
                    break;
                }
                likeButton.setLiked(false);
            }
            isLikedFromButton=true;
            likeButton.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    if(isLikedFromButton){
                        changeLike();
                    }
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    if(isLikedFromButton){
                        changeLike();
                    }
                }
            });
        }
    }
    private void changeLike() {
        LikePostTask.like(eventPost);
        setLikes(eventPost.getLikes());
    }
}
