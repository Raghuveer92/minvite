package com.minvite.models.products;

import com.minvite.models.BaseAdapterModel;
import com.minvite.models.BaseApi;

import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 17/11/16.
 */

public class ProductsResponse extends BaseAdapterModel {

    List<ProductData> data;

    public List<ProductData> getData() {
        return data;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.PRODUCTS;
    }

    public void setData(List<ProductData> data) {
        this.data = data;
    }
}
