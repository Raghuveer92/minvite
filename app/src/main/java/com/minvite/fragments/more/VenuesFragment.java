package com.minvite.fragments.more;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.activities.more.VendorsListActivity;
import com.minvite.models.venues.VenueData;
import com.minvite.models.venues.VenuesResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 18/11/16.
 */

public class VenuesFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<VenueData> vendorList = new ArrayList<>();
    CustomListAdapter customListAdapter;
    GridView gvVenue;

    @Override
    public void initViews() {
        initToolBar("Category");
        gvVenue = (GridView) findView(R.id.grid_view);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_venues, vendorList, this);
        gvVenue.setAdapter(customListAdapter);
        gvVenue.setOnItemClickListener(this);
        getData();
    }

    private void getData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.VENUE);
        httpParamObject.setClassType(VenuesResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_VENUES,httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_VENUES:
                VenuesResponse venuesResponse= (VenuesResponse) response;
                if(venuesResponse!=null){
                    List<VenueData> venueDataList = venuesResponse.getData();
                    if(venueDataList!=null&&venueDataList.size()>0){
                        setData(venueDataList);
                    }
                }
                break;
        }
    }

    private void setData(List<VenueData> venueDataList) {
        vendorList.addAll(venueDataList);
        customListAdapter.notifyDataSetChanged();
    }


    @Override
    public int getViewID() {
        return R.layout.activity_venue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        VenueData venueData = vendorList.get(position);
        holder.tv_category_name.setText(venueData.getName());
        String imageUrl = venueData.getImageUrl();
        String iconUrl = venueData.getIconUrl();
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl).into(holder.iv_background);
        }
        if(!TextUtils.isEmpty(iconUrl)){
            Picasso.with(getActivity()).load(iconUrl).into(holder.iv_icon);
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        VenueData venueData = vendorList.get(i);
        Bundle bundle=new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,venueData);
        startNextActivity(bundle,VendorsListActivity.class);
        
    }
    class Holder {
        TextView tv_category_name;
        ImageView iv_background, iv_icon;

        public Holder(View v) {
            tv_category_name = (TextView) v.findViewById(R.id.category_name);
            iv_background = (ImageView) v.findViewById(R.id.category_background_image);
            iv_icon = (ImageView) v.findViewById(R.id.iv_category_icon);
        }
    }
}
