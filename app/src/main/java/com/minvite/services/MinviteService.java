package com.minvite.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.minvite.models.Contact;
import com.minvite.models.InviteUtils;
import com.minvite.models.user.UserProfile;
import com.minvite.models.invitation.EventData;
import com.minvite.models.sub_events.SubEventData;
import com.minvite.models.sub_events.SubEventResponce;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.HttpRestService;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.BackendLessUtils;
import simplifii.framework.utility.Preferences;

public class MinviteService extends IntentService {
    private static final String ACTION_UPLOAD_GUEST_LIST = "com.minvite.services.action.upload_guest_list";
    private static final String ACTION_REFRESH_TOKEN = "action_refresh";
    private static final String ACTION_INVITE_USER = "action_invite_user";

    public MinviteService() {
        super("MinviteService");
    }

    public static void startUploadGuestList(Context context, HashSet<Contact> selectedContacts, ArrayList<SubEventData> subEventData) {
        Intent intent = new Intent(context, MinviteService.class);
        intent.setAction(ACTION_UPLOAD_GUEST_LIST);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, selectedContacts);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.INVITATION, subEventData);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    public static void updateToken(Context context) {
        Intent intent = new Intent(context, MinviteService.class);
        intent.setAction(ACTION_REFRESH_TOKEN);
        context.startService(intent);
    }
    public static void inviteUser(Context context) {
        Intent intent = new Intent(context, MinviteService.class);
        intent.setAction(ACTION_INVITE_USER);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_UPLOAD_GUEST_LIST:
                    Bundle bundle = intent.getExtras();
                    HashSet<Contact> selectedContacts = (HashSet<Contact>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                    ArrayList<SubEventData> subEventDataArrayList = (ArrayList<SubEventData>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.INVITATION);
                    if (selectedContacts != null && subEventDataArrayList != null) {
                        uploadContacts(selectedContacts, subEventDataArrayList);
                    }
                    break;
                case ACTION_REFRESH_TOKEN:
                    onHandleRefreshToken();
                    break;
                case ACTION_INVITE_USER:
                    onHandleInviteUser();
                    break;
            }
        }
    }

    private void onHandleInviteUser() {
        InviteUtils inviteUtils = InviteUtils.getSavedInstance();
        if(inviteUtils==null){
            return ;
        }
        UserProfile userProfile = UserProfile.getInstance();
        if(userProfile==null){
            return ;
        }
        if(userProfile.getObjectId().equals(inviteUtils.getUserId())){
            return ;
        }
        for (String subEventId: inviteUtils.getSubEventIds()) {
            HttpRestService service = new HttpRestService();
            try {
                HttpParamObject object = new HttpParamObject();
                object.setPostMethod();
                object.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("eventCode", inviteUtils.getEventCode());
                jsonObject.put("eventId", inviteUtils.getEventId());
                jsonObject.put("subEventId", subEventId);
                jsonObject.put("guestMobile", userProfile.getMobile());
                jsonObject.put("guestName", userProfile.getName());
                jsonObject.put("status", AppConstants.INVITATION_STATUS.WAITING);
                jsonObject.put("userId", inviteUtils.getUserId());
                object.setJson(jsonObject.toString());
                Object response = service.getData(object);
                Log.i("msg", "Guest List response: " + response.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        GetMyEventService.startGetEvents(this);
    }

    private void onHandleRefreshToken() {
        UserProfile userProfile = UserProfile.getInstance();
        if(userProfile==null){
            return;
        }
        HttpParamObject httpParamObject=new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.USERS+"/"+userProfile.getObjectId());
        httpParamObject.setPutMethod();
        httpParamObject.setClassType(UserProfile.class);
        JSONObject jsonObject=new JSONObject();
        try {
            String fcmToken = Preferences.getData(AppConstants.PREF_KEYS.FCM_TOKEN, "");
            if(TextUtils.isEmpty(fcmToken)){
                return;
            }
            jsonObject.put("fcmToken", fcmToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("http","request json: "+jsonObject.toString());
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        HttpRestService service = (HttpRestService) ServiceFactory.getInstance(this, 111);
        try {
            UserProfile data = (UserProfile) service.getData(httpParamObject);
            if(data!=null){
                UserProfile.saveUser(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
    }

    private void uploadContacts(HashSet<Contact> selectedContacts, ArrayList<SubEventData> subEventDataArrayList) {
        String userId = Preferences.getData(AppConstants.PREF_KEYS.OBJECT_ID, "");
        if (subEventDataArrayList == null) {
            return;
        }
        for (SubEventData subEventData : subEventDataArrayList) {
            for (Contact contact : selectedContacts) {
                String phoneNumber = contact.getPhoneNumber().trim();
                phoneNumber = phoneNumber.replaceAll(" ", "");
                phoneNumber = phoneNumber.replaceAll("-", "");
                phoneNumber = phoneNumber.replaceAll("\\(", "");
                phoneNumber = phoneNumber.replaceAll("\\)", "");

                if (phoneNumber.length() > 10) {
                    phoneNumber = phoneNumber.substring((phoneNumber.length()) - 10, phoneNumber.length());
                }
                HttpRestService service = new HttpRestService();
                try {
                    HttpParamObject object = new HttpParamObject();
                    object.setPostMethod();
                    object.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("eventCode", subEventData.getEventCode());
                    jsonObject.put("eventId", subEventData.getEventId());
                    jsonObject.put("subEventId", subEventData.getObjectId());
                    jsonObject.put("guestMobile", phoneNumber);
                    jsonObject.put("guestName", contact.getName());
                    jsonObject.put("status", AppConstants.INVITATION_STATUS.WAITING);
                    jsonObject.put("userId", userId);
                    object.setJson(jsonObject.toString());
                    Object response = service.getData(object);
                    Log.i("msg", "Guest List response: " + response.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<SubEventData> getSubEvents(EventData eventData) {
        HttpParamObject httpParamObject = BackendLessUtils.getBaseHttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SUB_EVENTS);
        httpParamObject.addParameter("where", "eventId='" + eventData.getObjectId() + "'");
        httpParamObject.setClassType(SubEventResponce.class);
        Service service = ServiceFactory.getInstance(this, AppConstants.TASK_CODES.GET_SUB_EVENTS);
        try {
            SubEventResponce subEventResponce = (SubEventResponce) service.getData(httpParamObject);
            return subEventResponce.getData();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
        return null;
    }
}
