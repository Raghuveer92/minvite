package com.minvite.fragments.invitation;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.minvite.R;
import com.minvite.activities.postbook.OnFragmentChangeListener;
import com.minvite.fragments.sub_event.SubEventsCardFragment;
import com.minvite.models.Contact;
import com.minvite.models.invitation.EventData;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import simplifii.framework.fragments.BaseFragment;

public class SendInvitationFragment extends BaseFragment implements SendInvitation_TabFragment.SelectContactListner, LoaderManager.LoaderCallbacks<Cursor>, PermissionListener {
    HashSet<Contact> selectedContacts = new HashSet<>();
    ArrayList<Contact> contactList;
    private EventData invitaion;
    OnFragmentChangeListener fragmentChangeListener;
    private SendInvitation_TabFragment sendInvitationTabFragment;

    @Override
    public void initViews() {
        initToolBar("Send Invitations");
        setHasOptionsMenu(true);
        new TedPermission(getActivity())
                .setPermissions(Manifest.permission.READ_CONTACTS)
                .setPermissionListener(this).check();
    }

    @Override
    public int getViewID() {
        return R.layout.activity_send_invitation;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }


    public void setFragment() {
        sendInvitationTabFragment = SendInvitation_TabFragment.getInstance(contactList, selectedContacts, this);
        getChildFragmentManager().beginTransaction().add(R.id.fragment_container_send_invitation, sendInvitationTabFragment).commit();
    }

    @Override
    public void onContactSelect() {
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(getActivity(), CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        cursor.moveToFirst();
        contactList = new ArrayList<>();
        while (!cursor.isAfterLast()) {
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            final String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            final String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            final String imageUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
            contactList.add(Contact.getInstance(id, name, number, imageUri));
            cursor.moveToNext();
        }
        Collections.sort(contactList);
        setFragment();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.send_invite, menu);
        SearchView.SearchAutoComplete searchAutoComplete = setSearchView(menu);
        searchAutoComplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sendInvitationTabFragment.filterList(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            if (selectedContacts.size() == 0) {
                showToast(getString(R.string.error_to_select_user));
                return false;
            } else {
//                MinviteService.startUploadGuestList(getActivity(),selectedContacts,invitaion,true);
                showToast(getString(R.string.success_invited));
                SubEventsCardFragment subEventsCardFragment = SubEventsCardFragment.getInstance(fragmentChangeListener, invitaion, true, null, 0);
                fragmentChangeListener.onChangeFragment(subEventsCardFragment,false);
            }
        }
        return false;
    }

    @Override
    public void onPermissionGranted() {
        getLoaderManager().initLoader(1, null, this);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        showToast("");
    }

    public static SendInvitationFragment getInstance(EventData eventData, OnFragmentChangeListener fragmentChangeListener) {
        SendInvitationFragment sendInvitationFragment=new SendInvitationFragment();
        sendInvitationFragment.fragmentChangeListener=fragmentChangeListener;
        sendInvitationFragment.invitaion= eventData;
        return sendInvitationFragment;
    }
}
