package com.minvite.models.beacons;

import com.minvite.models.BaseApi;
import com.minvite.models.invitation.EventData;

/**
 * Created by raghu on 10/12/16.
 */

public class BeaconData extends BaseApi {

    String beaconMajor;
    String beaconMiner;
    String beaconMessage;
    long beconMessageTime;
    String messageReminder;
    EventData event;
    String subEventId;

    public String getMessageReminder() {
        return messageReminder;
    }

    public String getBeaconMajor() {
        return beaconMajor;
    }

    public String getBeaconMiner() {
        return beaconMiner;
    }

    public String getBeaconMessage() {
        return beaconMessage;
    }

    public long getBeconMessageTime() {
        return beconMessageTime;
    }

    public EventData getEvent() {
        return event;
    }

    public String getSubEventId() {
        return subEventId;
    }
}
