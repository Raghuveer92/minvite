package com.minvite.enums;

/**
 * Created by raghu on 28/11/16.
 */

public enum ChatType {
    INCOMING(1), OUT_GOING(2);
    int status;

    ChatType(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public static ChatType findByStatus(int status) {
        for (ChatType chatType : ChatType.values()) {
            if (chatType.status == status)
                return chatType;
        }
        return INCOMING;
    }
}
