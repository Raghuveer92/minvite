package simplifii.framework.utility;

import java.util.List;

/**
 * Created by robin.bansal on 6/7/16.
 */
public class ListUtil {

    public static <T> Boolean isEmpty(List<T> list){
        if(list==null||list.isEmpty())
            return true;
        return false;
    }

    public static <T> Boolean isNotEmpty(List<T> list){
        return !isEmpty(list);
    }
}
