package com.minvite.models.album;

import com.minvite.models.BaseApi;

/**
 * Created by raghu on 18/11/16.
 */

public class AlbumData extends BaseApi{
    String imageUrl;
    String name;
    String eventId;

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getEventId() {
        return eventId;
    }
}
