package com.minvite.models.SendInvitation;


/**
 * Created by RAHU on 15-07-2016.
 */
public class SendInvitationData {
    String guest_name;
    String imageView_invitation;
    Boolean aBoolean;

    public void setGuest_name(String guest_name) {
        this.guest_name = guest_name;
    }

    public void setImageView_invitation(String imageView_invitation) {
        this.imageView_invitation = imageView_invitation;
    }

    public void setaBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
    }
}
