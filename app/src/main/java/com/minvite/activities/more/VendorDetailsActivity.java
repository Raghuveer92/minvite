package com.minvite.activities.more;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.minoon.disco.Disco;
import com.minoon.disco.Event;
import com.minoon.disco.ViewParam;
import com.minvite.R;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.models.BaseAdapterModel;
import com.minvite.models.products.ProductsResponse;
import com.minvite.models.vendor.VendorData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class VendorDetailsActivity extends BaseActivity {
    List<BaseAdapterModel> adapterModels = new ArrayList<>();
    RecyclerView mRecyclerView;
    private Disco mDisco;
    private ImageView mHeaderImage;
    private String SAVE_DISCO_STATE = "saveDiscoState";
    private BaseRecycleAdapter baseRecycleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_details);
        initToolBar(getString(R.string.vendor_detail));
        mRecyclerView = (RecyclerView) findViewById(R.id.a_sample_rv_list);
        mHeaderImage = (ImageView) findViewById(R.id.a_simple_iv_header);
        setRecyclerAdapter(savedInstanceState);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        VendorData vendorData = (VendorData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        String imageUrl = vendorData.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(this).load(imageUrl).into(mHeaderImage);
        }
        setVendorData(vendorData);
        getProducts(vendorData);
    }

    private void getProducts(VendorData vendorData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_PRODUCTS);
        httpParamObject.addParameter("where", "vendorId='" + vendorData.getObjectId() + "'");
        httpParamObject.setClassType(ProductsResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_PRODUCTS,httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_PRODUCTS:
                ProductsResponse productsResponse= (ProductsResponse) response;
                if(productsResponse!=null){
                    adapterModels.add(productsResponse);
                    baseRecycleAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    private void setVendorData(VendorData vendorData) {
        vendorData.setViewType(AppConstants.VIEW_TYPE.VENDOR_DETAIL);
        adapterModels.add(vendorData);
        baseRecycleAdapter.notifyDataSetChanged();
    }

    private void setRecyclerAdapter(Bundle savedInstanceState) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        baseRecycleAdapter = new BaseRecycleAdapter(this, adapterModels);
        mRecyclerView.setAdapter(baseRecycleAdapter);
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                if (parent.getChildAdapterPosition(view) == 0) {
                    DisplayMetrics metrics = view.getContext().getResources().getDisplayMetrics();
                    outRect.top = 300 * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
                }
            }
        });

        // set up disco
        mDisco = new Disco();
        mDisco.addScrollView(mRecyclerView);

        mDisco.addScrollObserver(mHeaderImage, mDisco.getScrollChoreographyBuilder()
                .onScroll()
                .scaleX(1f, 0.8f)
                .scaleY(1f, 0.8f)
                .multiplier(0.7f)
                .build());
        mDisco.addViewObserver(mHeaderImage, mHeaderImage, mDisco.getViewChaseChoreographyBuilder()
                .atTag(ViewParam.TRANSLATION_Y, -200)
                .alpha(0, 1)
                .duration(600)
                .build()
        );

        mDisco.addScrollObserver(toolbar, mDisco.getScrollChoreographyBuilder()
                .at(Event.START_SCROLL_BACK)
                .translationY(0)
                .end()
                .at(Event.START_SCROLL_FORWARD)
                .translationY(-200)
                .build());
//
//        mDisco.addViewObserver(toolbar, mButton, mDisco.getViewChaseChoreographyBuilder()
//                .onChange(ViewParam.TRANSLATION_Y, 0, -200)
//                .translationX(Position.LEFT_OVER, Position.DEFAULT)
//                .interpolator(new DecelerateInterpolator())
//                .alpha(0f, 1f)
//                .build()
//        );

        if (savedInstanceState != null) {
            mDisco.restoreInstanceState(savedInstanceState.getParcelable(SAVE_DISCO_STATE));
        }

        mDisco.setUp();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vendor_details, menu);
        return true;
    }

}
