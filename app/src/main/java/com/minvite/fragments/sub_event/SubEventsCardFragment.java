package com.minvite.fragments.sub_event;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.minvite.R;
import com.minvite.activities.GuestListActivity;
import com.minvite.activities.SendInvitationActivity;
import com.minvite.activities.postbook.OnFragmentChangeListener;
import com.minvite.activities.postbook.PostBookActivity;
import com.minvite.application.AppController;
import com.minvite.enums.DeepLinkParams;
import com.minvite.fragments.base.SubEventBaseFragment;
import com.minvite.models.invitation.EventData;
import com.minvite.models.invitation.EventTypeModel;
import com.minvite.models.sub_events.SubEventData;
import com.minvite.models.sub_events.SubEventResponce;
import com.minvite.models.user.UserProfile;
import com.minvite.services.BeaconRefreshService;

import java.util.ArrayList;
import java.util.List;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;
import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.BackendLessUtils;
import simplifii.framework.utility.Util;

/**
 * Created by raghu on 11/10/16.
 */

public class SubEventsCardFragment extends SubEventBaseFragment implements CustomPagerAdapter.PagerAdapterInterface {


    private ViewPager viewPager;
    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private LinearLayout slide_circle_container;
    private EventData eventData;
    private boolean isEventProfile;
    private OnFragmentChangeListener onFragmentChangeListener;
    private List<View> circleViews = new ArrayList<>();
    private ArrayList<SubEventData> subEventDataList;
    private int selectedPosition;

    public static SubEventsCardFragment getInstance(OnFragmentChangeListener onFragmentChangeListener, EventData eventData, boolean isEventProfile, ArrayList<SubEventData> subEventList, int selectedPosition) {
        SubEventsCardFragment subEventsCardFragment = new SubEventsCardFragment();
        subEventsCardFragment.eventData = eventData;
        subEventsCardFragment.isEventProfile = isEventProfile;
        subEventsCardFragment.onFragmentChangeListener = onFragmentChangeListener;
        if (subEventList != null) {
            subEventsCardFragment.subEventDataList = subEventList;
        }
        subEventsCardFragment.selectedPosition = selectedPosition;
        return subEventsCardFragment;
    }

    @Override
    public void initViews() {
        fragmentList.clear();
        circleViews.clear();
        slide_circle_container = (LinearLayout) findView(R.id.lay_slide_circles);
        initToolBar(getString(R.string.invitation));
        setHasOptionsMenu(true);
        if (eventData != null) {
            if (subEventDataList == null) {
                getMySubEvents(eventData);
            } else {
                setSubEvents(subEventDataList);
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_sub_event_cards;
    }

    @Override
    protected void getSubEvents(String params) {
        HttpParamObject httpParamObject = BackendLessUtils.getBaseHttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SUB_EVENTS);
        httpParamObject.addParameter("where", params);
        httpParamObject.addParameter("where", "eventId='"+eventData.getObjectId()+"'");
        httpParamObject.setClassType(SubEventResponce.class);
        executeTask(AppConstants.TASK_CODES.GET_SUB_EVENTS, httpParamObject);
    }

    @Override
    protected void setButtonStatus(String status) {

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showVisibility(R.id.emptyView);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_SUB_EVENTS:
                SubEventResponce subEventResponce = (SubEventResponce) response;

                if (subEventResponce.isError()) {
                    showVisibility(R.id.emptyView);
                    return;
                }
                subEventDataList = subEventResponce.getData();
                if (subEventDataList != null) {
                    if (subEventDataList.size() > 0) {
                        setSubEvents(subEventDataList);
                    } else {
                        showVisibility(R.id.emptyView);
                    }
                }
                break;
        }
    }

    private void setSubEvents(ArrayList<SubEventData> eventDataList) {
        for (SubEventData subEventData : eventDataList) {
            EventTypeModel typeModel = subEventData.getType();
            subEventData.setEventCode(eventData.getEventCode());
            String type = typeModel.getType();
            switch (type) {
                case AppConstants.EVENT_TYPES.IMAGE:
                case AppConstants.EVENT_TYPES.VIDEO:
                case AppConstants.EVENT_TYPES.TEXT_AND_IMAGE:
                    fragmentList.add(ImageVideoFragment.getInstance(subEventData, eventData));
                    break;
                case AppConstants.EVENT_TYPES.WEB:
                    fragmentList.add(WebEventFragment.getInstance(subEventData, eventData));
                    break;
                case AppConstants.EVENT_TYPES.FULL_VIDEO:
                    fragmentList.add(FullVideoFragment.getInstance(subEventData, eventData));
                    break;
                case AppConstants.EVENT_TYPES.CARD:
                    fragmentList.add(CardEventFragment.getInstance(subEventData, eventData));
                    break;
            }
            addBottomCircle();

        }
        setPager();
    }

    private void setPager() {
        viewPager = (ViewPager) findView(R.id.pager_sub_events);
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), fragmentList, this);
        viewPager.setAdapter(customPagerAdapter);
        viewPager.setClipToPadding(false);
        viewPager.setOffscreenPageLimit(4);

        int margin = Util.convertDpToPixels(16, getActivity());
        viewPager.setPageMargin(margin);
//        viewPager.setOffscreenPageLimit(5);
        viewPager.setCurrentItem(selectedPosition);
        setPagerListener();
        if (circleViews.size() > 0) {
            circleViews.get(0).findViewById(R.id.lay_row_slider).setBackgroundResource(R.drawable.slide_circle_select);
        }
    }


    private void setPagerListener() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                unSelectAll();
                circleViews.get(position).findViewById(R.id.lay_row_slider).setBackgroundResource(R.drawable.slide_circle_select);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomCircle() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.row_slide_circle, null);
        slide_circle_container.addView(view);
        circleViews.add(view);
    }

    private void unSelectAll() {
        for (View view : circleViews) {
            view.findViewById(R.id.lay_row_slider).setBackgroundResource(R.drawable.slide_circle_unselect);
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return fragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return null;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invitation, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle bundle = new Bundle();
        switch (item.getItemId()) {
            case R.id.action_add:
                shareEvent();
//                if (subEventDataList != null) {
//                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, subEventDataList);
//                    startNextActivity(bundle, SendInvitationActivity.class);
//                }
                break;
            case R.id.action_guestList:
                int currentItem = viewPager.getCurrentItem();
                if (currentItem >= 0) {
                    SubEventData subEventData = subEventDataList.get(currentItem);
                    if (subEventData != null) {
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, subEventData);
                        startNextActivity(bundle, GuestListActivity.class);
                    }
                }
                break;
        }
        return false;
    }

    private void shareEvent() {
        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setTitle("Minvite Invitation")
                .setContentDescription("You are invited for a event from minvite")
                .setContentImageUrl(eventData.getImageUrl())
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata(DeepLinkParams.LINK_TYPE.getParamName(), AppConstants.LINK_TYPE.INVITE)
                .addContentMetadata(DeepLinkParams.EVENT_ID.getParamName(), eventData.getObjectId())
                .addContentMetadata(DeepLinkParams.EVENT_CODE.getParamName(), eventData.getEventCode())
                .addContentMetadata(DeepLinkParams.USER_ID.getParamName(), UserProfile.getInstance().getObjectId())
                .addContentMetadata(DeepLinkParams.SUB_EVENT_IDS.getParamName(), getsubEvents());

        LinkProperties linkProperties = new LinkProperties();
        branchUniversalObject.generateShortUrl(getActivity(), linkProperties, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    Log.i("MyApp", "got my Branch link to share: " + url);
                    ShareCompat.IntentBuilder
                            .from(getActivity())
                            .setText(url)
                            .setType("text/plain")
                            .setChooserTitle("Share your invitation")
                            .startChooser();
                }
            }
        });
    }

    private String getsubEvents() {
        StringBuilder stringBuilder=new StringBuilder();
        boolean isFirst=true;
        for(SubEventData subEventData:subEventDataList){
            if(isFirst){
                stringBuilder.append(subEventData.getObjectId());
                isFirst=false;
            }else {
                stringBuilder.append("&").append(subEventData.getObjectId());
            }
        }
        return stringBuilder.toString();
    }

}
