package com.minvite.fragments.postFragments;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.minvite.R;
import com.minvite.activities.SlideImageActivity;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.listeners.OnRecyclerItemClickListener;
import com.minvite.models.BaseAdapterModel;
import com.minvite.models.invitation.EventData;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.postbook.ImageModel;
import com.minvite.util.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 2/12/16.
 */

public class ImagesFragment extends BaseFragment implements OnRecyclerItemClickListener {
    private List<EventPost> eventPosts =new ArrayList<>();
    private EventData eventData;
    public static ImagesFragment getInstance(List<EventPost> imageEvents, EventData eventData) {
        ImagesFragment imagesFragment=new ImagesFragment();
        imagesFragment.eventPosts = imageEvents;
        imagesFragment.eventData=eventData;
        return imagesFragment;
    }

    @Override
    public void initViews() {
        List<BaseAdapterModel> imageModels = getImageModelList(eventPosts);
        RecyclerView recyclerView= (RecyclerView) findView(R.id.grid_images);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.addItemDecoration(new SpacesItemDecoration(4));
        BaseRecycleAdapter baseRecycleAdapter=new BaseRecycleAdapter(getActivity(),imageModels);
        recyclerView.setAdapter(baseRecycleAdapter);
        baseRecycleAdapter.setOnItemClickListener(this);
    }
    private List<BaseAdapterModel> getImageModelList(List<EventPost> imageEvents) {
        List<BaseAdapterModel> adapterModels = new ArrayList<>();
        if (imageEvents != null) {
            for (EventPost eventPost : imageEvents) {
                List<String> images = eventPost.getImages();
                if (images != null) {
                    for (String img : images) {
                        ImageModel imageModel=new ImageModel(){
                            @Override
                            public int getViewType() {
                                return AppConstants.VIEW_TYPE.GRIDE_IMAGE;
                            }
                        };
                        imageModel.setImageUrl(img);
                        imageModel.setEventPost(eventPost);
                        adapterModels.add(imageModel);

                    }
                }
            }
        }
        return adapterModels;
    }

    private ArrayList<String> getImageUrls(List<EventPost> imageEvents) {
        ArrayList<String> imageUrls = new ArrayList<>();
        if (imageEvents != null) {
            for (EventPost eventPost : imageEvents) {
                List<String> images = eventPost.getImages();
                if (images != null) {
                    for (String img : images) {
                        imageUrls.add(img);
                    }
                }
            }
        }
        return imageUrls;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_images;
    }

    @Override
    public void onRecyclerItemClick(View view, int position, Object o, int actionType) {
        Bundle bundle=new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, getImageUrls(eventPosts));
        bundle.putInt(AppConstants.BUNDLE_KEYS.SELECTED_POSITION,position);
        startNextActivity(bundle, SlideImageActivity.class);
    }
}
