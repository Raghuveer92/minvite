package com.minvite.activities.invitation;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.minvite.R;
import com.minvite.models.BaseApi;
import com.minvite.models.Contact;
import com.minvite.models.EventTable;
import com.minvite.models.GetEventTable;
import com.minvite.models.user.UserProfile;
import com.minvite.models.guest.GuestData;
import com.minvite.models.guest.GuestListResponse;
import com.minvite.models.invitation.EventData;
import com.minvite.models.invitation.EventsResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.BackendLessUtils;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by raghu on 26/9/16.
 */

public class AddEventActivity extends BaseActivity {
    private EventData eventEventData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        setOnClickListener(R.id.btn_submit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                String code = getEditText(R.id.et_code);
                if (TextUtils.isEmpty(code)) {
                    showToast("Please enter your code..!");
                    return;
                }
                getInvitation(code);
        }
    }

    private void getInvitation(String code) {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.EVENTS);
        object.addParameter("where", "eventCode=" + code);
        object.setClassType(EventsResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_INVITAION, object);
    }

    private void addToEventTable(EventData eventData) {
        HttpParamObject httpParamObject = BackendLessUtils.getBaseHttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.EVENT_TABLE);
        httpParamObject.setPostMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("eventCode", eventData.getEventCode());
            jsonObject.put("userId", Preferences.getData(AppConstants.PREF_KEYS.OBJECT_ID, ""));
            jsonObject.put("event", Util.getRelationalObject("Events", eventData.getObjectId()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(BaseApi.class);
        Log.i("msg",jsonObject.toString());
        executeTask(AppConstants.TASK_CODES.EVENT_TABLE, httpParamObject);
    }

    private void checkEvent(String eventCode) {
        HttpParamObject httpParamObject = BackendLessUtils.getBaseHttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.EVENT_TABLE);
        httpParamObject.addParameter("where", "eventCode=" + eventCode + " and userId='" + Preferences.getData(AppConstants.PREF_KEYS.OBJECT_ID, "")+"'");
        httpParamObject.setClassType(GetEventTable.class);
        executeTask(AppConstants.TASK_CODES.GET_EVENT_TABLE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_INVITAION:
                EventsResponse eventsResponse = (EventsResponse) response;
                List<EventData> data = eventsResponse.getData();
                if (data != null) {
                    if (data.size() > 0) {
                        eventEventData = data.get(0);
                        checkEvent(eventEventData.getEventCode());
                    }
                }
                break;
            case AppConstants.TASK_CODES.EVENT_TABLE:
                BaseApi baseApi = (BaseApi) response;
                if (baseApi != null) {
                    String objectId = baseApi.getObjectId();
                    if (!TextUtils.isEmpty(objectId)) {
                        getStatus(eventEventData);
                        showToast("Successfully added to this event");
                        setResult(RESULT_OK);
                    }
                }
                break;
            case AppConstants.TASK_CODES.GUEST_LIST:
                GuestListResponse guestListResponse = (GuestListResponse) response;
                if (guestListResponse != null) {
                    List<GuestData> guestDataList = guestListResponse.getData();
                    if (guestDataList != null) {
                        if (guestDataList.size() > 0) {
                            finish();
                            return;
                        }
                    }
                    addToGuestList(eventEventData);
                    finish();
                }
                break;
            case AppConstants.TASK_CODES.GET_EVENT_TABLE:
                GetEventTable getEventTable= (GetEventTable) response;
                if(getEventTable!=null){
                    List<EventTable> eventTables = getEventTable.getData();
                    if(eventTables!=null){
                        if(eventTables.size()>0){
                            showToast("You have already invited for this event");
                            return;
                        }
                    }
                }
                addToEventTable(eventEventData);
                break;
        }
    }
    private void getStatus(EventData eventData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
        String mobile="";
        UserProfile userProfile = UserProfile.getInstance();
        if(userProfile!=null){
            mobile=userProfile.getMobile();
        }
        if (!TextUtils.isEmpty(mobile)) {
            if (mobile.length() > 10) {
                mobile = mobile.substring((mobile.length()) - 10, mobile.length());
            }
        }
        httpParamObject.addParameter("where", "eventCode=" + eventData.getEventCode() + " and guestMobile="+mobile);
        httpParamObject.setClassType(GuestListResponse.class);
        executeTask(AppConstants.TASK_CODES.GUEST_LIST, httpParamObject);
    }
    private void addToGuestList(EventData eventEventData) {
        HashSet<Contact> contacts=new HashSet<>();
        UserProfile instance = UserProfile.getInstance();
        Contact contact=new Contact();
        contact.setName(instance.getName());
        contact.setPhoneNumber(instance.getMobile());
        contacts.add(contact);
//        MinviteService.startUploadGuestList(this,contacts, eventEventData,false);
    }
}
