    package com.minvite.fragments.invitation;

import android.view.View;

import com.minvite.R;
import com.minvite.listeners.EventDetailListener;
import com.minvite.util.EventType;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by RAHU on 14-07-2016.
 */
public class ChooseEventFragment extends BaseFragment {
    EventDetailListener lintener;

    public static ChooseEventFragment getInstance(EventDetailListener lintener) {
        ChooseEventFragment chooseEventFragment = new ChooseEventFragment();
        chooseEventFragment.lintener = lintener;
        return chooseEventFragment;
    }

    @Override
    public void initViews() {
        setOnClickListener(R.id.typeText, R.id.typeTextImage, R.id.typeImage, R.id.typeVideo);
    }

    @Override
    public void onResume() {
        super.onResume();
        lintener.setTitle("Choose");
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_choose_event;
    }

    @Override
    public void onClick(View v) {
        lintener.setTitle("Event Detail");
        switch (v.getId()) {
            case R.id.typeText:
                lintener.setEventType(EventType.TEXT);
                break;
            case R.id.typeTextImage:
                lintener.setEventType(EventType.TEXT_IMAGE);
                break;
            case R.id.typeImage:
                lintener.setEventType(EventType.IMAGE);
                break;
            case R.id.typeVideo:
                lintener.setEventType(EventType.VIDEO);
                break;
        }
    }
}
