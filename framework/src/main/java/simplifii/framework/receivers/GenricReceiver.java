package simplifii.framework.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by nitin on 04/12/15.
 */
public class GenricReceiver extends BroadcastReceiver {
    private GenBroadcastListener genBroadcastListener;
    public ArrayList<GenBroadcastListener> listeners = new ArrayList<>();
    public static final String ACTION_CART_UPDATE = "CART_UPDATE";
    public static final String ACTION_PUSH= "action_push";

    public GenricReceiver(GenBroadcastListener listener) {
        this.genBroadcastListener = listener;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_CART_UPDATE)) {
            if (this.genBroadcastListener != null) {
                genBroadcastListener.onReceive(intent);
            }
        }
    }

    public static void sendBroadcast(Context context, String action) {
        context.sendBroadcast(new Intent(action));
    }

    public void removeListener(GenBroadcastListener listener) {
        this.listeners.remove(listener);
    }

    public static interface GenBroadcastListener {
        public void onReceive(Intent intent);
    }
}
