package com.minvite.holders;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.minvite.R;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.postbook.ImageModel;
import com.minvite.models.user.UserProfile;
import com.squareup.picasso.Picasso;

/**
 * Created by raghu on 6/12/16.
 */

public class ImageGridHolder extends BaseHolder {
    private ImageView imageView,ivUser;
    private TextView tvStatus;

    public ImageGridHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) findView(R.id.iv_album);
        ivUser= (ImageView) findView(R.id.iv_user);
        tvStatus= (TextView) findView(R.id.tv_status);
    }


    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ImageModel imageModel = (ImageModel) obj;
        String imageUrl = imageModel.getImageUrl();
        EventPost eventPost = imageModel.getEventPost();
        if(eventPost!=null){
            String status = eventPost.getStatus();
            tvStatus.setText(status);
            UserProfile user = eventPost.getUser();
            if(user!=null){
                String imageUrlUser = user.getImageUrl();
                if(!TextUtils.isEmpty(imageUrlUser)){
                    Picasso.with(context).load(imageUrlUser.trim()).placeholder(R.drawable.photo_place_holder).into(ivUser);
                }
            }
        }
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl.trim()).placeholder(R.drawable.photo_place_holder).into(imageView);
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRecyclerClick(view,0);
            }
        });
    }
}
