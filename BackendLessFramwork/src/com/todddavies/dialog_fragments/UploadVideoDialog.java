package com.todddavies.dialog_fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.todddavies.Main;
import com.todddavies.ProgressWheel;
import com.todddavies.components.progressbar.R;
import com.todddavies.listeners.OnUploadImage;
import com.todddavies.models.SlideVideo;
import com.todddavies.utils.BackendLessUtil;

import java.io.File;


/**
 * Created by raghu on 7/11/16.
 */

public class UploadVideoDialog extends DialogFragment {
    SlideVideo slideImage;
    OnUploadImage onUploadListener;
    private Holder holder;
    private ImageLoader imageLoader;

    public static UploadVideoDialog getInstance(SlideVideo slideImage, OnUploadImage onUploadListener) {
        UploadVideoDialog uploadImageDialog = new UploadVideoDialog();
        uploadImageDialog.slideImage = slideImage;
        uploadImageDialog.onUploadListener = onUploadListener;
        return uploadImageDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_upload_image, null);
        getDialog().setTitle("Uploading Images...");
        imageLoader = ImageLoader.getInstance();
        holder = new Holder(view);
        startUploadImageTask();
        return view;
    }

    private void startUploadImageTask() {
        holder.startSpining();
        String videoPath = slideImage.getVideoPath();
        if (videoPath != null) {
            File file = new File(videoPath);
            BackendLessUtil.uploadFile(file, new BackendLessUtil.OnResponseListener() {
                @Override
                public void onSuccess(String fileUrl) {
                    slideImage.setVideoUrl(fileUrl);
                    onUploadListener.onSuccess();
                    dismissAllowingStateLoss();
                }

                @Override
                public void onFailed(String exception) {
                    onUploadListener.onFailed();
                    dismissAllowingStateLoss();
                }
            });
        }
    }


    class Holder {
        ImageView imageView;
        ImageView ivSuccess;
        ProgressWheel progressWheel;

        public Holder(View view) {
            imageView = (ImageView) view.findViewById(R.id.album_art);
            ivSuccess = (ImageView) view.findViewById(R.id.iv_success);
            progressWheel = (ProgressWheel) view.findViewById(R.id.pw_spinner);
            Main.styleRandom(progressWheel, getActivity());
            imageView.setImageResource(R.drawable.video_image);
        }

        public void startSpining() {
            progressWheel.startSpinning();
        }
    }
}
