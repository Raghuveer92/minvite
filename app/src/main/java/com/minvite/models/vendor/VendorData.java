package com.minvite.models.vendor;

import com.minvite.models.BaseAdapterModel;
import com.minvite.models.BaseApi;

import java.io.Serializable;

/**
 * Created by my on 08-11-2016.
 */
public class VendorData extends BaseAdapterModel {
    String venueId;
    String shopName;
    String name;
    String address;
    String contact;
    String description;
    String imageUrl;
    String lat;
    String lng;

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVenueId() {
        return venueId;
    }

    public String getContact() {
        return contact;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}
