package com.minvite.models;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.minvite.enums.DeepLinkParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by raghu on 20/12/16.
 */

public class InviteUtils implements Serializable{
    String eventId,userId,eventCode;
    List<String> subEventIds;

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getSubEventIds() {
        return subEventIds;
    }

    public void setSubEventIds(List<String> subEventIds) {
        this.subEventIds = subEventIds;
    }
    public static void saveData(JSONObject jsonObject) throws JSONException {
        InviteUtils inviteUtils=new InviteUtils();
        if(jsonObject.has(DeepLinkParams.EVENT_ID.getParamName())){
            inviteUtils.eventId=jsonObject.getString(DeepLinkParams.EVENT_ID.getParamName());
        }
        if(jsonObject.has(DeepLinkParams.USER_ID.getParamName())){
            inviteUtils.userId=jsonObject.getString(DeepLinkParams.USER_ID.getParamName());
        }
        if(jsonObject.has(DeepLinkParams.EVENT_CODE.getParamName())){
            inviteUtils.eventCode=jsonObject.getString(DeepLinkParams.EVENT_CODE.getParamName());
        }
        if(jsonObject.has(DeepLinkParams.SUB_EVENT_IDS.getParamName())){
            String subEventsString = jsonObject.getString(DeepLinkParams.SUB_EVENT_IDS.getParamName());
            if(!TextUtils.isEmpty(subEventsString)){
                String[] split = subEventsString.split("&");
                List<String> list = Arrays.asList(split);
                inviteUtils.setSubEventIds(list);
            }
        }
        Gson gson=new Gson();
        Preferences.saveData(AppConstants.PREF_KEYS.DEEP_LIST_DATA,gson.toJson(inviteUtils));
    }
    public static InviteUtils getSavedInstance(){
        InviteUtils inviteUtils = null;
        String json = Preferences.getData(AppConstants.PREF_KEYS.DEEP_LIST_DATA, "");
        if(!TextUtils.isEmpty(json)){
            inviteUtils = new Gson().fromJson(json, InviteUtils.class);
        }
        return inviteUtils;
    }
}
