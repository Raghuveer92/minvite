package com.minvite.enums;

/**
 * Created by raghu on 28/11/16.
 */

public enum DeepLinkParams {
    LINK_TYPE("type"),EVENT_ID("eventId"),EVENT_CODE("eventCode"),SUB_EVENT_IDS("sub_event_ids"),USER_ID("userId");
    String paramName;

    DeepLinkParams(String paramName) {
        this.paramName = paramName;
    }

    public String getParamName() {
        return paramName;
    }
    public static DeepLinkParams findByStatus(String status) {
        for (DeepLinkParams chatStatus : DeepLinkParams.values()) {
            if (chatStatus.paramName == status)
                return chatStatus;
        }
        return null;
    }
}
