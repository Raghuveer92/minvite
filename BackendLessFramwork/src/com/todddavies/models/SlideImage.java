package com.todddavies.models;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by raghu on 8/11/16.
 */
public class SlideImage implements Serializable {
    @SerializedName("imgUrl")
    String imageUrl;
    String imgPath;
    Bitmap bitmap;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public boolean isUploaded() {
        if (TextUtils.isEmpty(imageUrl)) {
            return false;
        } else {
            return true;
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
