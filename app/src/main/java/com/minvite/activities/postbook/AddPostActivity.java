package com.minvite.activities.postbook;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.minvite.R;
import com.minvite.adapters.BaseRecycleAdapter;
import com.minvite.models.BaseAdapterModel;
import com.minvite.models.invitation.EventData;
import com.minvite.models.postbook.EventPost;
import com.minvite.models.postbook.ImageModel;
import com.minvite.models.postbook.VideoModel;
import com.todddavies.UploadMediaUtil;
import com.todddavies.dialog_fragments.MultipalImageUploadDialog;
import com.todddavies.listeners.OnUploadImage;
import com.todddavies.models.SlideImage;
import com.todddavies.models.SlideVideo;
import com.todddavies.utils.CommanMethodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.MediaFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class AddPostActivity extends BaseActivity implements MediaFragment.MediaListener {

    private static final int REQUEST_CODE_PICKER = 120;
    private EventData eventData;
    private MediaFragment mediaFragment;
    private String fileUrl = "";
    private int mediaType = 0;
    private ArrayList<Image> images = new ArrayList<>();
    private ExposureVideoPlayer videoLayout;
    private List<BaseAdapterModel> adapterModels = new ArrayList<>();
    private List<ImageModel> imageModels = new ArrayList<>();
    private List<VideoModel> videoModels = new ArrayList<>();
    private String videoPath;
    private BaseRecycleAdapter baseRecycleAdapter;
    private RecyclerView recyclerView;
    private List<SlideImage> slideImages = new ArrayList<>();
    private SlideVideo slideVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        videoLayout = (ExposureVideoPlayer) findViewById(R.id.videoview);
        videoLayout.setFullScreen(false);
        videoLayout.setAutoPlay(false);
        setRecyclerAdapter();


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            eventData = (EventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            initToolBar(eventData.getName());
        }

        mediaFragment = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(mediaFragment, "").commit();
        setOnClickListener(R.id.iv_write_smthng_img, R.id.iv_video, R.id.btn_post);
    }

    private void setRecyclerAdapter() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        baseRecycleAdapter = new BaseRecycleAdapter(this, adapterModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(baseRecycleAdapter);

        SlideInUpAnimator animator = new SlideInUpAnimator();
        animator.setInterpolator(new OvershootInterpolator());
        recyclerView.setItemAnimator(animator);
        recyclerView.getItemAnimator().setRemoveDuration(500);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_write_smthng_img:
                getMultiaplImages();
                break;
            case R.id.iv_video:
                mediaFragment.getVideo(this);
                break;
            case R.id.btn_post:
                final String postText = getEditText(R.id.et_post);
                if (TextUtils.isEmpty(postText)) {
                    showToast(getString(R.string.error_empty_post));
                    return;
                }
                if (videoPath != null) {
                    uploadVideo();
                } else if (adapterModels.size() > 0) {
                    uploadImage();
                } else {
                    uploadPost(postText);
                }
                break;
        }
    }

    private void uploadImage() {
        for (BaseAdapterModel baseAdapterModel : adapterModels) {
            ImageModel imageModel = (ImageModel) baseAdapterModel;
            String imageUrl = imageModel.getImageUrl();
            if (TextUtils.isEmpty(imageUrl)) {
                SlideImage slideImage = new SlideImage();
                slideImage.setImgPath(imageModel.getImagePath());
                slideImages.add(slideImage);
            }
        }
        UploadMediaUtil.UploadMultipleImage(slideImages, new MultipalImageUploadDialog.OnUploadListener() {
            @Override
            public void onSuccess() {
                uploadPost(getEditText(R.id.et_post));
            }

            @Override
            public void onFailed() {
                showToast(R.string.fail_to_upload);
            }
        }, this);
    }

    private void getMultiaplImages() {
        ImagePicker.create(this)
                .returnAfterCapture(true) // set whether camera action should return immediate result or not
                .folderMode(true) // folder mode (false by default)
                .folderTitle(getString(R.string.app_name)) // folder selection title
                .imageTitle(getString(R.string.post_images)) // image selection title
                .single() // single mode
                .multi() // multi mode (default mode)
                .limit(10) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    private void uploadVideo() {
        if (videoPath != null) {
            slideVideo = new SlideVideo();
            slideVideo.setVideoPath(videoPath);
            UploadMediaUtil.UploadVideo(slideVideo, new OnUploadImage() {
                @Override
                public void onSuccess() {
                    uploadPost(getEditText(R.id.et_post));
                }

                @Override
                public void onFailed() {
                    showToast(R.string.fail_to_upload);
                }
            }, this);
        }
    }

    private void uploadPost(String postText) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_EVENT_POST);
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(EventPost.class);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", postText);
            jsonObject.put("imageUrl", getImagesJSON());
            jsonObject.put("videoUrl", getVideoUrl());
            jsonObject.put("user", Util.getUser());
            jsonObject.put("eventId", eventData.getObjectId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        executeTask(AppConstants.TASK_CODES.POST_EVENT, httpParamObject);
    }

    private String getVideoUrl() {
        if (slideVideo != null) {
            return slideVideo.getVideoUrl();
        }
        return "";
    }

    private String getImagesJSON() {
        List<String> list = new ArrayList<>();
        for (SlideImage slideImage : slideImages) {
            list.add(slideImage.getImageUrl());
        }
        return list.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            images = (ArrayList<Image>) ImagePicker.getImages(data);
            if (images != null && images.size() > 0) {
                setImagesToRecyclerView(images);
                hideVisibility(R.id.layVideo);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                showVisibility(R.id.iv_video);
            }
        }
    }

    private void setImagesToRecyclerView(ArrayList<Image> images) {
        adapterModels.clear();
        for (Image image : images) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImagePath(image.getPath());
            imageModel.setOnRemoveImageListener(new ImageModel.OnRemoveImageListener() {
                @Override
                public void onRemove(ImageModel imageModel) {
                    int position = adapterModels.indexOf(imageModel);
                    adapterModels.remove(imageModel);
                    baseRecycleAdapter.notifyItemRemoved(position);
                    if (adapterModels.size() == 0) {
                        recyclerView.setVisibility(View.GONE);
                    }
                }
            });
            adapterModels.add(imageModel);
        }
        baseRecycleAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.POST_EVENT:
                EventPost eventPost = (EventPost) response;
                if (eventPost != null) {
                    String objectId = eventPost.getObjectId();
                    if (!TextUtils.isEmpty(objectId)) {
                        Intent intent=new Intent();
                        Bundle bundle=new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,eventPost);
                        intent.putExtras(bundle);
                        setResult(RESULT_OK,intent);
                        showToast(getString(R.string.success_post));
                        finish();
                    }
                }
                break;
        }
    }

    @Override
    public void onGetVideo(String path, int mediaType) {
        videoPath = path;
        recyclerView.setVisibility(View.GONE);
        hideVisibility(R.id.iv_write_smthng_img);
        showVisibility(R.id.layVideo);
        try {
            Uri uri = CommanMethodes.getUriFromPath(videoPath);
            videoLayout.setVideoSource(uri);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
