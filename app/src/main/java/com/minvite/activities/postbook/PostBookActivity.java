package com.minvite.activities.postbook;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.minvite.R;
import com.minvite.application.AppController;
import com.minvite.fragments.postbook.GuestListFragment;
import com.minvite.fragments.postbook.MoreFragment;
import com.minvite.fragments.postbook.PostBookFragment;
import com.minvite.fragments.sub_event.SubEventsCardFragment;
import com.minvite.models.invitation.EventData;
import com.minvite.models.sub_events.SubEventData;
import com.minvite.services.BeaconRefreshService;
import com.minvite.services.ScanService;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class PostBookActivity extends BaseActivity implements OnFragmentChangeListener {

    BaseFragment postBookFragment, albumFragment, subEventsFragment, moreFragment, guestListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_book);
    }



    protected void loadBundle(Bundle bundle) {
        int selectedPosition = bundle.getInt(AppConstants.BUNDLE_KEYS.SELETED_SUB_EVENT);
        EventData eventData = (EventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        ArrayList<SubEventData> subEventList = (ArrayList<SubEventData>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SUB_EVENTS);
        if (eventData != null) {
            subEventsFragment = SubEventsCardFragment.getInstance(this, eventData, true, subEventList, selectedPosition);
            postBookFragment = PostBookFragment.getInstance(eventData);
            moreFragment = MoreFragment.getInstance(eventData);
            guestListFragment = GuestListFragment.getInstance(this, eventData);
            albumFragment = GuestListFragment.getInstance(this, eventData);
        }
        initToggleMap();
    }

    List<PostImageToggle> postImageToggles = new ArrayList<>();

    private void initToggleMap() {
        PostImageToggle togleInvitation = new PostImageToggle(R.drawable.sentmail_red, R.drawable.sentmail_white, subEventsFragment, R.id.iv_send_foot);
        PostImageToggle togleAlbum = new PostImageToggle(R.drawable.photocamera_red, R.drawable.camera_white, albumFragment, R.id.iv_foot_camera);
        PostImageToggle postBook = new PostImageToggle(R.drawable.red_male_user, R.drawable.white_male_user, postBookFragment, R.id.iv_foot_contact);
        PostImageToggle timeLine = new PostImageToggle(R.drawable.msg_red, R.drawable.msg_white, guestListFragment, R.id.iv_foot_msg);
        PostImageToggle toglevenue = new PostImageToggle(R.drawable.menu, R.drawable.menu_white, moreFragment, R.id.iv_foot_menu);
        postImageToggles.add(togleInvitation);
        postImageToggles.add(togleAlbum);
        postImageToggles.add(postBook);
        postImageToggles.add(timeLine);
        postImageToggles.add(toglevenue);
        togleInvitation.imageView.callOnClick();
    }

    private void setUnselectAll() {
        for (PostImageToggle postImageToggle : postImageToggles) {
            postImageToggle.setUnselect();
        }
    }

    public void replaceFragment(Fragment f) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, f).commitAllowingStateLoss();
    }

    @Override
    public void onChangeFragment(Fragment fragment, boolean isAddToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack("");
        }
        fragmentTransaction.replace(R.id.frame_container, fragment, "").commit();
    }

    Fragment selecedFragment;

    public class PostImageToggle implements View.OnClickListener {
        private int redDrawableId, whiteDrawableId;
        private Fragment fragment;
        private ImageView imageView;

        PostImageToggle(int redId, int whiteId, Fragment f, int imageId) {
            this.redDrawableId = redId;
            this.whiteDrawableId = whiteId;
            this.fragment = f;
            imageView = (ImageView) findViewById(imageId);
            imageView.setOnClickListener(this);
        }

        void setUnselect() {
            imageView.setImageResource(redDrawableId);
        }


        @Override
        public void onClick(View view) {
            if (fragment != selecedFragment) {
                setUnselectAll();
                imageView.setImageResource(whiteDrawableId);
                replaceFragment(fragment);
                selecedFragment = fragment;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        postBookFragment.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getBluetoothPermission();
    }

    private void getBluetoothPermission() {
        if (AppController.isPermissionGranted(this, Manifest.permission.BLUETOOTH)) {
            if (AppController.isPermissionGranted(this, Manifest.permission.BLUETOOTH_ADMIN)) {
                return;
            }
        }
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BLUETOOTH,Manifest.permission.BLUETOOTH_ADMIN},AppConstants.REQUEST_CODES.BLUETOOTH);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for(int x=0;x<permissions.length;x++){
            if(grantResults[x]!= PackageManager.PERMISSION_GRANTED){
                return;
            }
        }
    }

}
