package com.minvite.fragments.sub_event;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.minvite.R;
import com.minvite.activities.SendInvitationActivity;
import com.minvite.fragments.base.SubEventBaseFragment;
import com.minvite.models.invitation.EventData;
import com.minvite.models.sub_events.SubEventData;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 11/10/16.
 */

public class WebEventFragment extends SubEventBaseFragment {
    private SubEventData subEventData;
    private EventData eventData;

    public static WebEventFragment getInstance(SubEventData subEventData, EventData eventData) {
        WebEventFragment webEventFragment = new WebEventFragment();
        webEventFragment.subEventData = subEventData;
        webEventFragment.eventData=eventData;
        return webEventFragment;
    }

    private WebView webView;

    @Override
    public void initViews() {
        webView = (WebView) findView(R.id.web_view);
        webView.setVisibility(View.GONE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(null);
        webView.setWebViewClient(null);
        webView.loadDataWithBaseURL("about:blank", "<html></html>","text/html", "UTF-8", null);
        if (subEventData != null) {
            getStatus(subEventData);
            webView.loadUrl(subEventData.getWebsite());
        }
        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                webView.setVisibility(View.VISIBLE);
            }
        });
        setOnClickListener(R.id.btn_accept, R.id.btn_may_be, R.id.btn_reject, R.id.iv_edit);
    }

    @Override
    protected void getSubEvents(String params) {

    }

    @Override
    protected void setButtonStatus(String status) {
        showVisibility(R.id.lay_status);
        hideVisibility(R.id.lay_btns);
        switch (status) {
            case AppConstants.INVITATION_STATUS.ACCEPTED:
                setText(getString(R.string.invite_accepted_status),R.id.tv_status);
                break;
            case AppConstants.INVITATION_STATUS.REJECTED:
                setText(getString(R.string.invite_rejected_status),R.id.tv_status);
                break;
            case AppConstants.INVITATION_STATUS.MAY_BE:
                setText(getString(R.string.invite_may_be_status),R.id.tv_status);
                break;
            default:
                hideVisibility(R.id.lay_status);
                showVisibility(R.id.lay_btns);
                break;
        }

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_web_event;
    }
}
