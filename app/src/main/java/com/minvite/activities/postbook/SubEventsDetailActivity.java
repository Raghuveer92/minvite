package com.minvite.activities.postbook;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.minvite.R;
import com.minvite.activities.FullScreenVideoActivity;
import com.minvite.async_tasks.CommentSubEventTask;
import com.minvite.async_tasks.LikeSubEventTask;
import com.minvite.models.user.UserProfile;
import com.minvite.models.postbook.Comment;
import com.minvite.models.sub_events.SubEventData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import nz.co.delacour.exposurevideoplayer.FullScreenClickListener;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class SubEventsDetailActivity extends BaseActivity implements OnLikeListener {

    private LinearLayout layCommentContainer;
    private LayoutInflater inflater;
    private LikeButton likeButton;
    private SubEventData subEventData;
    private EditText etComment;
    private boolean isChangeLike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_events_detail);
        layCommentContainer = (LinearLayout) findViewById(R.id.lay_comment_container);
        inflater = LayoutInflater.from(this);
        likeButton = (LikeButton) findViewById(R.id.btn_like);
        likeButton.setOnLikeListener(this);
        etComment = (EditText) findViewById(R.id.et_post_comment);
        setOnClickListener(R.id.sent_comment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sent_comment:
                String comment = etComment.getText().toString().trim();
                if (!TextUtils.isEmpty(comment)) {
                    Comment newComment = CommentSubEventTask.sendComment(comment, subEventData);
                    subEventData.getComments().add(newComment);
                    setComments(subEventData.getComments());
                    etComment.setText("");
                    Util.hideKeyboard(this, etComment);
                }
                break;
        }
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        subEventData = (SubEventData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if (subEventData != null) {
            setData(subEventData);
        }
    }

    private void setData(SubEventData subEventData) {
        this.subEventData = subEventData;
        initToolBar(subEventData.getName());
        setImageUrl(subEventData.getImageUrl(), R.id.iv_photo);
        List<Comment> comments = subEventData.getComments();
        List<UserProfile> likes = subEventData.getLikes();
        setLikes(likes);
        if (comments != null) {
            setComments(comments);
        }
        final String videoUrl = subEventData.getVideoUrl();
        if (!TextUtils.isEmpty(videoUrl)) {
            ExposureVideoPlayer exposureVideoPlayer = (ExposureVideoPlayer) findViewById(R.id.videoview);
            exposureVideoPlayer.init(this);
            exposureVideoPlayer.setFullScreen(false);
            exposureVideoPlayer.setAutoPlay(false);
            try {
                exposureVideoPlayer.setVideoSource(videoUrl);
            } catch (Exception e) {

            }
            exposureVideoPlayer.setOnFullScreenClickListener(new FullScreenClickListener() {
                @Override
                public void onToggleClick(boolean isFullscreen) {
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.BUNDLE_KEYS.VIDEO_URL, videoUrl);
                    startNextActivity(bundle, FullScreenVideoActivity.class);
                }
            });
        } else {
            hideVisibility(R.id.videoview);
        }
    }

    private void setLikes(List<UserProfile> likes) {
        isChangeLike = false;
        likeButton.setLiked(checkMyLike(likes));
        setText("" + likes.size(), R.id.tv_like_count);
        isChangeLike = true;
    }

    private void setComments(List<Comment> comments) {
        Collections.sort(comments);
        setText("" + comments.size(), R.id.tv_comment_count);
        layCommentContainer.removeAllViews();
        for (Comment comment : comments) {
            setComment(comment);
        }
    }

    protected boolean checkMyLike(List<UserProfile> likes) {
        if (likes == null) {
            return false;
        }
        String objectId = Preferences.getData(AppConstants.PREF_KEYS.OBJECT_ID, "");
        if (!TextUtils.isEmpty(objectId)) {
            for (UserProfile userProfile : likes) {
                if (objectId.equals(userProfile.getObjectId())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void setComment(Comment comment) {
        View view = inflater.inflate(R.layout.row_comment, null);
        setText(comment.getComment(), R.id.tv_comment_text, view);
        String time = DateUtils.getRelativeTimeSpanString(comment.getCreated(), System.currentTimeMillis(), 0L).toString();
        setText(time, R.id.tv_time_stamp, view);
        layCommentContainer.addView(view);
        UserProfile user = comment.getUser();
        if (user != null) {
            setText(user.getName(), R.id.tv_comment_user, view);
            setImageUrl(user.getImageUrl(), R.id.iv_comment_row, view);
        }
    }

    @Override
    public void liked(LikeButton likeButton) {
        if (isChangeLike) {
            changeLike(subEventData);
        }
    }


    @Override
    public void unLiked(LikeButton likeButton) {
        if (isChangeLike) {
            changeLike(subEventData);
        }
    }

    protected void changeLike(SubEventData subEventData) {
        LikeSubEventTask.like(subEventData);
        setLikes(subEventData.getLikes());
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, subEventData);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }
}
