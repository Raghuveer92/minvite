package com.minvite.FCM;

import android.util.Log;

/**
 * Created by raghu on 19/11/16.
 */

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.minvite.services.MinviteService;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class MyFireBaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "FCM";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        Preferences.saveData(AppConstants.PREF_KEYS.FCM_TOKEN,refreshedToken);
        MinviteService.updateToken(this);
    }
}