package com.minvite.activities.more;

import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.minvite.R;
import com.minvite.fragments.more.AlbumFragment;
import com.minvite.fragments.more.VenuesFragment;
import com.minvite.fragments.more.TimeLineFragment;
import com.minvite.models.MoreData;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class MoreActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_container);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        MoreData moreData = (MoreData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if (moreData != null) {
            switch (moreData.getFragmentType()) {
                case AppConstants.FRAGMENT_TYPE.VENUES:
                    addFragment(new VenuesFragment());
                    break;
                case AppConstants.FRAGMENT_TYPE.ALBUM:
                    addFragment(AlbumFragment.getInstance(moreData.getEventData()));
                    break;
                case AppConstants.FRAGMENT_TYPE.TIME_LINE:
                    addFragment(TimeLineFragment.getInstance(moreData.getEventData()));
                    break;
            }
        }
    }

    private void addFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.activity_more_container, fragment).commit();
    }
}
