package com.minvite.fragments.postFragments;

import android.text.TextUtils;

import com.minvite.R;
import com.squareup.picasso.Picasso;

import ooo.oxo.library.widget.TouchImageView;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by raghu on 5/12/16.
 */
public class FullImageFragment extends BaseFragment {
    String imageUrl;

    public static FullImageFragment getInstance(String imageUrl) {
        FullImageFragment fullImageFragment = new FullImageFragment();
        fullImageFragment.imageUrl = imageUrl;
        return fullImageFragment;
    }

    @Override
    public void initViews() {
        TouchImageView touchImageView = (TouchImageView) findView(R.id.iv_image_full);
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl.trim()).placeholder(R.drawable.photo_place_holder).into(touchImageView);
        }

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_full_image;
    }
}
