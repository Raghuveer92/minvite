package simplifii.framework.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import simplifii.framework.R;
import simplifii.framework.utility.AppConstants;


public class CustomImageCheckBox extends ImageView implements View.OnClickListener {

    private int selectResourceId;
    private int unSelectResourceId;
    boolean isSelect;
    OnSelectChangeListener selectListener;

    public CustomImageCheckBox(Context context) {
        super(context);
    }

    public CustomImageCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomImageCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomImageCheckBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs,
                R.styleable.CustomImageCheckBox);
        selectResourceId = a.getResourceId(R.styleable.CustomImageCheckBox_selectSrc,0);
        unSelectResourceId = a.getResourceId(R.styleable.CustomImageCheckBox_unSelectSrc,0);
        boolean isChecked=a.getBoolean(R.styleable.CustomImageCheckBox_checked,false);
        setSelect(isChecked);
        setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(isSelect){
            isSelect=false;
            changeResoure(isSelect);
            callOnSelect(view,isSelect);
        }else {
            isSelect=true;
            changeResoure(isSelect);
            callOnSelect(view,isSelect);
        }
    }

    private void changeResoure(boolean isSelect) {
        if(isSelect){
            setImageResource(selectResourceId);
        }else {
            setImageResource(unSelectResourceId);
        }
        this.isSelect=isSelect;
    }

    private void callOnSelect(View view,boolean isSelect) {
        if(selectListener!=null){
            selectListener.onSelectChange(view,isSelect);
        }
    }
    public void setSelect(boolean isSelected){
        changeResoure(isSelected);
        this.isSelect=isSelected;
    }

    public void setOnSelectListener(OnSelectChangeListener selectListener){
        this.selectListener=selectListener;
    }
    interface OnSelectChangeListener{
        void onSelectChange(View view,boolean isSelected);
    }
}
