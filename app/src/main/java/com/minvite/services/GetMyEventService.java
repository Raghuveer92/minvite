package com.minvite.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.minvite.models.user.UserProfile;
import com.minvite.models.guest.GuestData;
import com.minvite.models.guest.GuestListResponse;
import com.minvite.receivers.MinviteReceiver;

import org.json.JSONException;

import java.sql.SQLException;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class GetMyEventService extends IntentService {
    private static final String ACTION_GET_EVENTS = "com.bricksnwheels.services.action.FOO";

    public GetMyEventService() {
        super("GetMyEventService");
    }

    public static void startGetEvents(Context context) {
        Intent intent = new Intent(context, GetMyEventService.class);
        intent.setAction(ACTION_GET_EVENTS);
        context.startService(intent);
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_EVENTS.equals(action)) {
                handleActionGetEvents();
            }
        }
    }

    private void handleActionGetEvents() {
        HttpParamObject httpParamObject=new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GUEST_LIST);
        UserProfile profile = UserProfile.getInstance();
        if(profile==null){
            return;
        }
        httpParamObject.addParameter("where","guestMobile="+profile.getMobile());
        httpParamObject.addParameter("props","eventId");
        httpParamObject.setClassType(GuestListResponse.class);
        Service service= ServiceFactory.getInstance(this,AppConstants.TASK_CODES.GET_MY_EVENTS);
        try {
           GuestListResponse guestListResponse= (GuestListResponse) service.getData(httpParamObject);
            if(guestListResponse!=null){
                List<GuestData> dataList = guestListResponse.getData();
                if(dataList!=null){
                    saveData(dataList);
                    MinviteReceiver.sendBroadCast(this);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }

    }

    private void saveData(List<GuestData> dataList) {
        StringBuilder stringBuilder=new StringBuilder();
        for (int x=0;x<dataList.size();x++){
            GuestData guestData = dataList.get(x);
            if(x==0){
                stringBuilder.append("objectId='"+guestData.getEventId()+"'");
            }else {
                stringBuilder.append(" or objectId='"+guestData.getEventId()+"'");
            }
        }
        Preferences.saveData(AppConstants.PREF_KEYS.EVENT_IDS,stringBuilder.toString());
    }
}
