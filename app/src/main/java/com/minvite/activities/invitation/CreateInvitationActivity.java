package com.minvite.activities.invitation;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.minvite.R;
import com.minvite.models.invitation.EventsResponse;
import com.minvite.models.invitation.EventData;
import com.minvite.util.EventCat;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class CreateInvitationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setOnClickListener(R.id.lay_personal, R.id.lay_professional, R.id.lay_wedding,R.id.btn_submit);
    }

    @Override
    public void onClick(View v) {
        EventCat eventCat = null;
        switch (v.getId()) {
            case R.id.lay_personal:
                eventCat=EventCat.PERSONAL;
                break;
            case R.id.lay_professional:
                eventCat=EventCat.PROFESSIONAL;
                break;
            case R.id.lay_wedding:
                eventCat=EventCat.PUBLIC;
                break;
            case R.id.btn_submit:
                String code=getEditText(R.id.et_code);
                if(TextUtils.isEmpty(code)){
                    showToast("Please enter your code..!");
                    return;
                }
                getInvitation(code);
                return;
        }
        Bundle bundle=new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.EVENT_CAT,eventCat.getType());
        Intent intent=new Intent(this,RegisterEventActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent,AppConstants.REQUEST_CODES.ADD_INVITAION);
//        startNextActivity(bundle,RegisterEventActivity.class);
    }

    private void getInvitation(String code) {
        HttpParamObject object=new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.EVENTS);
        object.addParameter("where","objectId%3D"+code);
        object.setClassType(EventsResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_INVITAION,object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(taskCode== AppConstants.TASK_CODES.GET_INVITAION&&response!=null){
            EventsResponse eventsResponse = (EventsResponse) response;
                for(EventData eventData : eventsResponse.getData()){
                    if(eventData.getObjectId().equals(getEditText(R.id.et_code))){
                        Bundle bundle=new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, eventData);
                        startNextActivity(bundle,EventIDetailActivity.class);
                    }
                }
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==AppConstants.REQUEST_CODES.ADD_INVITAION&&resultCode==RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }
}
