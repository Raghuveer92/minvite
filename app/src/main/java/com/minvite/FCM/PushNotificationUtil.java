package com.minvite.FCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.minvite.R;
import com.minvite.activities.SplashActivity;
import com.minvite.enums.ChatStatus;
import com.minvite.enums.ChatType;
import com.minvite.enums.NotificationType;
import com.minvite.models.user.UserProfile;
import com.minvite.models.chat.ChatModel;
import com.minvite.models.guest.GuestData;
import com.minvite.util.NotificationsUtil;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.utility.AppConstants;

/**
 * Created by raghu on 29/11/16.
 */

public class PushNotificationUtil {
    public static void sendChatNotification(Context context, JSONObject contentObject){
        ChatModel chatModel=new ChatModel();
        GuestData guestData=new GuestData();
        chatModel.setStatus(ChatStatus.INCOMING.getStatus());
        chatModel.setType(ChatType.INCOMING.getStatus());
        try {
            if(contentObject.has("messageType")){
                chatModel.setMessageType(contentObject.getInt("messageType"));
            }
            if(contentObject.has("message")){
                chatModel.setMessage(contentObject.getString("message"));
            }
            if(contentObject.has("fileUrl")){
                chatModel.setFileUrl(contentObject.getString("fileUrl"));
            }
            if(contentObject.has("eventId")){
                chatModel.setEventId(contentObject.getString("eventId"));
                guestData.setEventId(contentObject.getString("eventId"));
            }
            if(contentObject.has("timeStamp")){
                chatModel.setTimeStamp(contentObject.getLong("timeStamp"));
            }
            UserProfile userProfile = UserProfile.getInstance();
            if(userProfile!=null){
                chatModel.setMyNumber(userProfile.getMobile());
            }
            if(contentObject.has("myNumber")){
                chatModel.setToUserNumber(contentObject.getString("myNumber"));
                guestData.setGuestMobile(contentObject.getString("myNumber"));
            }
            saveChatData(chatModel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        guestData.setGuestName("Loading...");
        NotificationsUtil.showChatNotification(context,guestData);
    }

    private static void saveChatData(ChatModel chatModel) {
        chatModel.save();
    }
}
